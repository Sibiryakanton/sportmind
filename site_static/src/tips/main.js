import Vue from 'vue'
import TipTable from './vue/tips_list_page'

new Vue({
  render: h => h(TipTable)
}).$mount('#app');


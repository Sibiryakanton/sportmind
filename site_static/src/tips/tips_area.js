Vue.component('TipsArea', {
  data: function () {
    return {
      table_headers: ['Дата', 'Страна', 'Лига', 'Матч', 'Прогноз', 'Кэфы', 'Статус', 'Результат', 'Профит'],
      leagues: [
        ],
      tip_sources: [
        ],
      /*tips: [
        {event_datetime: '12.09.2019', country: 'England', league: 'Championship', match: 'Manchester v CSKA',
        tip_type: 'Обе забьют - Yes', tip_status: 'win', result: '0:2', odds: [{'bookmaker': 'BetFair', 'value': 2.21},
                                                                                {'bookmaker': 'Bet365', 'value': 2.51}],
          profit: 0.9, event_status: 'FT'
        },
        {event_datetime: '13.09.2019', country: 'French', league: 'Ligue 1', match: 'Tusur v Musur',
        tip_type: 'Обе забьют - No', tip_status: 'lose', result: '0:2', odds: [{'bookmaker': 'BetFair', 'value': 2.51},
                                                                                {'bookmaker': 'Bet365', 'value': 1.21}],
          profit: -1.00, event_status: 'FT'
        },
        {event_datetime: '14.09.2019', country: 'German', league: 'Bundesliga', match: 'Ziber v Ahten',
        tip_type: 'Обе забьют - No', tip_status: null, result: null, odds: [{'bookmaker': 'BetFair', 'value': 3.00},
                                                                                {'bookmaker': 'Bet365', 'value': 2.55}],
          profit: null, event_status: 'live'
        }
      ],*/
      tips: [],
      show_tips_none_pop: null,
      show_waiting_pop: false,
      show_error_pop: false,
      show_start_pop: true,
      current_tip_source: '',
      current_datetime: '',
      current_league: '',
    }
  },
  mounted: function(){
    this.href = window.location.href;
    this.uploadLeagues();
    this.uploadTipSources();
  },
  watch: {
    tips: function (val) {
      this.show_tips_none_pop = (!Boolean(this.tips.length));
    }
  },
  methods:
      {
        uploadLeagues: function () {
          axios.get('/rest/get_leagues', {
            })
            .then(function (response) {
              this.app.$children[1].leagues = response.data;
            })
            .catch(function (error) {
              console.log(error);
            });
        },
        uploadTipSources: function () {
          axios.get('/rest/tip_sources', {
            })
            .then(function (response) {
              this.app.$children[1].tip_sources = response.data;

            })
            .catch(function (error) {
              console.log(error);
              this.app.$children[1].show_error_pop = true;
            });
        },
        uploadTips: function (event_date='', league='', tip_source='') {
          this.show_waiting_pop = true;
          this.show_start_pop = false;
          this.tips = [{'event_status': 'FT'}];
          if (league === 0) {
            league = '';
          }
          if (tip_source === 0) {
            tip_source = '';
          }
          var data = {'event_date': event_date, 'league': league, 'tip_source': tip_source};
          var str_parts = [];
          for(var p in data)
             str_parts.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
          var query = str_parts.join("&");
          axios.get('/rest/get_tips/?'+query, {})
            .then(function (response) {
              this.app.$children[1].tips = response.data;
              this.app.$children[1].show_error_pop = false;
              this.app.$children[1].show_waiting_pop = false;

            })
            .catch(function (error) {
              this.app.$children[1].show_error_pop = true;
              this.app.$children[1].show_waiting_pop = false;

              console.log(error.response);
            });

        },
      },
  template: `    
    <div class="container-fluid" id="tips_area">
        <form class="mb-2">
          <div class="row">
            <div class="col-md-2">
              <input type="date" class="form-control" name="event_datetime" v-model="current_datetime">
            </div>
              <div class="col-md-2">
                  <select class="custom-select " v-model="current_league">
                    <option selected value="">Все лиги</option>
                    <option v-for="league in leagues" :value="league.id"> {{league.country}} | {{league.league_name}}</option>
                  </select>
              </div>
              <div class="col-md-2">
                  <select class="custom-select " v-model="current_tip_source">
                    <option selected value="">Источник прогноза</option>
                    <option v-for="tip_source in tip_sources" :value="tip_source.id" 
                    name="tip_source">{{tip_source.title}}</option>
                  </select>
              </div>
              <div class="col-md-2">
                  <button type="button" class="btn btn-info" 
                  v-on:click="uploadTips(current_datetime, current_league, current_tip_source)">Применить</button>
              </div>
          </div>
        </form>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col" v-for="table_header in table_headers">{{table_header}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="tip in tips">
              <th scope="row">{{tip.event_datetime}}</th>
              <td>{{tip.country}}</td>
              <td>{{tip.league}}</td>
              <td>{{tip.match}}</td>
              <td>{{tip.tip_type}}</td>
              <td colspan="4" class="bg-warning" v-if="tip.event_status === 'LIVE'">Матч идет в лайве, сбор коэффициентов остановлен</td>
              <td v-if="tip.event_status !== 'LIVE'">
                  <div class="row mr-1" v-for="odd in tip.odds">
                      <span class="sportsbook">{{odd.bookmaker}}</span>
                  <span class="odd_value ml-1">{{odd.value}}</span>
                  </div>
              </td>
              <td class="text-success" v-if="tip.tip_status=='win' & tip.event_status !== 'LIVE'">{{tip.tip_status}}</td>
              <td class="text-danger" v-else-if="tip.tip_status=='lose' & tip.event_status !== 'LIVE'">{{tip.tip_status}}</td>
              <td colspan="3" class="bg-info" v-else-if="tip.event_status === 'NS'">Матч еще не начался</td>
              <td colspan="3" class="bg-secondary" v-else-if="tip.event_status === 'POSTP'">Матч был перенесен по неизвестным причинам</td>

              <td class="text-success" v-if="tip.tip_status=='win' & tip.event_status !== 'LIVE'">{{tip.result}}</td>
              <td class="text-danger" v-else-if="tip.tip_status=='lose' & tip.event_status !== 'LIVE'">{{tip.result}}</td>

              <td class="text-success" v-if="tip.tip_status=='win' & tip.event_status !== 'LIVE'">{{tip.profit}}</td>
              <td class="text-danger" v-else-if="tip.tip_status=='lose' & tip.event_status !== 'LIVE'">{{tip.profit}}</td>
            </tr>
          </tbody>
        </table>
        <div class="alert alert-primary" role="alert" v-if="show_tips_none_pop">
          На сегодня прогнозов больше нет
        </div>
        <div class="alert alert-primary" role="alert" v-if="show_waiting_pop">
          Подождите, база прогнозов загружается...
        </div>
        <div class="alert alert-danger" role="alert" v-if="show_error_pop">
          При загрузке базы произошла ошибка. Обновите странице или сообщите об ошибке администратору.
        </div>
      <div class="alert alert-primary" role="alert" v-if="show_start_pop">
          Выберите источник прогнозов
        </div>
        <!--
        <nav aria-label="История прогнозов">
            <ul class="pagination justify-content-center">
                <li class="page-item"><a class="page-link" href="#">Предыдущая</a></li>

                <li v-for='page in all_pages' class="page-item active" v-if="page!=='...' & current_page===page"><span class="page-link" :href="href+'page/'+page">{{page}}</span></li>
                <li class="page-item" v-else-if="page!=='...'"><a class="page-link" :href="href+'page/'+page">{{page}}</a></li>
                <li v-else><span class="page-link" >{{page}}</span></li>

                <li class="page-item"><a class="page-link" href="#">Следующая</a></li>
            </ul>
        </nav>-->
    </div>`
})

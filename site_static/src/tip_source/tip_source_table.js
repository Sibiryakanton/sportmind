Vue.component('TipSourceDetail', {
  data: function () {
    return {
      table_headers: ['Страна', 'Лига', 'Все прогнозы', 'Победы', 'Проигрыши', 'Средний кэф',
        'Кэф выигрыша', 'Профит', 'Эффективность, %'],
      tip_source: null,
      href: '/',
    }
  },
  mounted: function(){
    this.href = window.location.href;
    var splitted_url = this.href.split('/');
    var tip_source_id = splitted_url[splitted_url.length-1];
    this.uploadTipSource(tip_source_id);
  },
  methods:
      {
        uploadTipSource: function (tip_source_id) {
          axios.get('/rest/tip_sources/'+tip_source_id, {
            })
            .then(function (response){
              this.app.$children[1].tip_source = response.data;

            })
            .catch(function (error) {
              console.log(error);
            });
        },
      },
  template: `    <div class="container-fluid" id="tip_source_detail">
    <h5>Детальная статистика модели {{tip_source.title}}</h5>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col" v-for="table_header in table_headers">{{table_header}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="league_info in tip_source.league_stats">
              <th scope="row">{{league_info.league.country}}</th>
              <th scope="row">{{league_info.league.league_name}}</th>
              <td>{{league_info.tip_stat.all}}</td>
               <td>{{league_info.tip_stat.wins}}</td>
              <td>{{league_info.tip_stat.loses}}</td>
              <td>{{league_info.tip_stat.avg_bet}}</td>
              <td>{{league_info.tip_stat.avg_profit_bet}}</td>
              <td>{{league_info.tip_stat.profit}}</td>
              <td>{{league_info.tip_stat.performance}}</td>
            </tr>
          </tbody>
        </table>
    </div>`
});

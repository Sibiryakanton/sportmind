Vue.component('Header', {
  data() {
    return {
      sources_headers: [
          {'title': 'ID', 'colspan': 1},
        {'title': 'Title', 'colspan': 1},
        {'title': '7 days', 'colspan': 5},
        {'title': '30 days', 'colspan': 5},
        {'title': 'Current Year', 'colspan': 5},

        ],
      subcolumns_sources: ['Profit', 'All', 'Wins', 'Loses', '%'],

      components: [],
      system_proxies: [],
      components_headers: [
          {'title': 'ID', 'colspan': 1},
        {'title': 'Status', 'colspan': 1},
        {'title': 'Service', 'colspan': 1},
        {'title': 'Last Updates', 'colspan': 1},
        ],
      system_proxies_headers: [
          {'title': 'Country', 'colspan': 1},
        {'title': 'Proxy Type', 'colspan': 1},
        {'title': 'IP', 'colspan': 1},
        {'title': 'Port', 'colspan': 1},
        {'title': 'Will Expire At', 'colspan': 1},

        {'title': 'Is Active', 'colspan': 1},
        ],
      sportsbooks_accounts: [],
      sportsbooks_accounts_headers: [
          {'title': 'ID', 'colspan': 1},
        {'title': 'Bookmaker', 'colspan': 1},
        {'title': 'Currency', 'colspan': 1},
        {'title': 'Balance', 'colspan': 1},
        {'title': 'Proxy', 'colspan': 1},

        {'title': 'Is Active', 'colspan': 1},
        ],

      tip_sources: [],
      user_info: {},
      time: '',
      timezone: '',
    }
  },
  methods:
      {
        uploadTipSources: function () {
          axios.get('/rest/tip_sources', {
            })
            .then(function (response) {
              console.log(response.data);
              this.app.$children[0].tip_sources = response.data;

            })
            .catch(function (error) {
              console.log(error);
              this.app.$children[1].show_error_pop = true;
            });
        },
        uploadUserInfo: function () {
          axios.get('/rest/user/', {
            })
            .then(function (response) {
              this.app.$children[0].user_info = response.data;

            })
            .catch(function (error) {
              console.log(error);
              this.app.$children[1].show_error_pop = true;
            });
        },
        uploadServiceInfo: function() {
          this.uploadComponents();
          this.uploadSystemProxies();
        },
        uploadComponents: function () {
          axios.get('/rest/components/', {
            })
            .then(function (response) {
              this.app.$children[0].components = response.data['results'];

            })
            .catch(function (error) {
              console.log(error);
              this.app.$children[1].show_error_pop = true;
            });
        },
        uploadSystemProxies: function () {
          axios.get('/rest/user/get_system_proxies', {
            })
            .then(function (response) {
              this.app.$children[0].system_proxies = response.data;

            })
            .catch(function (error) {
              console.log(error);
            });
        },
        uploadSportsbooksInfo: function () {
          axios.get('/rest/user/get_profiles', {
            })
            .then(function (response) {
              this.app.$children[0].sportsbooks_accounts = response.data;
            })
            .catch(function (error) {
              console.log(error);
              this.app.$children[1].show_error_pop = true;
            });
        },
        uploadTime: function () {
          axios.get('/rest/get_time', {
            })
            .then(function (response) {
              this.app.$children[0].time = response.data['time'];
              this.app.$children[0].timezone = response.data['timezone'];

            })
            .catch(function (error) {
              console.log(error);
              this.app.$children[1].show_error_pop = true;
            });
        },

  },
  mounted: function(){
    this.uploadTime();
    setInterval(this.uploadTime, 60*1000);
    this.uploadUserInfo();
  },
  template: `    <div class="container-fluid">
            <br>
        <div class="row">
          <div class="col"><h3><a href="/">Sportmind</a></h3>
            <h6 class="mb-2">Время сервера: {{time}} ({{timezone}})</h6>
            <h6><a href="/admin" v-if="user_info.is_active == false">Вход</a></h6>
            <br>
  <!-- Button trigger modal -->
<button type="button" class="btn btn-outline-primary mt-2" data-toggle="modal" data-target="#tipSourceModal" v-on:click="uploadTipSources">
  Источники прогнозов
</button>
<button type="button" class="btn btn-outline-primary mt-2" data-toggle="modal" data-target="#serviceInfoModal" 
        v-if='user_info.is_superuser' v-on:click="uploadServiceInfo">
  Состояние сервиса
</button>
<button type="button" class="btn btn-outline-primary mt-2" data-toggle="modal" 
        data-target="#financialInfoModal" v-if='user_info.is_active' v-on:click="uploadSportsbooksInfo">
  Финансы
</button>
<!-- Tip Sources Modal -->
<div class="modal bd-example-modal-xl fade" id="tipSourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Краткая статистика источников прогнозов</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col" v-for="table_header in sources_headers" :colspan="table_header.colspan">{{table_header.title}}</th>
            </tr>
          </thead>
          <tbody>
          <tr>
                        <td></td><td></td>
                        <td v-for="subheader in subcolumns_sources">{{subheader}}</td>
                        <td v-for="subheader in subcolumns_sources">{{subheader}}</td>
                        <td v-for="subheader in subcolumns_sources">{{subheader}}</td>

          </tr>
            <tr v-for="tip_source in tip_sources">
              <th scope="row">{{tip_source.id}}</th>
              <td><a v-bind:href="'/tip_sources/' + tip_source.id">{{tip_source.title}}</a></td>
              <th scope="row">{{tip_source.days_7.profit}}</th>
              <td>{{tip_source.days_7.all}}</td>
              <td>{{tip_source.days_7.wins}}</td>
              <td>{{tip_source.days_7.loses}}</td>
              <td>{{tip_source.days_7.performance}}</td>
              <th scope="row">{{tip_source.days_30.profit}}</th>
              <td>{{tip_source.days_30.all}}</td>
              <td>{{tip_source.days_30.wins}}</td>
              <td>{{tip_source.days_30.loses}}</td>
              <td>{{tip_source.days_30.performance}}</td>
              <th scope="row">{{tip_source.days_year.profit}}</th>
              <td>{{tip_source.days_year.all}}</td>
              <td>{{tip_source.days_year.wins}}</td>
              <td>{{tip_source.days_year.loses}}</td>
              <td>{{tip_source.days_year.performance}}</td>

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>          

            <!-- Components modal --> 
            <div class="modal bd-example-modal-xl fade" id="serviceInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Информация о состоянии сервиса</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
          <thead>
          <p>Состояние компонентов</p>
            <tr>
              <th scope="col" v-for="table_header in components_headers" :colspan="table_header.colspan">{{table_header.title}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="component in components" v-bind:class="[!component.is_valid ? 'bg-danger' : '']">
              <td>{{component.id}}</td>
              <td >{{component.is_valid}}</td>
              <td>{{component.manager_name}}</td>
              <td>{{component.last_updated}}</td>

            </tr>
          </tbody>
        </table>
        
        <table class="table table-striped">
          <thead>
          <p>Состояние прокси-серверов</p>
            <tr>
              <th scope="col" v-for="table_header in system_proxies_headers" :colspan="table_header.colspan">{{table_header.title}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="system_proxy in system_proxies" v-bind:class="[!system_proxy.is_valid ? 'bg-danger' : '']">
              <th scope="row">{{system_proxy.country.country_name_en}}</th>
              <td >{{system_proxy.proxy_type}}</td>
              <td>{{system_proxy.ip}}</td>
              <td>{{system_proxy.port}}</td>
              <td>{{system_proxy.will_expire_at}}</td>

              <td>{{system_proxy.is_valid}}</td>


            </tr>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>          

                        <!-- Sportsbook Profiles modal --> 
            <div class="modal bd-example-modal-xl fade" id="financialInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Информация о состоянии счетов</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col" v-for="table_header in sportsbooks_accounts_headers" :colspan="table_header.colspan">{{table_header.title}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="sportsbooks_account in sportsbooks_accounts" v-bind:class="[!sportsbooks_account.is_active ? 'bg-danger' : '']">
              <td>{{sportsbooks_account.id}}</td>
              <td >{{sportsbooks_account.bookmaker.title}}</td>
              <td>{{sportsbooks_account.wallet.currency}}</td>
              <td>{{sportsbooks_account.wallet.balance}}</td>

              <td v-bind:class="[!sportsbooks_account.proxy.is_valid ? 'bg-danger' : '']">{{sportsbooks_account.proxy.ip}}:{{sportsbooks_account.proxy.port}}</td>
              <td>{{sportsbooks_account.is_active}}</td>

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>          

            
            </div>
        </div>
        <hr>
    </div>`
})

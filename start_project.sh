#!/bin/bash
# Установка базовых пакетов: docker, docker-compose
sudo apt-get install -y docker.io
sudo apt-get install postgresql postgresql-contrib
sudo systemctl start docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
docker --version

# Непосредственно сборка контейнеров, а затем в приложении
# выполняются миграции и пдогружаются фикстуры, стартовые данные (можно закомментировать, если фикстуры не нужны)
sudo docker-compose build
python manage.py collectstatic
sudo docker-compose run sportmind_web python manage.py makemigrations
sudo docker-compose run sportmind_web python manage.py migrate
sudo docker-compose run sportmind_web python manage.py load_fixtures


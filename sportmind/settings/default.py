import os
import sys
from datetime import timedelta

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(BASE_DIR)

SECRET_KEY = '@*1@h&td24u$w!@oa9f39bbidr69g$$79f6bi0mi3-p4+0v)y8'

DEBUG = os.environ.get('DEBUG', False)
ALLOWED_HOSTS = ['*']

CUSTOM_APPS = [
    'apps.common',
    'apps.authentication',
    'apps.data_collectors',
    'apps.rest',
    'apps.analysis',
    'apps.frontend',
    'apps.betting',
]

INSTALLED_APPS = [
    'rest_framework',
    'rest_framework.authtoken',
    'drf_yasg',

    'encrypted_model_fields',
    'django_extensions',
    'django_cleanup.apps.CleanupConfig',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
] + CUSTOM_APPS


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_NAME', 'sportmind_db'),
        'USER': os.environ.get('POSTGRES_USER', 'postgres'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD', 'postgrespass'),
        'HOST': os.environ.get('DB_HOST', '127.0.0.1'),
        'PORT': os.environ.get('DB_PORT', '5432'),
    }
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'sportmind.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'sportmind.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
AUTH_USER_MODEL = 'authentication.User'
LANGUAGE_CODE = 'RU-ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


LOG_DIR = os.path.join(PROJECT_ROOT, "logs")

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
    },
    'formatters': {
        'verbose': {
            'format': '%(asctime)s; %(process)d; %(thread)d; %(levelname)s; %(module)s; %(message)s'
        },
        'simple': {
            'format': '%(asctime)s; %(levelname)s; %(module)s; %(message)s'
        },
    },
    'handlers': {
        'error.log': {
            'level': 'ERROR',
            'formatter': 'verbose',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'error.log')
        },
        'analysis.log': {
            'level': 'INFO',
            'formatter': 'verbose',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'analysis.log')
        },
        'parsers.log': {
            'level': 'INFO',
            'formatter': 'verbose',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'parsers.log')
        },
        'debug.log': {
            'level': 'DEBUG',
            'formatter': 'verbose',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'debug.log')
        },

        'command_error.log': {
            'level': 'ERROR',
            'formatter': 'verbose',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'command_error.log')
        },
        'command_debug.log': {
            'level': 'DEBUG',
            'formatter': 'verbose',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'command_debug.log')
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['error.log', 'debug.log'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.command': {
            'handlers': ['command_error.log', 'command_debug.log'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.analysis': {
            'handlers': ['analysis.log'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.parsers': {
            'handlers': ['parsers.log'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}


STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, "site_static"),
]
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication'
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100,
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': timedelta(hours=9999999),
}

# Параметры, которые нужно указать в local_settings.py
# SPORTMONKS_TOKEN = ''
# SPORTMONKS_ROOT_URL = ''

# Данные группы ВК, от имени которой ведется рассылка в личные сообщения
# VK_GROUP_TOKEN = ''
# VK_GROUP_ID = ''
# VK_API_VERSION = ''

BETFAIR_APP_KEY = os.environ.get('BETFAIR_APP_KEY', '')
BETFAIR_USERNAME = os.environ.get('BETFAIR_USERNAME', '')
BETFAIR_PASSWORD = os.environ.get('BETFAIR_PASSWORD', '')

XSCORE_USERNAME = os.environ.get('XSCORE_USERNAME', '')
XSCORE_PASSWORD = os.environ.get('XSCORE_PASSWORD', '')


CERTIFICATE_PATH = os.environ.get('CERTIFICATE_PATH', os.path.join(PROJECT_ROOT, 'client-2048.crt'))
CERTIFICATE_KEY_PATH = os.environ.get('CERTIFICATE_KEY_PATH', os.path.join(PROJECT_ROOT, 'client-2048.key'))


# Для исправной работы двустороннего шифрования данных авторизации
FIELD_ENCRYPTION_KEY = os.environ.get('FIELD_ENCRYPTION_KEY', '')
RESULT_KEY = os.environ.get('RESULT_KEY', '')

# Настройки работы с фикстурами
FIXTURES_DIR = os.path.join(PROJECT_ROOT, 'new_fixtures')
FIXTURES_APPS = CUSTOM_APPS
FIXTURES_APPS.remove('apps.betting')

FIXTURES_EXCLUDED_MODELS = ['Tip', 'NewestLogFileContent', 'Match', 'Team',
                            'OddModel', 'Result', 'TeamAliasArray', 'TeamStatistic',
                            'XGMatchValue', 'XGParameter']

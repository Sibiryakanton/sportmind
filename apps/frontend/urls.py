from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html')),
    path('tip_sources/<int:tip_source_id>', TemplateView.as_view(template_name='tip_source_detail.html')),

]

from sportmind.celery import app
from apps.common.models import NewestLogFileContent, ProxySettings

from django.conf import settings
import os


def collect_logs():
    log_files = os.listdir(settings.LOG_DIR)
    for filename in log_files:
        response = os.popen('tail -n 100 {}'.format(os.path.join(settings.LOG_DIR, filename))).read()
        log_model = NewestLogFileContent.objects.get(log_filename=filename)
        if log_model.content != response:
            log_model.content = response
            log_model.save()


def check_proxies():
    proxies = ProxySettings.objects.all()
    for proxy in proxies:
        proxy.check_proxy()

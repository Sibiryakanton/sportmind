from enum import Enum


class BaseEnum(Enum):

    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)

    def dict(self):
        return dict(self.choices())

class EventStatusTypes:
    NOT_STARTED = 'NS'
    LIVE = 'LIVE'
    HALF_TIME = 'HT'
    FINISHED = 'FT'
    EXTRA_TIME = 'ET'
    PENALTY_SHOOTOUT = 'PEN_LIVE'
    FINISHED_AFTER_EXTRA_TIME = 'AET'
    REGULAR_TIME_FINISHED = 'BREAK'
    FULL_TIME_AFTER_PENALTIES = 'FT_PEN'
    CANCELLED = 'CANCL'
    POSTPONED = 'POSTP'
    INTERRUPTED = 'INT'
    ABANDONED = 'ABAN'
    SUSPENDED = 'SUSP'
    AWARDED = 'AWARDED'
    DELAYED = 'DELAYED'
    TO_BE_ANNOUNCED = 'TBA'
    WALKOVER = 'WO'
    AWAITING_UPDATED = 'DELAYED'

    CHOICES = (
        (NOT_STARTED, 'Not Started'),
        (LIVE, 'Live'),
        (HALF_TIME, 'Half-Time'),
        (FINISHED, 'Finished'),
        (EXTRA_TIME, 'Extra-Time'),
        (PENALTY_SHOOTOUT, 'Penalty Shootout'),
        (FINISHED_AFTER_EXTRA_TIME, 'Finished after extra time'),
        (REGULAR_TIME_FINISHED, 'Regular time finished'),
        (FULL_TIME_AFTER_PENALTIES, 'Full-Time after penalties'),
        (CANCELLED, 'Cancelled'),
        (POSTPONED, 'PostPoned'),
        (INTERRUPTED, 'Interrupted'),
        (ABANDONED, 'Abandoned'),
        (SUSPENDED, 'Suspended'),
        (AWARDED, 'Awarded'),
        (DELAYED, 'Delayed'),
        (TO_BE_ANNOUNCED, 'To Be Announced'),
        (WALKOVER, 'Walkover'),
        (AWAITING_UPDATED, 'Awaiting Updates'),
    )

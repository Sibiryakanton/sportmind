from apps.common.models import ProxySettings, ComponentInfo
from apps.data_collectors.enums import ServicesTypes
from requests.exceptions import ProxyError
import traceback


def request_with_proxy(method, service_name, *args, **kwargs):
    '''
    Без прокси не получится ни делать ставки, ни получать данные для создания прогнозов.
    При этом сами прокси нестабильны, в любой момент отдельно взятый прокси может отвалиться,
    и чтобы это не вытекало в ошибку, прекращающую работу команды в целом, я написал своего рода декоратор,
     который пробует сделать запрос с каждого доступного прокси, пока не получит результат.
     Примеры использования есть в проекте
    :param method: вызываемый метод запроса ( requests.get, requests.post и т.п.)
    :param service_name: Сервис, к которому предполагается делать запрос (примеры вариантов есть в ServiceTypes)

    :param args: значения без указателя, которые вы планировали передать в запрос requests
    :param kwargs: значения с указателем на конкретнеый аргумент, который должны быть переданы в requests.запрос
    :return: стандартный объект Response, возвращаемый методами requests
    '''
    if service_name not in ServicesTypes.choices().keys():
        raise AttributeError('The service_name argument value incorrect: must be variable from list in ServicesTypes')
    component = ComponentInfo.objects.get_or_create(manager_name=service_name)

    def wrapper(function, *request_args, **request_kwargs):
        public_proxies = ProxySettings.objects.get_public_proxies(component.countries.all())
        for i in public_proxies:
            try:
                proxy_data = i.dict_format()
                response = function(*request_args, **request_kwargs, proxies=proxy_data)
                return response
            except ProxyError:
                print(i, 'broken')
                i.is_valid = False
                i.save()
    return wrapper(method, *args, **kwargs)


def catch_manager_error(service):
    '''
    Мы должны иметь возможность быстро узнать, в порядке ли
    все используемые парсеры и апи-менеджеры четко, без просмотра логов. Идеальный вариант
    - это когда в случае ошибки в функции где-то изменяется статус работы
     класса на False и указывается, что и где сломалось. Для этого мы оборачиваем функции
     декоратором, котоырй отлавливает ошибки и меняет статус в админке в зависимости от результата
    '''
    def catch_manager_error_internal_decorator(method):
        def wrapper(*function_args, **function_kwargs):
            try:
                method(*function_args, **function_kwargs)
            except Exception as err:
                tb = traceback.format_exc()
                ComponentInfo.objects.update_or_create(manager_name=service,
                                                       defaults={'is_valid': False, 'traceback': tb,
                                                                 'data_args': function_args,
                                                                 'data_kwargs': function_kwargs})
                raise err
        return wrapper
    return catch_manager_error_internal_decorator

from django.core.management.base import BaseCommand

from django.conf import settings
import os
from django.core.management import call_command


class Command(BaseCommand):
    help = 'Dumping the fixtures in directory "new_fixtures" in project root with grouping by app name'

    def handle(self, *args, **options):
        new_fixtures_dir = settings.FIXTURES_DIR
        if os.path.exists(new_fixtures_dir):
            subdir_list = sorted(os.listdir(new_fixtures_dir), key=lambda x: int(x.split('_')[0]))
            for name in subdir_list:
                app_fixture_path = os.path.join(new_fixtures_dir, name)
                json_files = [pos_json for pos_json in os.listdir(app_fixture_path) if pos_json.endswith('.json')]
                sorted_json = sorted(json_files, key=lambda x: int(x.split('_')[0]))
                for file in sorted_json:
                    file_path = os.path.join(app_fixture_path, file)
                    call_command('loaddata', file_path)

from django.core.management import base
from apps.common.tasks import check_proxies


class Command(base.BaseCommand):
    help = 'Check the proxies'

    def handle(self, *args, **options):
        check_proxies()

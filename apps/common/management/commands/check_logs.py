from django.core.management import base
from apps.common.tasks import collect_logs


class Command(base.BaseCommand):
    help = 'Collect the project logs in db for admin paned displaying'

    def handle(self, *args, **options):
        collect_logs()

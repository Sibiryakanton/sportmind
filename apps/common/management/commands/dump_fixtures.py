from django.core.management.base import BaseCommand
from django.conf import settings
import os
from django.core.management import call_command
from django.apps import apps
import sys


class Command(BaseCommand):
    help = 'Dumping the fixtures in directory "new_fixtures" in project root with grouping by app name'

    def handle(self, *args, **options):
        new_fixtures_dir = settings.FIXTURES_DIR
        if not os.path.exists(new_fixtures_dir):
            os.mkdir(new_fixtures_dir)
        app_counter = 1
        for app in settings.FIXTURES_APPS:
            try:
                app_models = apps.get_app_config(app).get_models()
            except LookupError as e:
                app = str(e).split('\'')[-2]
                app_models = apps.get_app_config(app).get_models()
            app_new_fixtures_dir = os.path.join(settings.BASE_DIR, new_fixtures_dir, '{}_{}'.format(app_counter, app))
            if not os.path.exists(app_new_fixtures_dir):
                os.mkdir(app_new_fixtures_dir)
            fixture_counter = 1
            for model in app_models:
                model_name = model.__name__
                if model_name in settings.FIXTURES_EXCLUDED_MODELS:
                    continue
                model_path = '{}.{}'.format(app, model_name)
                fixture_name = '{}_{}_{}.json'.format(fixture_counter, app, model_name)
                sysout = sys.stdout
                sys.stdout = open(os.path.join(app_new_fixtures_dir, fixture_name), 'w')
                call_command('dumpdata', model_path, indent=4)
                sys.stdout = sysout
                fixture_counter += 1
            if not os.listdir(app_new_fixtures_dir):
                os.rmdir(app_new_fixtures_dir)
                continue
            app_counter += 1

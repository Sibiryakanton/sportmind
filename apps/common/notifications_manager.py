import requests
import json
import random
from django.conf import settings


class NotificationManager(object):
    '''
    Класс для реализации любых оповещений. СМС, оповещения в соцсетях, рассылки на почтовые ящики - все в таком духе
     должно реализовываться здесь
    '''
    @staticmethod
    def send_vk_message(message, user_id):
        '''
        Отправить сообщение ВК от имени группы
        :param message: Строковое значение с текстом сообщения
        :param user_id: Числовое значение для ID пользователя (в качестве значений берется только числовая часть,
        без префикса 'id'
        '''
        url = 'https://api.vk.com/method/messages.send'

        random_id = random.getrandbits(32)
        params = {
            'access_token': settings.VK_GROUP_TOKEN,
            'v': settings.VK_API_VERSION,
            'group_id': settings.VK_GROUP_ID,
            'random_id': random_id,
            'user_id': user_id,
            'message': message,
        }
        response = requests.post(url, params=params)
        response_string = response.content.decode('utf-8').replace('\\n', '')
        json_data = json.loads(response_string)

        if response.status_code != 200:
            print(json_data)

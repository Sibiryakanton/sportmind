from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.data_collectors.enums import ServicesTypes


class ComponentInfo(models.Model):
    '''
    Мы должны иметь возможность быстро узнать, в порядке ли все используемые парсеры и апи-менеджеры,
    работают ли они четко, без просмотра логов. Идеальный вариант - это когда в случае ошибки
    в функции мы увидим изменения в соответствующем разделе админки с деталями, что где, когда и почему сломалось.
    Для этого используется этот класс - декоратор catch_manager_error отлавливается любые исключения
    во время работы других классов и записывает их в админке с указанием деталей
    '''
    is_valid = models.BooleanField(default=True, verbose_name=_('Работает'))
    manager_name = models.CharField(max_length=100, choices=ServicesTypes.choices(), unique=True)
    traceback = models.TextField(null=True, blank=True)
    data_args = models.TextField(null=True, blank=True)
    data_kwargs = models.TextField(null=True, blank=True)

    countries = models.ManyToManyField('data_collectors.Country',
                                       verbose_name=_('Страны, у которых есть доступ к сервису'), blank=True,
                                       help_text=_('Используется при выборе прокси для подключения'))

    last_updated = models.DateTimeField(auto_now=True, verbose_name=_('Время последнего обновления'))

    def __str__(self):
        return '{} - {}'.format(self.manager_name, self.is_valid)

    class Meta:
        verbose_name_plural = _('Статусы компонентов')
        verbose_name = _('Статус компонента')

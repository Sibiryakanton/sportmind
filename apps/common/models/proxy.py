from django.db import models
from django.utils.translation import ugettext_lazy as _
import requests
from django.contrib.auth import get_user_model
from encrypted_model_fields.fields import EncryptedCharField
from apps.data_collectors.enums import ServicesTypes


class ProxyTypes:
    HTTPS = 'https'

    CHOICES = (
        (HTTPS, HTTPS),
    )


class ProxySettingsManager(models.Manager):
    def get_public_proxies(self, allowed_countries=None):
        '''
        Получить список прокси
        :param allowed_countries: Список стран (объектов Country)
        :return:
        '''
        queryset = super().filter(owner__isnull=True, proxy_type='https', is_valid=True)
        if allowed_countries is not None:
            queryset.filter(country__in=allowed_countries)
        return queryset


class ProxySettings(models.Model):
    owner = models.ForeignKey(get_user_model(), null=True, on_delete=models.SET_NULL, verbose_name=_('Владелец'),
                              blank=True, related_name='proxies')
    proxy_type = models.CharField(choices=ProxyTypes.CHOICES, max_length=50)
    login = EncryptedCharField(max_length=100, verbose_name=_('Логин доступа к прокси'))
    password = EncryptedCharField(max_length=200, verbose_name=_('Пароль доступа к прокси'))
    ip = models.GenericIPAddressField(verbose_name=_('IP прокси'))
    port = models.PositiveIntegerField(verbose_name=_('Номер порта'))
    will_expire_at = models.DateField(verbose_name=_('Оплачен до'), null=True, blank=True)
    country = models.ForeignKey('data_collectors.Country', verbose_name=_('Страна-хост прокси'),
                                null=True, on_delete=models.PROTECT)
    is_valid = models.BooleanField(default=True)

    objects = ProxySettingsManager()

    class Meta:
        verbose_name = _('Прокси')
        verbose_name_plural = _('Прокси')

    def clear_string(self):
        return '{}://{}:{}@{}:{}/'.format(self.proxy_type, self.login, self.password, self.ip, self.port)

    def dict_format(self):
        return {self.proxy_type: self.clear_string()}

    def check_proxy(self):
        try:
            requests.get('https://google.com', proxies=self.dict_format())
            self.is_valid = True
        except requests.exceptions.ProxyError:
            self.is_valid = False
        self.save()

    def __str__(self):
        return self.clear_string()

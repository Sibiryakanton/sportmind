from django.contrib import admin
from .models import ProxySettings, NewestLogFileContent, ComponentInfo


@admin.register(NewestLogFileContent)
class NewestLogFileContentAdmin(admin.ModelAdmin):
    list_display = ('pk', 'log_filename', 'last_updated')
    list_display_links = ('log_filename',)
    search_fields = ('log_filename', 'pk')

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(ProxySettings)
class ProxySettingsAdmin(admin.ModelAdmin):
    list_display = ('owner', 'country', 'proxy_type', 'ip', 'port', 'will_expire_at', 'is_valid')
    list_display_links = ('proxy_type', 'ip', 'port')
    search_fields = ('proxy_type', 'ip', 'port', 'is_valid')
    list_editable = ('is_valid',)


@admin.register(ComponentInfo)
class ComponentInfoAdmin(admin.ModelAdmin):
    list_display = ('id', 'manager_name', 'is_valid', 'last_updated')

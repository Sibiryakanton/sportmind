# Generated by Django 2.1 on 2019-12-22 08:41

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('betting', '0003_auto_20191204_0705'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sportsbookprofile',
            options={'verbose_name': 'Аккаунт букмекера', 'verbose_name_plural': 'Аккаунты букмекеров'},
        ),
        migrations.AlterModelOptions(
            name='sportsbookwallet',
            options={'verbose_name': 'Кошелек аккаунта букмекера', 'verbose_name_plural': 'Кошельки аккаунтов букмекеров'},
        ),
        migrations.AlterModelOptions(
            name='wallethistoryelem',
            options={'verbose_name': 'Элемент истории кошелька', 'verbose_name_plural': 'Элементы истории кошельков'},
        ),
        migrations.AlterField(
            model_name='sportsbookprofile',
            name='bookmaker',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='data_collectors.Bookmaker', verbose_name='Букмекер'),
        ),
        migrations.AlterField(
            model_name='wallethistoryelem',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Создано'),
        ),
        migrations.AlterField(
            model_name='wallethistoryelem',
            name='tip',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='placed_bets', to='analysis.Tip', verbose_name='Прогноз'),
        ),
    ]

# Generated by Django 2.1 on 2020-01-06 15:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('betting', '0007_wallethistoryelem_bet_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wallethistoryelem',
            name='tip',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='placed_bets', to='analysis.Tip', verbose_name='Прогноз'),
        ),
    ]

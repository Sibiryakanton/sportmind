# Generated by Django 2.1 on 2020-01-06 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('betting', '0004_auto_20191222_0841'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallethistoryelem',
            name='result_balance',
        ),
        migrations.AddField(
            model_name='wallethistoryelem',
            name='matched_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Подобрано'),
        ),
        migrations.AddField(
            model_name='wallethistoryelem',
            name='settled_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Проставлено'),
        ),
    ]

from django.core.management.base import BaseCommand
from apps.analysis.models import Tip


def activate_tips():
    '''
    Когда прогноз уже сгенерирован, имеет смысл перед самой ставкой дождаться удачного момента, лучшего коэффцииента.
     Если момент наступил, требуется собрать все зарегистрированные анкеты и от каждой сделать ставку на матч
    :return:
    '''
    tips = Tip.objects.filter(is_time_for_bet=False, tip_source__is_active=True)
    for tip in tips:
        tip.calculate_best_odd()


class Command(BaseCommand):
    help = 'launch the activate_tips'

    def handle(self, *args, **options):
        activate_tips()

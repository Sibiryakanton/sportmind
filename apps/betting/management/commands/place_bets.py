from django.core.management.base import BaseCommand
from apps.data_collectors.enums.bookmakers_controllers import BookmakerControllerTypes

from apps.betting.models import WalletHistoryElem
from apps.analysis.models import Tip
from apps.authentication.models import User


def place_bets():
    '''
    Общая команда, в которой идет перебор прогнозов. В зависимости от того, у какого букмекера
    лучший кэф, выбирается нужный api-менеджер
    :return:
    '''
    tips = Tip.objects.get_for_betting()
    users = User.objects.all()
    for user in users:
        profiles = user.profiles.all()
        for profile in profiles:
            user_wallet = profile.wallet
            user_bets = WalletHistoryElem.objects.filter(wallet=user_wallet, tip__match__result__isnull=True)
            user_betted_tips = [user_bet.tip for user_bet in user_bets]
            for tip in tips:
                if tip not in user_betted_tips:
                    bookmaker = tip.preferred_odd.odd_source.bookmaker
                    manager_name = bookmaker.component.manager_name
                    bookmaker_controller = BookmakerControllerTypes.CLASS_CHOICES[manager_name]()
                    bookmaker_profile = profiles.objects.filter(bookmaker=bookmaker).first()
                    bookmaker_controller.place_bet(bookmaker_profile, tip)


class Command(BaseCommand):
    help = 'launch the place_bets'

    def handle(self, *args, **options):
        place_bets()

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from encrypted_model_fields.fields import EncryptedCharField


class SportsbookProfile(models.Model):
    '''
    Модель для данных о профиле букмекера: логин, пароль,
    ключ и прокси для входа. Баланс и другие финансовые
    данные были вынесены в отдельную модель SportsbookWallet для логического разделения.
    '''
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, verbose_name=_('Владелец'),
                             related_name='profiles')
    bookmaker = models.ForeignKey('data_collectors.Bookmaker', on_delete=models.CASCADE, verbose_name=_('Букмекер'))
    login = EncryptedCharField(max_length=100, verbose_name=_('Логин'))
    password = EncryptedCharField(max_length=200, verbose_name=_('Пароль'))
    api_key = EncryptedCharField(max_length=500, verbose_name=_('API-ключ доступа'))
    proxy = models.ForeignKey('common.ProxySettings', null=True, verbose_name=_('Прокси'), on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=False, verbose_name=_('Использовать в автоматических запросах'))

    def get_balance(self):
        return '{} {}'.format(self.wallet.balance, self.wallet.currency)

    def __str__(self):
        return '{} {}'.format(self.user, self.bookmaker)

    class Meta:
        verbose_name_plural = _('Аккаунты букмекеров')
        verbose_name = _('Аккаунт букмекера')

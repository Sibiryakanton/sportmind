from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from ..enums import BetfairCurrencyMinStackTypes


def is_bet_percent_validator(value):
    if value > 5:
        raise ValidationError(
            _('Указанная доля от банка выше 5% - недопустимые риски'),
            params={'value': value},
        )


class SportsbookWallet(models.Model):
    profile = models.OneToOneField('SportsbookProfile', on_delete=models.CASCADE, verbose_name=_('Владелец'),
                                   related_name='wallet')
    balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    bet_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, null=True,
                                      verbose_name=_('Баланс на начало текущего месяца'))
    bet_balance_updated_at = models.DateTimeField(verbose_name=_('Последнее время обновления баланса для '
                                                                 'расчета размера ставки'), null=True, blank=True)
    currency = models.CharField(verbose_name=_('Валюта'), max_length=10, default='USD')
    bet_percent = models.PositiveIntegerField(verbose_name=_('Доля ставки от банка'), default=1,
                                              help_text=_('Не более 5'), validators=[is_bet_percent_validator])

    def calculate_bet_amount(self):
        '''
        Расчитать размер ставки
        :return:
        '''
        percent = self.bet_percent/100
        calc_bet_amount = float(self.bet_balance) * percent
        betfair_min_stack = BetfairCurrencyMinStackTypes.DICT[self.currency]
        if betfair_min_stack > calc_bet_amount:
            raise ValidationError(_('Расчитанный размер ставки меньше минимально допустимого со стороны BetFair. '
                                    'Измените настройки менеджера или пополните баланс'))
        if calc_bet_amount < self.balance - self.bet_balance:
            raise ValidationError(_('Расчитанный размер ставки меньше объема баланса, разрешенного для использования'))
        return calc_bet_amount

    def get_bookmaker(self):
        return self.profile.bookmaker

    def __str__(self):
        return '{} {}'.format(self.profile, self.balance)

    class Meta:
        verbose_name_plural = _('Кошельки аккаунтов букмекеров')
        verbose_name = _('Кошелек аккаунта букмекера')

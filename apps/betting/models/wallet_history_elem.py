from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now


class WalletHistoryElem(models.Model):
    '''
    Элемент истории ставок со счета
    '''
    bet_id = models.CharField(max_length=50, verbose_name=_('ID ставки в системе букмекера'), null=True)
    wallet = models.ForeignKey('SportsbookWallet', on_delete=models.CASCADE, verbose_name=_('Кошелек'),
                               related_name='history_elems')
    amount = models.DecimalField(max_digits=20, decimal_places=2, verbose_name=_('Сумма '))
    tip = models.ForeignKey('analysis.Tip', on_delete=models.PROTECT,
                            verbose_name=_('Прогноз'), related_name='placed_bets', null=True)
    price = models.DecimalField(max_digits=20, decimal_places=5,
                                verbose_name=_('Коэффициент на момент ставки'), null=True, blank=True)
    created_at = models.DateTimeField(default=now, verbose_name=_('Создано'))
    matched_at = models.DateTimeField(verbose_name=_('Подобрано'), null=True, blank=True)
    settled_at = models.DateTimeField(verbose_name=_('Проставлено'), null=True, blank=True)

    @property
    def profit(self):
        if self.tip.is_win:
            return self.amount * self.price
        else:
            return -self.amount

    class Meta:
        verbose_name_plural = _('Элементы истории кошельков')
        verbose_name = _('Элемент истории кошелька')

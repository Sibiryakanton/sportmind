from django.utils.translation import ugettext_lazy as _


class BetfairCurrencyMinStackTypes:
    '''
    На стороне Betfair есть лимит на минимальную сумму
    ставки. Здесь мы храним этот самый минимум, чтобы в случае,
    если расчитанный объем ставки по стратегии был меньше этой
    суммы, мы отправили минимум
    '''
    DICT = {
        'USD': 2,
        'GBP': 1,
        'EUR': 1,
        'AUD': 3,
    }

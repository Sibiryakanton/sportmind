from django.contrib import admin
from .models import WalletHistoryElem, SportsbookWallet, SportsbookProfile


class SportsbookWalletInline(admin.StackedInline):
    model = SportsbookWallet


@admin.register(SportsbookProfile)
class SportsbookProfileAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user', 'bookmaker', 'get_balance', 'is_active')
    list_editable = ('is_active',)
    list_display_links = ('pk', 'user')

    inlines = [SportsbookWalletInline]


# @admin.register(SportsbookWallet)
class SportsbookWalletAdmin(admin.ModelAdmin):
    list_display = ('pk', 'profile', 'get_bookmaker', 'balance', 'bet_balance', 'bet_percent', 'currency')
    list_display_links = ('pk', 'profile')


@admin.register(WalletHistoryElem)
class WalletHistoryElemAdmin(admin.ModelAdmin):
    list_display = ('pk', 'bet_id', 'wallet', 'amount', 'tip', 'price', 'profit')
    list_display_links = ('pk', 'bet_id', 'wallet', 'amount', 'tip', 'price', 'profit')

    def profit(self, obj):
        return obj.profit

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


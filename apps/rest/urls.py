from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework import routers

from .views import *
from .schemas import schema_view_api


router = routers.SimpleRouter()
router.register(r'tip_sources', TipSourceViewset, basename='tip_sources')
router.register(r'components', ComponentInfoViewset, basename='components')
router.register(r'user', UserViewset, basename='users')

urlpatterns = [
    path('get_tips/', GetTipsView.as_view(), name='get_tips'),
    path('get_leagues/', GetLeaguesView.as_view(), name='get_leagues'),
    path('get_time/', GetTimeInfoView.as_view(), name='get_time'),
    path('login/', obtain_jwt_token, name='get_token'),

    path('docs/', schema_view_api.with_ui('redoc', cache_timeout=0),
         name='api-swagger-ui'),
] + router.urls

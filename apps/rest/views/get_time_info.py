from rest_framework.views import APIView
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.openapi import Schema
from django.utils.timezone import now
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class GetTimeInfoView(APIView):
    @swagger_auto_schema(
        operation_id='Получить текущее время сервера',
        responses={200:  Schema(type='object',
                                properties={'time': Schema(type='string', title=_('Дата и время')),
                                            'timezone': Schema(type='string', title=_('Часовой пояс'))
                                            }
                                )},
        operation_description=''''''
    )
    def get(self, request, *args, **kwargs):
        response = {'time': now().strftime("%d.%m.%Y %H:%M:%S"), 'timezone': settings.TIME_ZONE}
        return Response(data=response)

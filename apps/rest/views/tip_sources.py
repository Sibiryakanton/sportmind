from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin
from drf_yasg.utils import swagger_auto_schema

from apps.rest.serializers import TipSourceSerializer, TipSourceRetrieveSerializer
from apps.analysis.models import TipSource


class TipSourceViewset(GenericViewSet, RetrieveModelMixin, ListModelMixin):
    queryset = TipSource.objects.filter(is_active=True)
    serializer_class = TipSourceSerializer

    @swagger_auto_schema(
        operation_id='Получить список моделей прогнозирования',
        responses={200: TipSourceSerializer(many=True)},
        operation_description=''''''
    )
    def list(self, request, *args, **kwargs):
        '''
        Я намеренно не использую вызов super(), т.к. в этом запросе мне не нужна пагинация
        '''
        response = TipSourceSerializer(self.queryset, many=True).data
        return Response(data=response)

    @swagger_auto_schema(
        operation_id='Получить информацию о модели прогнозирования',
        responses={200: TipSourceRetrieveSerializer()},
        operation_description='''Получить детальную информацию о модели прогнозирования,
         в частности разделенную статистику по каждой лиге'''
    )
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        response = TipSourceRetrieveSerializer(instance).data
        return Response(data=response)

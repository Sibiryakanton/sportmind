from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.mixins import ListModelMixin
from rest_framework.decorators import action
from drf_yasg.utils import swagger_auto_schema

from apps.common.models import ProxySettings

from apps.rest.serializers import UserSerializer, SportsbookProfileSerializer, ProxySettingsSerializer

from apps.authentication.models import User


class UserViewset(GenericViewSet, ListModelMixin):
    permission_classes = [AllowAny]
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer

    @swagger_auto_schema(
        operation_id='Получить информацию о текущем пользователе',
        responses={200: UserSerializer},
    )
    def list(self, request, *args, **kwargs):
        '''
        Получить информацию о текущем пользователе
        '''
        response = UserSerializer(self.request.user).data
        return Response(data=response)

    @swagger_auto_schema(
        operation_id='Получить список профилей в букмекерских конторах',
        responses={200: SportsbookProfileSerializer(many=True)},
    )
    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated])
    def get_profiles(self, request):
        profiles = request.user.profiles.all()
        data = SportsbookProfileSerializer(profiles, many=True).data
        return Response(data=data)

    @swagger_auto_schema(
        operation_id='Получить список системных прокси',
        responses={200: ProxySettingsSerializer(many=True)},
        operation_description=''''''
    )
    @action(methods=['GET'], detail=False, permission_classes=[IsAdminUser])
    def get_system_proxies(self, request):
        '''
        Получить список системных прокси
        '''
        proxies = ProxySettings.objects.filter(owner__isnull=True)
        data = ProxySettingsSerializer(proxies, many=True).data
        return Response(data=data)

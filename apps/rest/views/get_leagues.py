from rest_framework.views import APIView
from rest_framework.response import Response
from apps.rest.serializers.league_model import LeagueSerializer
from apps.data_collectors.models import League
from drf_yasg.utils import swagger_auto_schema


class GetLeaguesView(APIView):
    @swagger_auto_schema(
        operation_id='Получить список активных лиг',
        responses={200: LeagueSerializer(many=True)},
        operation_description=''''''
    )
    def get(self, request, *args, **kwargs):
        leagues = League.objects.filter(is_active=True).order_by('country', 'league_name')
        response = LeagueSerializer(leagues, many=True)
        return Response(data=response.data)

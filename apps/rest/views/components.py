from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema

from apps.rest.serializers import ComponentInfoSerializer
from apps.common.models import ComponentInfo


@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_id='Получить список компонентов сервиса',
    responses={200: ComponentInfoSerializer(many=True)},
    operation_description=''''''
))
class ComponentInfoViewset(GenericViewSet, ListModelMixin):
    queryset = ComponentInfo.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = ComponentInfoSerializer

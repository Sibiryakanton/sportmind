from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema

from apps.rest.serializers import GetTipsSerializer, TipSerializer


class GetTipsView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = GetTipsSerializer

    @swagger_auto_schema(
        operation_id='Получить список прогнозов по фильтру',
        responses={200: TipSerializer(many=True)},
        query_serializer=GetTipsSerializer,
        operation_description='''По умолчанию приходят все прогнозы всхе лиг и моделей на сегодняшнюю дату'''
    )
    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.GET)
        serializer.is_valid(raise_exception=True)
        tips = serializer.get_tips()
        response = TipSerializer(tips, many=True).data
        return Response(data=response)

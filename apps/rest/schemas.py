from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator


class RestGenerator(OpenAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super(RestGenerator, self).get_schema(request, public)
        api_type = self.url.split('/')[-2]
        schema.base_path = '/rest/{}'.format(api_type)
        return schema


schema_view_api = get_schema_view(
    openapi.Info(
      title="SportMind API",
      default_version='v28.02.2020',
      description="Описание API",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    generator_class=RestGenerator,
    urlconf='apps.rest.urls',
    url='https://127.0.0.1:8000/rest/'
)

from rest_framework import serializers
from apps.common.models import ProxySettings
from .country import CountrySerializer


class ProxySettingsSerializer(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = ProxySettings
        exclude = ['owner', 'login', 'password']

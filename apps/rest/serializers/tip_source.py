from rest_framework import serializers
from drf_yasg.utils import swagger_serializer_method
from datetime import timedelta, date
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from apps.analysis.models import TipSource
from apps.data_collectors.models import League

from .league_model import LeagueSerializer
from .tip_source_stat import TipSourceStatSerializer


class Days7Stat(TipSourceStatSerializer):
    pass


class Days30Stat(TipSourceStatSerializer):
    pass


class DayYearStat(TipSourceStatSerializer):
    pass


class TipSourceSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField()
    days_7 = serializers.SerializerMethodField()
    days_30 = serializers.SerializerMethodField()
    days_year = serializers.SerializerMethodField()

    @swagger_serializer_method(serializer_or_field=Days7Stat(
        help_text=_('Статистика модели за последние 7 дней')))
    def get_days_7(self, value):
        return value.get_stat(now()-timedelta(days=7), now())

    @swagger_serializer_method(serializer_or_field=Days30Stat(
        help_text=_('Статистика модели за последние 30 дней')))
    def get_days_30(self, value):
        return value.get_stat(now()-timedelta(days=30), now())

    @swagger_serializer_method(serializer_or_field=DayYearStat(
        help_text=_('Статистика модели за последний год')))
    def get_days_year(self, value):
        end_date = now().date()
        start_date = now().date() - timedelta(days=365)
        return value.get_stat(start_date, end_date)

    @swagger_serializer_method(serializer_or_field=serializers.CharField(help_text=_('Название матмодели')))
    def get_title(self, value):
        return str(value)

    class Meta:
        model = TipSource
        exclude = ['is_ready_for_betting', 'is_active', 'provider_class_name', 'math_model']

from rest_framework import serializers
from apps.data_collectors.models import Bookmaker


class BookmakerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bookmaker
        fields = '__all__'

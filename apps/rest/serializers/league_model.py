from rest_framework import serializers
from apps.data_collectors.models import League


class LeagueSerializer(serializers.ModelSerializer):
    country = serializers.SerializerMethodField()

    def get_country(self, obj):
        return obj.country.country_name_en

    class Meta:
        model = League
        fields = ('id', 'league_name', 'country')

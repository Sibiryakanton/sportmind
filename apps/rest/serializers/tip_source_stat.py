from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _


class TipSourceStatSerializer(serializers.Serializer):
    all = serializers.IntegerField(min_value=0, help_text=_('Общее кол-во прогнозов'))
    wins = serializers.IntegerField(help_text=_('Победы'))
    loses = serializers.IntegerField(help_text=_('Проигрыши'))
    performance = serializers.FloatField(help_text=_('Эффективность стратегии в процентах'))
    profit = serializers.FloatField(help_text=_('Относительную фффективность стратегии в сумме '
                                                'кэфов проигрышей и выигрышей'))

    class Meta:
        fields = '__all__'

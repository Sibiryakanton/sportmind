from rest_framework import serializers
from apps.betting.models import SportsbookProfile
from .proxy import ProxySettingsSerializer
from .bookmaker import BookmakerSerializer
from .sportsbook_wallet import SportsbookWalletSerializer


class SportsbookProfileSerializer(serializers.ModelSerializer):
    proxy = ProxySettingsSerializer()
    bookmaker = BookmakerSerializer()
    wallet = serializers.SerializerMethodField()

    def get_wallet(self, model_obj):
        return SportsbookWalletSerializer(model_obj.wallet).data

    class Meta:
        model = SportsbookProfile
        exclude = ['password', 'api_key', 'user']

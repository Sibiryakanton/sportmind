from rest_framework import serializers
from apps.betting.models import SportsbookWallet


class SportsbookWalletSerializer(serializers.ModelSerializer):

    class Meta:
        model = SportsbookWallet
        exclude = ['profile']

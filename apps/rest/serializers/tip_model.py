from rest_framework import serializers
from drf_yasg.utils import swagger_serializer_method
from django.utils.translation import ugettext_lazy as _
from apps.common.enums import EventStatusTypes
from apps.analysis.enums import TipStatusTypes
from apps.analysis.models import Tip

from .odds_model import OddSerializer


def match_statuses_description():
    statuses = dict(EventStatusTypes.CHOICES)
    status_as_strings_list = ['{} - {}'.format(status, statuses[status]) for status in statuses]
    statuses_string = ', '.join(status_as_strings_list)
    text = _('Статус матча. Варианты: {}'.format(statuses_string))
    return text


class TipSerializer(serializers.ModelSerializer):
    match = serializers.SerializerMethodField()
    country = serializers.SerializerMethodField()
    league = serializers.SerializerMethodField()
    tip_type = serializers.SerializerMethodField()
    event_datetime = serializers.SerializerMethodField()
    event_status = serializers.SerializerMethodField()
    odds = serializers.SerializerMethodField()
    result = serializers.SerializerMethodField()
    profit = serializers.SerializerMethodField()

    @swagger_serializer_method(serializer_or_field=serializers.CharField(
        help_text=_('Название матча в формате "ДОМАШНЯЯ vs ГОСТЕВАЯ')))
    def get_match(self, value):
        return '{} vs {}'.format(value.match.first_team.title, value.match.second_team.title)

    @swagger_serializer_method(serializer_or_field=serializers.CharField(
        help_text=_('Название страны')))
    def get_country(self, value):
        return value.match.league.country.country_name_en

    @swagger_serializer_method(serializer_or_field=serializers.CharField(
        help_text=_('Дата проведения матча в формате ДД.ММ.ГГГГ ЧЧ:ММ')))
    def get_event_datetime(self, value):
        return value.match.event_datetime.strftime("%d.%m.%Y %H:%M")

    @swagger_serializer_method(serializer_or_field=serializers.ChoiceField(
        choices=EventStatusTypes.CHOICES,
        help_text=match_statuses_description()))
    def get_event_status(self, value):
        return value.match.event_status

    @swagger_serializer_method(serializer_or_field=serializers.CharField(
        help_text=_('Название лиги')))
    def get_league(self, value):
        return value.match.league.league_name

    @swagger_serializer_method(serializer_or_field=serializers.CharField(
        help_text=_('Тип прогноза')))
    def get_tip_type(self, value):
        return str(value.tip_type)

    @swagger_serializer_method(serializer_or_field=OddSerializer(many=True,
                                                                 help_text=_('Тип прогноза')))
    def get_odds(self, value):
        odds = value.match.odds.filter(event_type=value.tip_type)
        if not len(odds):
            return [{'bookmaker': '--', 'value': value.first_odd}]
        return OddSerializer(odds, many=True).data

    @swagger_serializer_method(serializer_or_field=serializers.CharField(
        help_text=_('Итоговый счет матча')))
    def get_result(self, value):
        if value.match.result is not None:
            return str(value.match.result)

    @swagger_serializer_method(serializer_or_field=serializers.DecimalField(
        max_digits=8, decimal_places=4,
        help_text=_('Профит прогноза')))
    def get_profit(self, value):
        if value.match.result is not None:
            if value.preferred_odd is not None:
                odd = value.preferred_odd.value
            else:
                odd = value.first_odd
            if value.tip_status == TipStatusTypes.WIN:
                return odd - 1
            elif value.tip_status == TipStatusTypes.LOSE:
                return -1.00

    class Meta:
        model = Tip
        fields = ('event_datetime', 'event_status', 'match', 'league', 'tip_type', 'country', 'odds',
                  'tip_status', 'result', 'profit')

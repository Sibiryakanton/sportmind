from rest_framework import serializers
from drf_yasg.utils import swagger_serializer_method
from datetime import timedelta, date
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from apps.analysis.models import TipSource
from apps.data_collectors.models import League

from .league_model import LeagueSerializer
from .tip_source import TipSourceSerializer
from .tip_source_stat import TipSourceStatSerializer


class LeagueStatSerializer(serializers.Serializer):
    '''
    Сериализатор статистики мдоели прогнозирования в рамках конкретной лиги.
     Применяется только для отображения структуры в генерируемой документации drf-yasg
    '''
    league = LeagueSerializer(help_text=_('Информация о лиге'))
    tip_stat = TipSourceStatSerializer(many=True,
                                       help_text=_('Статистика модели по '
                                                   'лиге с разбиением на 7, 30 и 365 дней'))

    class Meta:
        fields = '__all__'


class TipSourceRetrieveSerializer(TipSourceSerializer):
    league_stats = serializers.SerializerMethodField()

    @swagger_serializer_method(serializer_or_field=LeagueStatSerializer(many=True))
    def get_league_stats(self, value):
        leagues_data = []
        leagues = League.objects.get_using_leagues()
        today = now().date()

        for league in leagues:
            start_date = date(year=today.year-1, month=today.month, day=today.day)
            tip_stats = value.get_stat(start_date, today, league=league)
            if tip_stats['all']:
                league_data = {'league': LeagueSerializer(league).data, 'tip_stat': tip_stats}
                leagues_data.append(league_data)

        return sorted(leagues_data, key=lambda x: x['tip_stat']['profit'], reverse=True)

    class Meta:
        model = TipSource
        fields = '__all__'


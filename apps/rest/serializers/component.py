from rest_framework import serializers
from apps.common.models import ComponentInfo


class ComponentInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComponentInfo
        exclude = ['traceback', 'data_args', 'data_kwargs', 'countries']

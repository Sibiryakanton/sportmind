from django.db.models import Q
from django.utils.timezone import now
from rest_framework import serializers
from apps.data_collectors.models import League
from apps.analysis.models import TipSource, Tip
from django.utils.translation import ugettext_lazy as _


class GetTipsSerializer(serializers.Serializer):
    event_date = serializers.DateField(default=None, help_text=_('Дата события'))
    league = serializers.PrimaryKeyRelatedField(queryset=League.objects.filter(is_active=True),
                                                default=None, help_text=_('Лига'))
    tip_source = serializers.PrimaryKeyRelatedField(queryset=TipSource.objects.all(),
                                                    default=None, help_text=_('Источник прогноза'))

    def get_tips(self):
        data = self.validated_data

        filters = Q(tip_source__is_active=True, preferred_odd__isnull=False)
        if data['event_date'] is not None:
            filters &= Q(match__event_datetime__date=data['event_date'])
        else:
            filters &= Q(match__event_datetime__date=now().date())
        if data['tip_source'] is not None:
            filters &= Q(tip_source=data['tip_source'])

        if data['league'] is not None:
            filters &= Q(match__league=data['league'])
        tips = Tip.objects.filter(filters).order_by('match__event_datetime')
        return tips

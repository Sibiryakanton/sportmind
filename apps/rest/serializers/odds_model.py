from rest_framework import serializers
from drf_yasg.utils import swagger_serializer_method
from django.utils.translation import ugettext_lazy as _

from apps.data_collectors.models import OddModel


class OddSerializer(serializers.ModelSerializer):
    bookmaker = serializers.SerializerMethodField()
    value = serializers.DecimalField(max_digits=8, decimal_places=2, help_text=_('Значение коэффициента'))

    @swagger_serializer_method(serializer_or_field=serializers.CharField(help_text=_('Источник коэффициента')))
    def get_bookmaker(self, value):
        source = value.odd_source
        title = source.bookmaker.title
        if source.title is None:
            return 'API {}'.format(title)
        return value.odd_source.bookmaker.title

    class Meta:
        model = OddModel
        fields = ('value', 'bookmaker')

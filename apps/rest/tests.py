from rest_framework.test import APITestCase
from django.shortcuts import reverse
from django.conf import settings

from apps.analysis.models import TipSource

import os


def get_fixtures(path=settings.FIXTURES_DIR):
    '''
    Собрать список фикстур из new_fixtures для использования (архив игнорируем)
    :param path:
    :return:
    '''
    result = []
    for name in os.listdir(path):
        obj_path = os.path.join(path, name)
        if name.endswith('.zip'):
            continue
        elif os.path.isfile(obj_path):
            files_list = [obj_path]
        else:
            files_list = get_fixtures(obj_path)
        result += files_list
    return result


class Tests(APITestCase):
    '''
    ВНИМАНИЕ: в папке new_fixtures часть
    "тяжелых" фикстур вынесены в архив. Перед тестами необходимо
     извлечь их в папку new_fixtures
    '''
    fixtures = get_fixtures()
    user_data = {'username': 'admin', 'password': 'password123!'}

    def authorize(self):
        token_request = self.client.post(reverse('get_token'), self.user_data)
        token = token_request.json().get('token')
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)

    def test_get_actual_tips(self):
        url = reverse('get_tips')
        data = {'event_date': '2019-12-20'}
        response = self.client.get(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 30)

    def test_get_leagues(self):
        url = reverse('get_leagues')
        data = {'event_date': '2019-12-20'}
        response = self.client.get(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.json()[0].keys()), ['id', 'league_name', 'country'])

    def test_get_time_info(self):
        url = reverse('get_time')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.json().keys()), ['time', 'timezone'])

    def test_tip_sources(self):
        url = reverse('tip_sources-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_tip_sources_detail(self):
        source = TipSource.objects.filter(is_active=True).first()
        url = reverse('tip_sources-detail', args=[source.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_current_user(self):
        url = reverse('users-list')
        response = self.client.get(url)
        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['is_active'], False)
        self.authorize()
        response = self.client.get(url)
        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['is_superuser'], True)

    def test_get_bookmaker_profiles(self):
        url = reverse('users-get-profiles')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_get_system_proxies(self):
        url = reverse('users-get-system-proxies')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_get_components(self):
        url = reverse('components-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 401)
        self.authorize()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

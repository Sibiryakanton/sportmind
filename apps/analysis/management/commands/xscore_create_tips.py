from django.core.management.base import BaseCommand
from apps.analysis.xscore_classifier import XGScoreClassifier, ModeTypes
from apps.analysis.models import Tip, TipSource


def xscore_create_tips(mode):
    '''
    :param production_mode: Если включено, брать будет только непрошедшие матчи (генерация "боевых" прогнозов)
    :param sandbox_mode: если включено, то мы просто хотим посмотреть эффективность новых моделей
    :return:
    '''
    classifier_manager = XGScoreClassifier()
    classifier_manager.create_tips(mode)
    # Блок закомментирован и используется ситуативно - когда надо быстро сбросить прогнозы по экспериментальным моделям
    # tip_source = TipSource.objects.filter(is_ready_for_betting=True)
    # Tip.objects.exclude(tip_source__in=tip_source).delete()


class Command(BaseCommand):
    help = 'launch the xscore_classifier'

    def add_arguments(self, parser):
        parser.add_argument('mode', type=str, nargs='?', default=ModeTypes.PRODUCTION)

    def handle(self, *args, **options):
        mode = options['mode']
        if mode in [ModeTypes.SANDBOX, ModeTypes.PRODUCTION]:
            xscore_create_tips(mode)
        else:
            print('A mode with that name does not exist.')

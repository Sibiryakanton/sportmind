from django.core.management.base import BaseCommand
from apps.analysis.xscore_classifier import XGScoreClassifier


class Command(BaseCommand):
    help = 'launch the xscore_classifier'

    def handle(self, *args, **options):
        # current_event_for_tips меняется в зависимости от того, на какой тип
        # ставки должны быть ориентированы обучаемые модели
        current_event_for_tips = ['is_total_over25', 'is_both_scored', 'is_ht_total_over15', 'winner']

        classifier_manager = XGScoreClassifier(min_efficiency=0.55)
        matches, results = classifier_manager.prepare_match_data()
        for event_type in current_event_for_tips:
            classifier_manager.train_classifiers_group(event_type, matches_data=matches, matches_results=results)
        classifier_manager.test_xg_classifiers_models()

from django.core.files import File
from django.utils.timezone import now
from django.db.models import Q
from sklearn.gaussian_process import (GaussianProcessClassifier, GaussianProcessRegressor)
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor, ExtraTreeClassifier, ExtraTreeRegressor
from sklearn.ensemble import (AdaBoostClassifier, BaggingClassifier, ExtraTreesClassifier,
                              GradientBoostingClassifier, RandomForestClassifier, AdaBoostRegressor,
                              ExtraTreesRegressor, GradientBoostingRegressor, IsolationForest, RandomForestRegressor)
from sklearn.svm import SVC
from sklearn.linear_model import LinearRegression, LogisticRegression, LogisticRegressionCV, SGDClassifier

from apps.data_collectors.models import Match, XGParameter, EventMarketType
from apps.analysis.models import MachineLearningModel, TipSource, Tip
from apps.analysis.enums import TipSourceTypes
from collections import OrderedDict
from datetime import timedelta
import pickle
import os
import logging
import json
import importlib
import numpy
from matplotlib import pyplot

log = logging.getLogger('django.analysis')


class ModeTypes:
    PRODUCTION = 'production'
    SANDBOX = 'sandbox'
    CHECK_STAT = 'check_stat'
    COLLECT_TRAINING = 'collect_training_set'


class XGScoreClassifier:
    def __init__(self, min_efficiency=0.58):
        '''
        :param min_efficiency: минимальная эффективность, которая нужна для эффективных ставок на событие. 1% = 0.01
        '''
        self.min_efficiency = min_efficiency

        # Набор матмоделей, которые испытываются на выборе данных
        self.classifier_class = [SVC, GaussianProcessClassifier, DecisionTreeClassifier,
                                 ExtraTreeClassifier, AdaBoostClassifier, BaggingClassifier,
                                 ExtraTreesClassifier, GradientBoostingClassifier,
                                 RandomForestClassifier, AdaBoostRegressor, IsolationForest,
                                 LogisticRegression, LogisticRegressionCV, SGDClassifier]
        self.classifier_class_args = {SVC: {'gamma': 'auto'}}

    def create_tips(self, mode):
        '''
        Функция, ради которой написаны все остальные - создание готовых прогнозов на будущие матчи
        :param mode: Варианты: production и sandbox: генерация "боевых" прогнозов или режим песочницы.
        Режим песочницы значит, что мы хотим быстро узнать эффективность алгоритмов, не дожидаясь
        будущих матчей. То есть мы берем матчи, на которые есть кэфы событий, используем из для прогнозов,
         отмечая сначала как актуальные, а потом тут же пробегаемся и проверяем каждый прогноз, отмечая в
         соответствии с результатом как проигрышный/выигрышный
        :return:
        '''
        tip_sources = TipSource.objects.filter(is_active=True, source_category=TipSourceTypes.INTERNAL.name,
                                               math_model__xg_param__isnull=False)
        if mode == ModeTypes.SANDBOX:
            tip_sources = tip_sources.filter(tips__isnull=True)
        matches_data = self.prepare_match_data(mode)[0]
        print('@@', len(matches_data))
        for tip_source in tip_sources:
            print(tip_source)
            ml_model = tip_source.math_model
            event_type = ml_model.title.split('__')[2]
            model_file = ml_model.file.path
            with open(model_file, 'rb') as clf_file:
                clf = pickle.load(clf_file)
                xg_param_names = [elem.full_name() for elem in ml_model.xg_param.all()]
                matches_xgdata = self.__extract_values(matches_data, [], xg_param_names, event_type,
                                                       is_production_mode=True)[0]
                for match in matches_xgdata:
                    if 0.00 in match:
                        continue
                    match_prediction = clf.predict([match[:-1]])
                    # Ищем, какой опции маркета соответствует прогноз классификатора
                    market_option = ml_model.market_type.runners.get(xg_clf_alias=match_prediction[0])
                    match_obj = Match.objects.get(pk=match[-1])
                    event_odd = match_obj.odds.filter(event_type=market_option).order_by('value').first()
                    if event_odd is None:
                        continue
                    else:
                        event_odd_value = event_odd.value
                    tip_data = {'first_odd': event_odd_value}
                    tip = Tip.objects.get_or_create(match=match_obj, tip_type=market_option,
                                                    tip_source=tip_source, defaults=tip_data)[0]
                    if mode == ModeTypes.SANDBOX:
                        tip.update_tip_status()

    @property
    def testing_models_dir(self):
        '''
        Папка, в которой будут сохраняться матмодели, которые прошли первичное тестирование.
        :return:
        '''
        dumps_dir = os.path.join(os.getcwd(), 'models_for_testing')
        if not os.path.exists(dumps_dir):
            os.makedirs(dumps_dir)
        return dumps_dir

    def train_classifiers_group(self, event_type, combine_mode=False, matches_data=None, matches_results=None):
        '''
        Создание датасетов для тренировки и тестов
        Тренировка классификаторов по xg-данным
        Сохранение эффективных моделей моделей в папку models_for_testing (если в папке уже есть модель
        с аналогичным названием, произойдет перезапись)
        На этом этапе модели еще только просто складируются в папке, но не вводятся в систему "официально",
        а дожидаются повторного теста на ранее неизвестных для них матчах
        :param event_type: Тип исхода события (название соответствующей функции у Result
        :param combine_mode: Если True, функция запущена с целью создать матмодели, принимающие несколько
        xg-параметров на вход
        :param matches_data:
        :param matches_results: Оба параметра аналогичны тем, что возвращает метод prepare_match_data()
        :return:
        '''
        self.__classifier_log('w', 'Time of start: {}\n'.format(now()))
        if matches_data is None or matches_results is None:
            matches_data, matches_results = self.prepare_match_data(ModeTypes.COLLECT_TRAINING)

        xg_params_list = XGParameter.objects.all()
        clf_results = {}
        for xg_param in xg_params_list:
            xg_names = [xg_param.full_name()]
            matches_xgdata, matches_xgresults = self.__extract_values(matches_data, matches_results,
                                                                      xg_names, event_type)
            print('Training set size: {} {}'.format(len(matches_xgdata), len(matches_xgresults)))
            clf_list_info = self.__train_classifier(xg_names, matches_xgdata, matches_xgresults,
                                                    event_type, combine_mode)
            if combine_mode:
                # Первым циклом мы просто упаковываем результаты обучения моделей абы как, второй
                # раз - сортируем по убыванию
                for clf_info in clf_list_info:
                    clf_model_name = clf_info[1]
                    if clf_model_name not in clf_results.keys():
                        clf_results[clf_model_name] = []
                    clf_info_as_dict = {'xg_name': clf_info[0], 'performance': clf_info[-1]}
                    clf_results[clf_model_name].append(clf_info_as_dict)
                for clf_name, clf_report in clf_results.items():
                    if len(clf_report):
                        sorted_report = sorted(clf_report, key=lambda k: float(k['performance']), reverse=True)
                        clf_results[clf_name] = sorted_report[:5]

        if combine_mode:
            # Далее мы будем объявлять классы, ориентируясь на их название в виде
            # строки, поэтому понадобится адрес модуля
            module_name = "apps.analysis.xscore_classifier"

            for clf_name, clf_report in clf_results.items():
                xg_names = [xg['xg_name'] for xg in clf_report]
                matches_xgdata, matches_xgresults = self.__extract_values(matches_data, matches_results,
                                                                          xg_names, event_type)
                clf_class = getattr(importlib.import_module(module_name), clf_name)
                self.classifier_class = [clf_class]
                self.__train_classifier(xg_names, matches_xgdata, matches_xgresults, event_type)
        self.__classifier_log('a', 'Time of completing: {}'.format(now()))

    def test_xg_classifiers_models(self):
        '''
        Функция берет уже обученные классификаторы из папки models_for_testing, потом отдельно берет пачку матчей,
        дробит их на 10 частей и по очереди закидывает их в функцию прогнозирования. Полученная эффективность кладется
        в список с остальными, а потом на его основе строится график
        Если модель проходит проверку новой базой на миниальном входном уровне, она вносится в систему
        деактивированной (решение, стоит ли включать систему, админ принимает сам на основе общей эффективности
        и статистики теста, сохраненной в отдельном поле)
        :return:
        '''
        self.__classifier_log('w', 'Time of start: {}\n'.format(now()))
        dumps_dir = self.testing_models_dir
        models_name_list = os.listdir(dumps_dir)
        if not len(models_name_list):
            print('There is no models for testing')
            os.removedirs(os.path.join(dumps_dir))
            return
        matches_data, matches_results = self.prepare_match_data(ModeTypes.CHECK_STAT)
        matches_packs = self.__split(matches_data)
        matches_results_packs = self.__split(matches_results)

        for model_filename in models_name_list:
            with open(os.path.join(dumps_dir, model_filename), 'rb') as clf_file:
                model_filename_list = model_filename.split('__')
                clf = pickle.load(clf_file)
                results_list = []
                xg_params_string = model_filename_list[0]
                xg_params_list = xg_params_string.split('&')

                model_performance = float(str(model_filename_list[-1]).replace('.pkl', ''))
                tip_type = model_filename_list[2]
                for index in range(10):
                    matches_xgdata, matches_xgresults = self.__extract_values(matches_packs[index],
                                                                              matches_results_packs[index],
                                                                              xg_params_list, tip_type)
                    match_prediction = clf.predict(matches_xgdata)
                    accuracy = self.__comparing_exact_values(match_prediction, matches_xgresults)
                    results_list.append(accuracy)
                mean, dispersion = self.__calc_mean_and_dispersion(results_list)
                name_accuracy_len = '{} {} {} {} \n'.format(xg_params_string, mean, dispersion, str(results_list))
                log.info(name_accuracy_len)
                self.__classifier_log('a', name_accuracy_len)
                if mean < self.min_efficiency-0.01 or mean-dispersion < self.min_efficiency-0.1:
                    log.info('{} is failed. Reason: mean {}, dispersion {}'.format(model_filename, mean, dispersion))
                else:
                    xg_params_ids = [xg_param.split('_')[-1] for xg_param in xg_params_list]
                    xg_params = XGParameter.objects.filter(pk__in=xg_params_ids)
                    model_file = File(clf_file)
                    market = EventMarketType.objects.get(xg_clf_alias=tip_type)
                    defaults = {'title': model_filename, 'stat_test_info': str(results_list)}
                    model = MachineLearningModel.objects.update_or_create(performance=model_performance,
                                                                          model_name=model_filename_list[1],
                                                                          market_type=market, defaults=defaults)[0]
                    model.xg_param.set(xg_params)
                    model.file.save(model_filename, model_file)
                    model.save()
                    internal = TipSourceTypes.INTERNAL.name
                    TipSource.objects.get_or_create(source_category=internal, math_model=model)
                os.remove(os.path.join(dumps_dir, model_filename))
        os.removedirs(os.path.join(dumps_dir))

    def __train_classifier(self, xg_names, matches_xgdata, matches_xgresults, result_type, combine_mode=False):
        '''
        На основе присланных данных обучить классификатор
        :param xg_names:
        :param matches_xgdata:
        :param matches_xgresults:
        :param combine_mode: Если True, запрос используется не для создания моделей, а для общего сбора информации о том
        :return: Два значения: классификатор и его эффективность
        '''
        # Разделяем выборку на обучающую и тестовую
        data_border_index = len(matches_xgdata) // 10
        train_matches, train_results = matches_xgdata[:-data_border_index], matches_xgresults[:-data_border_index]
        test_matches, test_results = matches_xgdata[-data_border_index:], matches_xgresults[-data_border_index:]

        classifiers_info = []
        if len(train_matches):
            for clf_class in self.classifier_class:
                if clf_class not in self.classifier_class_args.keys():
                    clf = clf_class()
                else:
                    clf = clf_class(**self.classifier_class_args[clf_class])
                clf.fit(train_matches, train_results)
                match_prediction = clf.predict(test_matches)
                accuracy = self.__comparing_exact_values(match_prediction, test_results)

                xg_names_as_string = '&'.join(xg_names)
                name_accuracy_len = '{} {} {} \n'.format(xg_names_as_string, accuracy,
                                                         '(кол-во матчей в тренировке: {})'.format(len(train_matches)))
                self.__classifier_log('a', name_accuracy_len)
                clf_info = [xg_names_as_string, clf_class.__name__, result_type, str(accuracy)]
                clf_name = '__'.join(clf_info).replace('/', '')
                filename = '{}.pkl'.format(clf_name)
                log.info('trained {}'.format(filename))
                if accuracy >= self.min_efficiency:
                    classifiers_info.append(clf_info)
                    if not combine_mode:
                        with open(os.path.join(self.testing_models_dir, filename), 'wb') as inf:
                            pickle.dump(clf, inf)
            return classifiers_info

    def prepare_match_data(self, collect_type):
        '''
        Подготовить общий массив с xg-данными по интересующим матчам
        :param collect_type: Тип сбора матчей, зависит от этапа подготовки моделей:

            production_mode: если мы уже создаем прогнозы с моделями, прошедшими первичное тестирование
            check_stat_mode: если запрос используется для быстрой проверки уже созданных моделей
            sandbox_mode: если мы хотим провести первичное тестирование, создав прогнозы по матчам,
             у которых есть кэфы на события (полезно для оценки не только доли
              успешных прогнозов, но и примерной прибыли)
        :return:
        '''
        check_stat_matches_filter = Q(event_datetime__month__gte=11, event_datetime__year=2019)
        if collect_type == ModeTypes.SANDBOX:
            # for test with creating tips
            matches = Match.objects.filter(odds__isnull=False,
                                           result__isnull=False).order_by('event_datetime').distinct().order_by('?')
        elif collect_type == ModeTypes.CHECK_STAT:
            # for test without creating tips
            matches = Match.objects.filter(check_stat_matches_filter, result__isnull=False).order_by('?')
        elif collect_type == ModeTypes.COLLECT_TRAINING:
            # for training set
            matches = Match.objects.filter(result__isnull=False,
                                           odds__isnull=True).exclude(check_stat_matches_filter).order_by('?')
        elif collect_type == ModeTypes.PRODUCTION:
            min_limit = now()+timedelta(days=7)
            max_limit = now()+timedelta(minutes=30)
            matches = Match.objects.filter(result__isnull=True, event_datetime__lte=min_limit,
                                           event_datetime__gte=max_limit).order_by('?')
        else:
            raise AttributeError('The collect_type should not have a value {}'.format(collect_type))
        print(len(matches))

        matches_data = []
        matches_results = []
        for i in matches:
            if not isinstance(i.xgdata_status(), bool):
                continue
            f_match_data, s_match_data, result = i.get_xscore_classifier_data()
            match_data = list()
            match_data.append(f_match_data)
            match_data.append(s_match_data)
            match_data.append(i.pk)
            if 0.00 not in match_data:
                matches_data.append(match_data)
                matches_results.append(result)
            self.__control_test_log('в пачке {} матчей, {} осталось подготовить'.format(len(matches_data),
                                                                                        len(matches) - len(matches_data)
                                                                                        ))
        return matches_data, matches_results

    def __extract_values(self, matches_data, matches_results, xg_params_list, result_type, is_production_mode=False):
        '''
        Пробежаться по данным о матчах и результатам и выцепить из первых только конкретные xg-данные, а из вторых
        - только конкретные атрибуты результата
        :param matches_data: список словарей с xg-данными о матчах
        :param matches_results: список объектов Result матчей
        :param xg_params_list: список названий всех xg-атрибутов, используемых в классификаторе
        :return:
        '''
        matches_xgdata, matches_xgresults = [], []
        for index, match_info in enumerate(matches_data):
            for xg_name in xg_params_list:
                try:
                    home_team_xg_value = match_info[0][xg_name]
                    away_team_xg_value = match_info[1][xg_name]
                    if 0 in [home_team_xg_value, away_team_xg_value]:
                        continue
                except KeyError:
                    continue
                match_data = [home_team_xg_value, away_team_xg_value]
                if is_production_mode:
                    match_data.append(match_info[2])
                    matches_xgdata.append(match_data)
                if not is_production_mode:
                    result = matches_results[index]
                    result_type_value = getattr(result, result_type)()
                    if result_type_value is None:
                        continue
                    matches_xgresults.append(getattr(result, result_type)())
                    matches_xgdata.append(match_data)

                matches_left = len(matches_data) - len(matches_xgdata)
                self.__control_test_log('{}, в пачке {} матчей,{} осталось'.format(xg_name, len(matches_xgdata),
                                                                                   matches_left))
        return matches_xgdata, matches_xgresults

    def __classifier_log(self, write_type, string):
        with open('testlog.txt', write_type) as inf:
            print(string)
            inf.write(string)

    def __control_test_log(self, string):
        '''
        Сменить запись в файле control_test
        :param string: новая строка статуса работы программы
        :return:
        '''
        with open('control_test.txt', 'w') as inf:
            inf.write(string)

    def __split(self, elems_list, n=10):
        '''
        Разбить список elems_list на n списков
        '''
        elems_list_len = len(elems_list)
        delimeter = round(elems_list_len/n)
        index_list = [delimeter*i for i in range(n+1)]
        index_list[-1] += (elems_list_len - index_list[-1]) - 1
        elems_packs = []
        for index, start_index in enumerate(index_list):
            if start_index == index_list[-1]:
                break
            end_index = index_list[index+1]
            pack = elems_list[start_index: end_index]
            elems_packs.append(pack)
        return elems_packs

    def __calc_mean_and_dispersion(self, array):
        '''
        Расчитать среднее и отклонение результатов вторичного тестирования на выборке
        :param array:
        :return:
        '''
        mean = sum(array) / len(array)
        quadratic_dimensions = [(i-mean)**2 for i in array]
        quadratic_dispersion = sum(quadratic_dimensions) / len(quadratic_dimensions)
        dispersion = quadratic_dispersion**(1/2)
        return mean, dispersion

    def __comparing_exact_values(self, predict, real):
        '''
        Метод сравнения прогнозов с реальными результатами, при котором нужно точное совпадение (к примеру,
        при бинарных прогнозах "будет / не будет")
        :param predict:
        :param real:
        :return:
        '''
        success = 0
        predict_list = list(predict)
        for index, value in enumerate(real):
            if value == predict_list[index]:
                success += 1
        return round(success/len(real), 4)

    def get_plot(self, checked_value, event_type):
        '''
        Сортировка значений по вхождениям в диапазоны
        :param checked_value: Контрольный тип ответа (к примеру, есть у исхода
        варианты Home Draw Away, один из них может быть checked_value
        :param event_type:
        :return:
        '''
        self.__classifier_log('w', 'Time of start: {}\n'.format(now()))
        json_reports_dir = os.path.join(os.getcwd(), 'schedules_reports')
        if not os.path.exists(json_reports_dir):
            os.makedirs(json_reports_dir)
        schedules_dir = os.path.join(os.getcwd(), 'schedules')
        if not os.path.exists(schedules_dir):
            os.makedirs(schedules_dir)

        matches_data, matches_results = self.prepare_match_data(ModeTypes.COLLECT_TRAINING)
        interval = 0.5
        array_for_sorting = numpy.arange(-10, 10, interval)

        xg_params_list = XGParameter.objects.all()
        for xg_param in xg_params_list:
            xg_name = [xg_param.full_name()]
            matches_xgdata, matches_xgresults = self.__extract_values(matches_data, matches_results,
                                                                      xg_name, event_type)
            result_x = []
            result_y = []
            result_y_full = []
            for elem in array_for_sorting:
                result_x.append(elem)
                elem_result = 0
                matches_in_area = 0
                for index, x_raw in enumerate(matches_xgdata):
                    if elem >= x_raw and elem < float(x_raw[0]) + interval:
                        matches_in_area += 1
                        if matches_xgresults[index] == checked_value:
                            elem_result += 1
                if matches_in_area < 50:
                    match_relatives = 0
                    match_result_y_full = {'total': 0, 'relative': match_relatives}
                else:
                    match_relatives = (elem_result/matches_in_area) * 100
                    match_result_y_full = {'total': matches_in_area, 'relative': match_relatives}
                result_y_full.append(match_result_y_full)
                result_y.append(match_relatives)
            pyplot.plot(result_x, result_y)
            pyplot.xlabel('Xg differences ranges')
            pyplot.ylabel('Probability, %')
            pyplot.title('Histogram of results with grouping by xg-data ({} matches)'.format(len(matches_xgdata)))
            pyplot.ylim(0, 100)
            pyplot.xlim(-20, 20)
            pyplot.xticks(array_for_sorting)
            pyplot.yticks(range(0, 100, 10))
            pyplot.grid(True)
            plot_name = '{}___{}___schedule'.format(xg_name[0], event_type)
            pyplot.savefig(os.path.join(schedules_dir, plot_name), format='png')
            pyplot.clf()
            stat_dict = dict(zip(result_x, result_y_full))
            stat_dict_ordered = OrderedDict(sorted(stat_dict.items(), key=lambda t: t[0]))

            statistic_as_json = json.dumps(stat_dict_ordered, indent=4)
            with open(os.path.join(json_reports_dir, plot_name)+'.json', 'w') as inf:
                inf.write(statistic_as_json)

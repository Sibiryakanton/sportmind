# Generated by Django 2.1 on 2019-10-28 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_collectors', '0001_initial'),
        ('analysis', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='machinelearningmodel',
            name='xg_param',
        ),
        migrations.AddField(
            model_name='machinelearningmodel',
            name='xg_param',
            field=models.ManyToManyField(blank=True, to='data_collectors.XGParameter', verbose_name='xg-параметры'),
        ),
    ]

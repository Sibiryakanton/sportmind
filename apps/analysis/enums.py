from django.utils.translation import ugettext_lazy as _
from apps.common.enums.base import BaseEnum


MATCH_OUTCOME_TYPES = (
    ('bothscored', _('Обе забьют')),
    ('total2.5', _('Тотал больше/меньше 2.5')),
    ('exacttotal', _('Точный тотал')),
)


class TipSourceTypes(BaseEnum):
    INTERNAL = 'internal algorithms'
    EXTERNAL = 'external resources'


class TipStatusTypes:
    WIN = 'win'
    LOSE = 'lose'

    CHOICES = (
        (WIN, WIN),
        (LOSE, LOSE)
    )

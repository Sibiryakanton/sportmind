from django.contrib import admin
from django.urls import path
from django.http import HttpResponseRedirect

from apps.analysis.models import Tip, TipSource
from django.utils.timezone import now, timedelta
from django.utils.translation import ugettext_lazy as _


@admin.register(TipSource)
class TipSourceAdmin(admin.ModelAdmin):
    list_display = ('pk', 'source_category', 'math_model', 'real_performance', 'is_active', 'is_ready_for_betting')
    search_fields = ('pk', 'source_category', 'math_model')
    ordering = ('math_model__performance',)
    list_editable = ('is_active', 'is_ready_for_betting')

    def real_performance(self, obj):
        stat_data = obj.get_stat(now()-timedelta(days=1200), now())
        return stat_data['performance']
    real_performance.short_description = 'Эффективность, %'


@admin.register(Tip)
class TipAdmin(admin.ModelAdmin):
    change_list_template = "list.html"

    list_display = ('match', 'event_time', 'tip_type', 'first_odd', 'tip_source', 'tip_status',
                    'bookmaker')
    search_fields = ('pk', 'match__pk', 'tip_type__runner_name', 'tip_status')
    readonly_fields = ('match', 'first_odd', 'tip_source', 'tip_status', 'preferred_odd')
    ordering = ['-match__event_datetime']

    def event_time(self, obj):
        obj.update_tip_status()
        return obj.match.event_datetime

    def league(self, obj):
        return str(obj.match.league)

    def bookmaker(self, obj):
        if obj.preferred_odd is not None:
            return obj.preferred_odd.odd_source.bookmaker.title

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('clear_null_tips/', self.clear_nullable_tips),
        ]
        return my_urls + urls

    def clear_nullable_tips(self, request):
        '''
        По неизвестным причинам прогнозы, связанные с удаленными источниками, не удаляются
         из админки через сигналы, поэтому пришлось вживить кнопку ручного
          удаления лишних прогнозов, если по ним нет ставок
        '''
        queryset = Tip.objects.filter(tip_source__isnull=True)
        for elem in queryset:
            if not elem.placed_bets.all().exists():
                elem.delete()
        return HttpResponseRedirect("/admin/analysis/tip/")
    clear_nullable_tips.short_description = _("Удалить лишние прогнозы")

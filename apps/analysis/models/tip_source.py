from django.db import models
from django.db.models.signals import post_delete, pre_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from apps.analysis.enums import TipSourceTypes
from apps.analysis.enums import TipStatusTypes


class TipSource(models.Model):
    source_category = models.CharField(choices=TipSourceTypes.choices(), verbose_name=_('Тип источника прогнозов'),
                                       max_length=100)
    provider_class_name = models.CharField(_('Название класса-менеджера источника'), null=True, max_length=200,
                                           blank=True)
    math_model = models.OneToOneField('MachineLearningModel', null=True, on_delete=models.CASCADE, blank=True,
                                      related_name='related_tip_source', verbose_name=_('Математическая модель'))
    is_active = models.BooleanField(default=False, verbose_name=_('Модель активна'),
                                    help_text=_('прогнозы генерируются, но не используются в ставках)'))
    is_ready_for_betting = models.BooleanField(default=False, verbose_name=_('Модель проверена'),
                                               help_text=_('Модель готова ко ставкам'))

    def __str__(self):
        if self.is_external():
            return self.provider_class_name
        else:
            return '{} {}'.format(self.pk, self.math_model.market_type.market_name_en, self.source_category)

    def is_external(self):
        '''
        Является ли источник прогнозов внешним (актуально для тех случае, когда в этом источнике мы
        просто берем прогнозы, составленные посторонними - например, парсим со сторонних сайтов)
        '''
        return self.source_category == TipSourceTypes.EXTERNAL and self.provider_class_name is not None

    def is_internal(self):
        '''
        Является ли источник прогнозов внутренним (то есть что источник реализован на нашей
        стороне, какая-то матмодель или жесткий скрипт)
        '''
        return self.source_category == TipSourceTypes.INTERNAL and self.math_model is not None

    def get_stat(self, start_datetime, end_datetime, league=None):
        '''
        Получить общее кол-во прогнозов, выигрышные и проигрышные прогнозы по указанному источнику за период времени
        :param start_datetime: Стартовая дата, от которой нужно брать прогнозы
        :param end_datetime: Конечная дата, до которой брать прогнозы
        :param league: объект League, прогнозы по которому нужно использовать в выдаче статистики
        :return:
        '''
        query = models.Q(match__event_datetime__gte=start_datetime, match__event_datetime__lte=end_datetime,
                         tip_status__isnull=False)
        if league is not None:
            query &= models.Q(match__league=league)
        tips = self.tips.filter(query).prefetch_related('preferred_odd')
        tips_len = len(tips)
        if tips_len:
            profit = float(0)
            success_tips = len(tips.filter(tip_status=TipStatusTypes.WIN))
            lose_tips = len(tips.filter(tip_status=TipStatusTypes.LOSE))
            performance = success_tips / len(tips)
            sum_plus = 0
            sum_minus = 0
            sum_all = 9
            for i in tips:
                if i.preferred_odd is None:
                    value = i.first_odd
                else:
                    value = i.preferred_odd.value
                if i.tip_status == TipStatusTypes.WIN:
                    current_profit = float(float(value)-1.0)
                    sum_plus += current_profit
                elif i.tip_status == TipStatusTypes.LOSE:
                    profit -= float(1.00)
                    sum_minus -= 1.00
                sum_all += i.first_odd
            if success_tips == 0:
                avg_profit = None
            else:
                avg_profit = round(1+sum_plus/success_tips, 2)
            return {'all': tips_len, 'wins': success_tips, 'loses': lose_tips, 'performance': round(performance, 2),
                    'profit': round(sum_plus+sum_minus, 2), 'avg_profit_bet': avg_profit,
                    'avg_bet': round(sum_all/tips_len, 2)}
        else:
            success_tips = '-'
            lose_tips = '-'
            performance = '-'
        return {'all': 0, 'wins': success_tips, 'loses': lose_tips, 'performance': performance, 'profit': 0.00}

    class Meta:
        verbose_name = _('Источник прогноза')
        verbose_name_plural = _('Источники прогноза')


@receiver(post_delete, sender=TipSource)
def remove_math_model_and_related_tips(sender, instance, **kwargs):
    if instance.math_model is not None:
        instance.math_model.delete()
    for elem in instance.tips.all():
        # если по прогнозу нет ставок
        if not elem.placed_bets.all().exists():
            elem.delete()

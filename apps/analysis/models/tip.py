from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now, timedelta
from apps.analysis.enums import TipStatusTypes


class TipManager(models.Manager):
    def get_for_betting(self, bookmaker=None):
        '''
        Получить прогнозы, по которым уже можно ставить и матчи еще не прошли
        :return:
        '''
        tip_filter = models.Q(tip_source__is_active=True, tip_source__is_ready_for_betting=True,
                              match__result__isnull=True, is_time_for_bet=True,
                              match__event_datetime__lte=now()-timedelta(minutes=5))
        if bookmaker is not None:
            tip_filter &= models.Q(preferred_odd__odd_source__bookmaker=bookmaker)
        return super().get_queryset().filter(tip_filter)

    def get_with_preferred_odds(self):
        return super().get_queryset().filter(preferred_odd__isnull=False).order_by('-match__event_datetime')


class Tip(models.Model):
    match = models.ForeignKey('data_collectors.Match', on_delete=models.CASCADE, related_name='tips')
    tip_type = models.ForeignKey('data_collectors.EventRunnerType', on_delete=models.CASCADE,
                                 verbose_name='Тип события')
    first_odd = models.DecimalField('Первый кэф', max_digits=8, decimal_places=4)

    tip_source = models.ForeignKey('TipSource', on_delete=models.SET_NULL, null=True, related_name='tips')
    published_date = models.DateTimeField(_('Дата публикации'), auto_now=True)
    tip_status = models.CharField(max_length=20, choices=TipStatusTypes.CHOICES, null=True, blank=True)
    is_time_for_bet = models.BooleanField(default=False, verbose_name=_('Можно делать ставку'),
                                          help_text=_('Ставка становится подходящей, когда кэф падает или до матча '
                                                      'остается полчаса'))
    preferred_odd = models.ForeignKey('data_collectors.OddModel', null=True, blank=True, on_delete=models.PROTECT,
                                      verbose_name=_('Лучший вариант для ставки'))

    objects = TipManager()

    def __str__(self):
        return '{} {} {}'.format(self.is_time_for_bet, self.match, self.tip_type)

    @property
    def is_win(self):
        return self.tip_status == TipStatusTypes.WIN

    def update_tip_status(self):
        '''
        Если матч уже прошел, изменить статус прогноза в зависимости от результата матча
        :return:
        '''
        if self.match.result is None:
            return None
        result = self.match.result
        runner_name = self.tip_type.runner_name
        if self.tip_type.event_type.market_name_en == 'Both teams to Score?':
            real_result = result.is_both_scored()
            if real_result is not None:
                if real_result and runner_name == 'Yes' or not real_result and runner_name == 'No':
                    self.tip_status = TipStatusTypes.WIN
                else:
                    self.tip_status = TipStatusTypes.LOSE
        elif self.tip_type.event_type.market_name_en == 'Over/Under 2.5 Goals':
            is_over25 = result.is_total_over25()
            if is_over25 is not None:
                tip_over25 = self.tip_type.runner_name == 'Over 2.5 Goals'
                if is_over25 == tip_over25:
                    self.tip_status = TipStatusTypes.WIN
                else:
                    self.tip_status = TipStatusTypes.LOSE

        elif self.tip_type.event_type.market_name_en == 'First Half Goals 1.5':
            is_over15 = result.is_ht_total_over15()
            if is_over15 is not None:
                tip_over15 = self.tip_type.runner_name == 'Over 1.5 Goals'
                if is_over15 == tip_over15:
                    self.tip_status = TipStatusTypes.WIN
                else:
                    self.tip_status = TipStatusTypes.LOSE
        self.save()

    def calculate_best_odd(self):
        '''
        Просмотреть все варианты кэфов (только непосредственно от API букмекеров - кэфы букмекеров, взятые
        со сторонних источников, не в счет), выбрать лучший вариант и посмотреть, насколько сейчас актуально делать
         ставку: если до матча осталось меньше получаса либо тренд кэфа начал падать, настало время отправлять ставку
        :return:
        '''
        odds = self.match.odds.filter(event_type=self.tip_type, odd_source__title__isnull=True).order_by('value')
        if odds.exists():
            best_odd = odds.last()
            if self.preferred_odd != best_odd:
                self.preferred_odd = best_odd
                self.save()
            is_match_soon = now() + timedelta(minutes=30) >= self.match.event_datetime
            if self.is_trend_falling() or is_match_soon:
                self.is_time_for_bet = True
                self.save()
                return True

    def is_trend_falling(self):
        '''
        Прежде чем решать, пора ли делать ставку, нужно проанализировать тренд: если ставка уверенно идет вниз, можно
        делать ставку. Если коэффициент "прыгает" или уверенно идет вверх, продолжаем ждать лучшего момента
        :return:
        '''
        trend = self.preferred_odd.trend_data
        min_trend_values = 5
        if len(trend) >= min_trend_values:
            last_value = trend[0]
            dispersions = []
            for i in trend[1:min_trend_values-1]:
                dispersions.append(last_value-i)
            if sum(dispersions) <= -0.05:
                return True
        else:
            return False

    def betfair_selection_id(self):
        '''
        ID селектора может быть как у самого селектора (если статичный), так и у кэфа (если маркет динамичный)
        :return:
        '''
        return self.tip_type.betfair_id or self.preferred_odd.value

    class Meta:
        verbose_name = _('Прогноз')
        verbose_name_plural = _('Прогнозы')
        ordering = ['-match__event_datetime']

from django.db import models
from django.utils.translation import ugettext_lazy as _


class MachineLearningModel(models.Model):
    '''
    Модель для хранения данных о математической модели. Правильнее сказать, что это
    модель для файла, в котором идут расчеты + для информационных полей. Каждая модель
    обучается на основе отдельного типа ML-алгоритма и XG-параметра
    '''
    title = models.CharField(max_length=400, verbose_name=_('Заголовок'))
    xg_param = models.ManyToManyField('data_collectors.XGParameter', verbose_name=_('XG-параметры'), blank=True)
    market_type = models.ForeignKey('data_collectors.EventMarketType', on_delete=models.PROTECT, null=True)
    model_name = models.CharField(max_length=150, verbose_name=_('Название матмодели'), default='')
    performance = models.DecimalField(max_digits=5, decimal_places=4)
    stat_test_info = models.TextField(_('Данные о последнем тесте на неизвестной выборке'))
    file = models.FileField(_('Файл матмодели'), upload_to='models_files/')
    active = models.BooleanField(default=False)

    def get_title(self):
        xg_ids = [str(xg.pk) for xg in self.xg_param.all()]
        xg_params = '&'.join(xg_ids)
        title = '__'.join([xg_params, self.market_type.xg_clf_alias, self.model_name, str(self.performance)])
        return title

    def __str__(self):
        return '{} {}%'.format(self.get_title(), self.performance)

    class Meta:
        verbose_name = _('Математическая модель')
        verbose_name_plural = _('Математические модели')

from apps.data_collectors.models import TeamAliasArray
from difflib import SequenceMatcher
import logging
log = logging.getLogger('django.parsers')


def get_or_update_alias(team_obj, match=None, new_name=None):
    '''
    Получить алиасы команды или добавить новое возможное название компании в список алиасов
    :param team_obj: объект Team
    :param new_name: новое имя команды для добавления
    :param match: Объект Match
    :return: объект TeamAliasArray
    '''
    team_title = team_obj.title
    defaults = {'titles_array': [team_title,]}
    try:
        team_alias = TeamAliasArray.objects.get_or_create(titles_array__contains=[team_title], defaults=defaults)[0]
    except TeamAliasArray.MultipleObjectsReturned as err:
        raise err
    is_adding_name_method = new_name is not None and match is not None
    team_names_similarity = round(SequenceMatcher(None, team_title, new_name or '').ratio(), 2)
    is_identity = new_name is not None and (team_title in new_name or new_name in team_title)
    is_same_team_new_title = new_name not in team_alias.titles_array and (team_names_similarity > 0.5 or is_identity)
    if is_adding_name_method:
        if is_same_team_new_title:
            team_alias.titles_array.append(new_name)
            team_alias.save()
    return team_alias, is_same_team_new_title

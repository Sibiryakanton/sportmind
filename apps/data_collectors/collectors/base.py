from apps.data_collectors.enums import ServicesTypes

from abc import abstractmethod
import logging

log = logging.getLogger('django.parsers')


class BaseServiceConnector:
    '''
    Базовый класс, от которого должны ветвиться все парсеры (и api-менеджеры косвенно, через BaseAPIManager)
    По сути класс нужен для обязательного внедрения атрибута service_name
    '''
    @property
    @abstractmethod
    def service_name(self, value):
        '''
        Условное название сервиса, через которое указанный класс ассоциируется
         с конкретным объектом ComponentInfo (допустимые варианты указаны в ServicesTypes)
        :param value:
        :return:
        '''
        if value is None or value not in dict(ServicesTypes.choices()).keys():
            raise AttributeError('The attribute service_name is required and must be selected '
                                 'from ServicesTypes choices')

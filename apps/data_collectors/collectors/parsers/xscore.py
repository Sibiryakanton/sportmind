from bs4 import BeautifulSoup
from apps.common.utils import request_with_proxy
from apps.data_collectors.collectors import BaseServiceConnector
from apps.data_collectors.enums import ServicesTypes

from django.conf import settings
from datetime import datetime
import requests
import re
import logging

log = logging.getLogger('django.parsers')


class XscoreParser(BaseServiceConnector):
    '''
    Класс-менеджер, в котором реализованы методы запросов к SoccerAPI сервиса Sportmonks,
    предоставляющего данные о спортивных матчах, и методы сохранения в базу
    '''
    service_name = ServicesTypes.xscore
    url_root = 'https://xscore.win/local/templates/xscore/ajax/get.php'
    auth = (settings.XSCORE_USERNAME, settings.XSCORE_PASSWORD)

    def get_countries_list(self):
        '''
        Получить список стран
        :return: список массивов с id и name страны
        '''
        countries_list = self.__parsing_the_li_list({'page': 'listtournament'})
        return countries_list

    def get_tournaments(self, country_id):
        '''
        Получить список лиг указанной страны (Бундеслига, Премьер-лига и т.п.)
        :param country_id: id страны (узнать id страны можно через запрос get_countries_list этого же класса
        :return:
        '''
        tournaments_list = self.__parsing_the_li_list({'page': 'listtournament', 'country': country_id})
        return tournaments_list

    def get_seasons(self, tournament_id):
        '''
        Получить все категории сезонов указанной лиги, которые есть в доступе на xscore
        :param tournament_id: id лиги
        :return:
        '''
        seasons_list = self.__parsing_the_li_list({'page': 'listyear', 'tournament': tournament_id})
        return seasons_list

    def get_tours(self, season_id):
        '''
        Получить список туров указанного сезона
        :param season_id: id сезона
        :return:
        '''
        tours_list = self.__parsing_the_li_list({'page': 'listtour', 'year': season_id})
        return tours_list

    def __get_matches_list(self, country_id, tournament_id, season_id, tour_id, content=None):
        '''
        Получить список матчей указанного тура, то есть пробегаемся по каждому матчу и собираем инфу о командах
         (названия и все xg-данные)
        :param country_id: id страны
        :param tournament_id: id лиги
        :param season_id: id сезона
        :param tour_id: id тура
        :param content: необязательное поле, которое используется для отладки. Обычно запрос матчей требует
         10-15 секунд, что очень мешает при разработке. Гораздо эффективнее один раз запросить ответ сервера,
         скопировать его в этот аргумент и парсить его, не делая запрос серверу
        :return:
        '''
        matches_list_info = []
        if content is None:
            request_data = {'page': 'getstatistics',
                            'ajaxtype': 'all',
                            'country': country_id,
                            'tournament': tournament_id,
                            'year': season_id,
                            'tour': tour_id,
                            'lasttour': tour_id}
            response = request_with_proxy(requests.post, self.url_root, data=request_data, auth=self.auth)
            content = response.content.decode()

        soup = BeautifulSoup(content, 'html.parser')
        matches = soup.find_all('div', {'id': re.compile(r'bx_*')})
        for match in matches:
            try:
                event_date, event_score, first_goal_team, error = self.__get_date_score_first_goal_team(match)
            except ValueError:
                continue
            if error:
                continue
            match_info = {'xscore_id': match.attrs['id'], 'event_date': event_date, 'teams': [],
                          'score': event_score, 'first_goal_team': first_goal_team}
            table_row_container = match.find('div', 'table_last_match').find('div', 'table_row_container')
            team_divs = table_row_container.find_all('div', 'table_cell_stat', recursive=False)
            for team_div in team_divs:
                team_name = team_div.find('div', 'prev-logo').find('img').attrs['alt']
                team_info = {'name': team_name, 'xg_data': [], 'event_date': event_date.date()}
                team_xg_group_containers = team_div.find_all('div', self.__is_not_preview_logo, recursive=False)

                for data_prev in team_xg_group_containers:
                    # Два объекта table_row: один с описаниями контейнеров, второй - с фактическими значениями у команды
                    xg_names_and_values_rows = data_prev.find_all('div', 'table_row', recursive=False)

                    # xg-значение - это не просто xG = 0.99: каждое значение имеет уникальный показатель в
                    # зависимости от ситуации на поле: например, от разрыва в счете и в чью пользу. Вот это
                    # и есть group_type - тип группы xg-данных. По факту выглядит xg условно так: draw xG = 0.99
                    group_type = data_prev.attrs['data-prev-table']
                    team_prev_info = self.__get_team_xg_group_data(xg_names_and_values_rows, group_type)
                    team_info['xg_data'].append(team_prev_info)
                match_info['teams'].append(team_info)
            matches_list_info.append(match_info)
        return matches_list_info

    def __get_date_score_first_goal_team(self, match):
        '''
        Получить 4 атрибута матча:
        Дата
        Итоговый счет
        какая команда открыла счет первой (если счет был открыт)
        Есть ли критичная ошибка
        :return: event_date, event_score, event_first_goal_team and boolean value (if true, stop and skip the match)
        '''

        # Извлечь дату матча и результат, если есть. Если событие прошло, а результата все еще нет, пропустить матч
        datetime_string = match.find('div', ["table_cell_name"]).string
        if datetime_string is None:
            return None, None, None, True
        event_date = datetime.strptime(datetime_string, '%d.%m.%Y')
        event_score_string = None
        event_first_goal_team = None
        if event_date.date() < datetime.today().date():
            table_footer = match.find('div', 'table_footer')
            try:
                table_footer_cell = table_footer.find('div', 'table_cell')
                match_result_raw_string = table_footer_cell.find('div').string
                event_score_string = match_result_raw_string.replace('Счет:', '').strip()
            except AttributeError:
                msg = str(event_date) + ' Error: cant extract the match result'
                return event_date, event_score_string, event_first_goal_team, True

            # Если были голы, ищем, когда и с чьей стороны был первый
            match_result_scores = [int(i) for i in event_score_string.split('-')]
            if sum(match_result_scores):
                events_table = match.find('div', 'table_body')
                rows_with_match_events = events_table.find_all('div', 'table_row')
                for row in rows_with_match_events:
                    # В редких случаях даже при наличии счета вместо списка событий может быть просто написано:
                    # " Техническое поражение команде [название]. " В таком случае если счет 0-1 или 1-0,
                    # понять, кто забил первый, легко, иначе оставляем None. Но если это не тот случай,
                    # все же выводим ошибку
                    score_cell = row.find('div', 'table_cell')
                    if score_cell is None:
                        comment_cell = row.find('div', 'match_comment')
                        if comment_cell is None:
                            msg = 'Unknown error {} {}'.format(match.attrs['id'], event_date)
                            log.info(msg)

                            break
                        comment_cell_string = row.find('div', 'match_comment').string.strip()

                        if match_result_scores[0]:
                            event_first_goal_team = 'home'
                        elif match_result_scores[0]:
                            event_first_goal_team = 'away'
                        break
                    score_cell_string = row.find('div', 'table_cell').string.strip()
                    if score_cell_string != '0-0':
                        teams_scores = score_cell_string.strip('-')
                        if teams_scores[0]:
                            event_first_goal_team = 'home'
                        else:
                            event_first_goal_team = 'away'
                        break
        return event_date, event_score_string, event_first_goal_team, False

    def __get_team_xg_group_data(self, xg_names_and_values_rows, xg_group_type):
        '''
        Получить список xg-параметрои и х значений для конкретной команды по конкретной группе
        :param xg_group_type: тип данных (стандартный xg, при ничьей, при преимуществе или отставании
        (значения должны браться из атрибутов data-prev-table)
        :param xg_names_and_values_rows:
        :return:
        '''
        # В зависимости от типа xg-группы структура меняется: внутри первого table_row у всех, кроме xg,
        # встречается table_row_container с двумя table_cell_stat, из которых первый содержит
        # общее название категории данных, а уже второй - названия xg-параметров
        xg_team_data_response = {'xg_group_type': xg_group_type, 'data': []}
        if xg_group_type == 'xg':
            current_xg_dict = {'subtype_name': 'xg', 'values': []}
            xg_descriptions = self.__parse_table_row_descriptions(xg_names_and_values_rows[0])
            xg_values = self.__get_table_cell_values(xg_names_and_values_rows[1])
            for index, description_list in enumerate(xg_descriptions):
                value_data = self.__convert_description_data(description_list, xg_values[index])
                current_xg_dict['values'].append(value_data)
            xg_team_data_response['data'].append(current_xg_dict)

        else:
            xg_row_containers_description = xg_names_and_values_rows[0].find_all('div', 'table_row_container')
            row_values = xg_names_and_values_rows[1]

            # Описания xg-атрибутов разбиты по отдельным группам, если тип prev-table != 'xg'
            xg_description_list = []
            for row_container in xg_row_containers_description:
                xg_group_cell_stats = row_container.find_all('div', 'table_cell_stat')
                row_description = xg_group_cell_stats[1].find('div', 'table_row', recursive=False)
                xg_description_data = self.__parse_table_row_descriptions(row_description)
                for i in xg_description_data:
                    xg_description_list.append(i)

            cell_values = self.__get_table_cell_values(row_values)
            xg_subtypes = self.__get_table_xg_subtypes(xg_row_containers_description)

            # Заранее создаем в итоговом ответе функции области для подтипов xg-данных
            subtype_data_types_as_keys = {}
            for i in xg_subtypes:
                subtype_data_types_as_keys[i] = []

            for index, description_data in enumerate(xg_description_list):
                xg_subtype = xg_subtypes[1] if index >= 3 else xg_subtypes[0]
                value_data = self.__convert_description_data(description_data, cell_values[index])
                subtype_data_types_as_keys[xg_subtype].append(value_data)
            for subtype_name, values in subtype_data_types_as_keys.items():
                xg_team_data_response['data'].append({'subtype_name': subtype_name, 'values': values})
        return xg_team_data_response

    def __convert_description_data(self, description_data, cell_value):
        '''
        Разобрать html-код ячейки с xg-параметром и конвертировать в словарь
        :param description_data: Описание xg-параметра в отрыве от его значения для конкретного матча
        :param cell_value: Собственно, само значение параметра
        :return:
        '''
        xg_description = description_data['description']
        xg_name = description_data['name']
        xg_value = cell_value
        if xg_value == '-':
            xg_value = 0
        elif '/' in xg_value:
            xg_value_parts = [float(i) for i in xg_value.split('/')]
            xg_value = 0 if 0 in xg_value_parts else xg_value_parts[0] / xg_value_parts[1]

        value_data = {'name': xg_name, 'value': float(xg_value), 'description': xg_description}
        return value_data

    def __parse_table_row_descriptions(self, table_row):
        xg_description_tooltips = table_row.find_all('div', 'table_tooltip')
        xg_descriptions_list = []
        for desc_div in xg_description_tooltips:
            xg_description = desc_div.attrs['data-description']
            xg_name = desc_div.find('div').string
            xg_description_data = {'name': xg_name, 'description': xg_description}
            xg_descriptions_list.append(xg_description_data)
        return xg_descriptions_list

    def __get_table_cell_values(self, table_row):
        '''
        Распарсить ряд со значениями xg-данных и вернуть чистый список цифр в том же порядке
        :param table_row:
        :return:
        '''
        xg_values = []
        xg_value_divs = table_row.find_all('div', 'table_cell')
        for i in xg_value_divs:
            xg_value = i.contents[0].strip()
            xg_values.append(xg_value)
        return xg_values

    def __get_table_xg_subtypes(self, table_row_containers):
        xg_types = []
        for description_div in table_row_containers:
            description_cell_stat = description_div.find('div', 'table_cell_stat')
            xg_type_value = description_cell_stat.find('div', 'table_cell').string
            xg_types.append(xg_type_value)
        return xg_types

    def __parsing_the_li_list(self, request_data):
        '''
        Большая часть запросов возвращают стандартизированные списки, где надо извлечь из каждого li атрибут
         data-value и строку в тэге strong
        :return: список словарей
        '''
        response = request_with_proxy(requests.post, self.url_root, data=request_data, auth=self.auth)
        content = response.content.decode()
        soup = BeautifulSoup(content, 'html.parser')
        source_tags = soup.find_all("li")
        result_list = []
        for i in source_tags:
            object_name = i.find('strong').string
            object_id = i.attrs['data-value']
            object_info = {'name': object_name, 'id': object_id}
            result_list.append(object_info)
        return result_list

    def __is_not_preview_logo(self, tag):
        '''
        Мы хотим собрать среди всех контейнеров команды, содержащих 'prev'приставку, только полезные, поэтому
        исключаем контейнер с логотипом
        :param tag:
        :return:
        '''
        return tag != 'prev-logo'

    def processing_tour_matches_list(self, league, season, tour):
        '''
        Запрос списка матчей, разбор кода и составление валидного словаря для выдачи
        :param league: объект League
        :param season: Строка с номером сезона (не самим сезоном, а его ID)
        :param tour: строка с id тура
        :return: список со слвоарями с информацией о матчах тура
        '''
        season_id = season['id']
        tour_id = tour['id']
        msg = 'Запрос списка матчей. {} {} {}'.format(league.league_name, season['name'], tour['name'])
        log.info(msg)
        league_matches_list = self.__get_matches_list(country_id=league.country.xscore_id,
                                                      tournament_id=league.xscore_id,
                                                      season_id=season_id, tour_id=tour_id)
        formatted_data = []
        for match in league_matches_list:
            first_team_name = match['teams'][0]['name']
            second_team_name = match['teams'][1]['name']
            if match['score'] is not None:
                scores = match['score'].split('-')
                result_info = {'first_team': {'title': first_team_name, 'score': scores[0], 'xg_score': None},
                               'second_team': {'title': second_team_name, 'score': scores[1], 'xg_score': None}}
                first_team_goal = match['first_goal_team']
                if first_team_goal == 'home':
                    result_info['first_team']['is_first_goal'] = True
                if first_team_goal == 'away':
                    result_info['second_team']['is_first_goal'] = True
            else:
                result_info = None

            match_data = {'xscore_id': match['xscore_id'],
                          'first_team_info': match['teams'][0],
                          'second_team_info': match['teams'][1],
                          'result_info': result_info,
                          'event_datetime': match['event_date'],
                          'event_datetime_end': None,
                          'country': league.country.country_name_en,
                          'league': league.xscore_id,
                          'season': season['name'],
                          'tour': tour['name'].replace('-й тур', ''),
                          }
            formatted_data.append(match_data)
        return formatted_data

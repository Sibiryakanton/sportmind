import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from apps.data_collectors.collectors import BaseServiceConnector

from apps.data_collectors.models import EventMarketType, LeagueNameAlias, OddSource
from apps.data_collectors.enums import ServicesTypes
from apps.common.enums import EventStatusTypes
from apps.common.utils import request_with_proxy
from django.utils.timezone import now
import pytz
import logging

log = logging.getLogger('django.parsers')


class FlashScoreParser(BaseServiceConnector):
    service_name = ServicesTypes.flashscore
    ROOT = 'https://www.flashscore.com'

    '''
    Полное описание к информации о матчах (на сайте она предоставлена в виде пугающей каши, 
    которая по факту является просто причудливой альтернативой записи csv. В конечном итоге 
    его можно перевести в простой массив с данными, значения ключей которого даны ниже:

    AL - Available Lines/Lives - список каналов, где матч будет проходить в лайве
    AA - flashscore_id': match_dict['AA'],
    ER = Строка с туром, формата Round 6 
    ADE - Время начала матча в формате UNIX
    CX - Название домашней команды
    AF - Название гостевой команды
    AG - итоговый счет домашней команды
    BA - счет домашней команды в первом тайме
    AH - итоговый счет гостевой команды
    BB - счет гостевой команды в первом тайме
    WV - ссылочное название гостевой
    WU - нормально название домашней
    'BX - статус матча (-1 - не начался, '' - лайв, )
    '''
    '''
    Ключи, чье значение осталось неизвестным:
    AB BGS JB RW AD
    JA BX MW AN WQ 
    AW CR
    '''

    def get_match_results(self, league):
        '''
        Загрузить результаты прошедших матчей за последний сезон лиги
        :param league:
        :return:
        '''
        formatted_data = []
        league_slug = self.__get_league_name_slug(league)
        country_name = league.country.country_name_en
        country_slug = country_name.lower().replace(' ', '-').replace('.', '')
        url = '{}/football/{}/{}/results/'.format(self.ROOT, country_slug, league_slug)
        response = requests.get(url)
        print(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        season = soup.find('div', 'teamHeader__text').string
        matches = soup.find('div', {'id': 'tournament-page-data-results'})
        matches_raw_info_string = matches.string
        if matches_raw_info_string is None:
            return formatted_data
        raw_info_list = matches_raw_info_string.split('~')
        matches_raw_info_list = raw_info_list[2:]

        matches_dicts_list = []
        for match in matches_raw_info_list:
            match_info = match.split('¬')[:-1]
            match_raw_dict = {}
            for string in match_info:
                key_value_list = string.split('÷')
                match_raw_dict[key_value_list[0]] = key_value_list[1]
            matches_dicts_list.append(match_raw_dict)
        for match_dict in matches_dicts_list:
            if match_dict == {}:
                continue
            try:
                tour = match_dict['ER'].split()[1]
            except IndexError as err:
                if match_dict['ER'] == 'Final':
                    continue
                else:
                    log.error(league, match_dict)
                    raise err
            except KeyError:
                continue
            event_datetime = datetime.fromtimestamp(float(match_dict['ADE']))
            event_date = event_datetime.date()
            translated_match_dict = {'flashscore_id': match_dict['AA'], 'tour': tour, 'league': league.pk,
                                     'season': season, 'country': country_name, 'event_datetime': event_datetime,
                                     'first_team_info': {'name': match_dict['CX'], 'event_date': event_date},
                                     'second_team_info': {'name': match_dict['AF'], 'event_date': event_date},
                                     'result_info': {
                                         'first_team': {'title': match_dict['CX'], 'score': match_dict['AG'],
                                                        'score_1_half': match_dict['BA']},
                                         'second_team': {'title': match_dict['AF'], 'score': match_dict['AH'],
                                                         'score_1_half': match_dict['BB']}
                                     },
                                     'event_status': EventStatusTypes.FINISHED,
                                     'odds': []}
            formatted_data.append(translated_match_dict)
        return formatted_data

    def get_match_fixtures(self, league):
        '''
        Загрузить данные о будущих или проходящих прямо сейчас матчах
        :param league:
        :return:
        '''
        league_slug = self.__get_league_name_slug(league)
        country_name = league.country.country_name_en
        country_slug = country_name.lower().replace(' ', '-')
        url = '{}/football/{}/{}/fixtures/'.format(self.ROOT, country_slug, league_slug)
        response = request_with_proxy(requests.get, url)
        soup = BeautifulSoup(response.text, 'html.parser')
        season = soup.find('div', 'teamHeader__text').string
        matches = soup.find('div', {'id': 'tournament-page-data-fixtures'})
        matches_raw_info_string = matches.string
        try:
            raw_info_list = matches_raw_info_string.split('~')
        except AttributeError:
            return []

        matches_raw_info_list = raw_info_list[2:]

        formatted_data = []

        for match in matches_raw_info_list:
            match_info = match.split('¬')[:-1]
            match_raw_dict = {}
            for string in match_info:
                key_value_list = string.split('÷')
                match_raw_dict[key_value_list[0]] = key_value_list[1]
            if match_raw_dict == {}:
                continue
            try:
                tour = match_raw_dict['ER'].split()[1]
            except IndexError as err:
                # Исключение для туров Швеции
                if match_raw_dict['ER'] == 'Final':
                    continue
                else:
                    raise err
            event_datetime = datetime.fromtimestamp(float(match_raw_dict['ADE']))
            localized_event_datetime = pytz.UTC.localize(event_datetime)
            if pytz.UTC.localize(event_datetime) > now() + timedelta(days=7):
                break

            # Периодически на стороне Flashscore всплывает странный баг: уже закончившиеся матчи продолжают
            # отображаться в фикстурах и упорно не переезжают в результаты. Чтобы не терять время, лучше сохранять
            # матч любого статуса, даже если он принадлежит к багованной выдаче
            event_status_key_value = {'0': EventStatusTypes.NOT_STARTED, '1': EventStatusTypes.LIVE}
            event_status = EventStatusTypes.NOT_STARTED
            max_event_time = localized_event_datetime+timedelta(minutes=120)
            if 'AX' in match_raw_dict.keys():
                event_status = event_status_key_value[match_raw_dict['AX']]
                if event_status == EventStatusTypes.NOT_STARTED and max_event_time < now():
                    event_status = EventStatusTypes.FINISHED
            match_id = match_raw_dict['AA']
            translated_match_dict = {
                'flashscore_id': match_id, 'tour': tour, 'league': league.pk,
                'event_status': event_status, 'season': season, 'country': country_name,
                'event_datetime': localized_event_datetime, 'result_info': None, 'odds': [],
                'first_team_info': {'name': match_raw_dict['CX'], 'event_date': localized_event_datetime.date()},
                'second_team_info': {'name': match_raw_dict['AF'], 'event_date': localized_event_datetime.date()},
            }
            # Если результат у фикстуры все же есть, сохраняем
            if event_status == EventStatusTypes.FINISHED:
                try:
                    translated_match_dict['result_info'] = {
                        'first_team': {'title': match_raw_dict['CX'],
                                       'score': match_raw_dict['AG'],
                                       'event_date': event_datetime.date(),
                                       'score_1_half': match_raw_dict['BA']},
                        'second_team': {'title': match_raw_dict['AF'],
                                        'score': match_raw_dict['AH'],
                                        'event_date': event_datetime.date(),
                                        'score_1_half': match_raw_dict['BB']}
                    }
                except KeyError:
                    continue
            elif event_status == EventStatusTypes.NOT_STARTED and localized_event_datetime < now():
                try:
                    match_odds = self.__get_match_odds_main_func(match_id)
                    translated_match_dict['odds'] = match_odds
                except requests.exceptions.SSLError:
                    pass
            formatted_data.append(translated_match_dict)

        return formatted_data

    def __get_match_odds_main_func(self, match_id):
        '''
        Собрать кэфы на букмекеров на события. Нужно учесть, что
         верстка каждого раздела с кэфами может уникально отличаться от остальных,
          поэтому вынести парсинг кэфов в одну красивую функцию,
          применимую ко всем разделам, не получится. Так что остается только при добавлении
          нового маркета отдельно внедрять новую функцию в этом парсере
        :param match_id: ID матча в системе Flashscore
        :return:
        '''
        referer_url = '{}/match/{}/#odds-comparison;both-teams-to-score;full-time'.format(self.ROOT, match_id)
        headers = {'User-Agent': 'core',
                   'Accept': '*/*',
                   'Accept-Language': '*',
                   'Accept-Encoding': 'gzip, deflate, br',
                   'X-GeoIP': '1',
                   'X-Referer': referer_url,
                   'X-Fsign': 'SW9D1eZo',
                   'X-Requested-With': 'XMLHttpRequest',
                   'DNT': '1',
                   'Connection': 'keep-alive',
                   'Referer': 'https://d.flashscore.com/x/feed/proxy-local'
                   }
        url = 'https://d.flashscore.com/x/feed/d_od_{}_en_1_eu'.format(match_id)
        response = request_with_proxy(requests.get, url, headers=headers)
        if response.status_code != 200:
            print(response.content)
        soup = BeautifulSoup(response.text, 'html.parser')
        market_odds_data = []
        active_markets = EventMarketType.objects.filter(is_active=True,
                                                        flashscore_method__isnull=False).prefetch_related()
        for market in active_markets:
            odd_collect_function = getattr(self, '{}{}'.format('_FlashScoreParser', market.flashscore_method))
            odds_data = odd_collect_function(soup, market)
            market_odds_data.append(odds_data)
        return market_odds_data

    def __get_match_odds_bts(self, match_odd_page_soup, market):
        '''
        Получить коэффициенты матча по категории "Обе забьют"
        :param match_odd_page_soup: общая таблица кэфов
        :param market: Объект модели маркета Обе Забьют
        :return: словарь с коэффициентами
        '''

        odd_sources = OddSource.objects.filter(title='Flashscore')
        allowed_bookmakers = [odd_source.bookmaker.title for odd_source in odd_sources]
        bts_market_name = market.market_name_en
        odds_data = {'market_type': bts_market_name, 'odds_info': []}

        odd_score_soup = match_odd_page_soup.find('div', {'id': 'block-both-teams-to-score-ft'})
        try:
            odd_table = odd_score_soup.find('table', {'id': 'odds_both_teams_to_score'})
        except AttributeError:
            return odds_data
        table_rows = odd_table.find_all('tr')
        header, bookmakers_rows = table_rows[0], table_rows[1:]
        for bookmaker_row in bookmakers_rows:
            bookmaker_string = bookmaker_row.find('a', 'elink').attrs['title']
            if bookmaker_string not in allowed_bookmakers:
                continue
            block_odds = bookmaker_row.find_all('td', 'kx')
            yes_runner_info = self.__get_option_odd(block_odds[0], 'Yes')
            no_runner_info = self.__get_option_odd(block_odds[1], 'No')

            current_bookmaker_data = {'bookmaker': bookmaker_string,
                                      'runners': [yes_runner_info, no_runner_info]
                                      }
            odds_data['odds_info'].append(current_bookmaker_data)
        return odds_data

    def __get_match_odds_total25(self, match_odd_page_soup, market):
        '''
        Получить коэффициенты матча по категории "Тотал больше/меньше 2.5"
        :param match_odd_page_soup: общая таблица кэфов
        :return:
        '''

        odd_sources = OddSource.objects.filter(title='Flashscore')
        allowed_bookmakers = [odd_source.bookmaker.title for odd_source in odd_sources]
        market_name = market.market_name_en

        odds_data = {'market_type': market_name, 'odds_info': []}

        total_all_tables = match_odd_page_soup.find('div', {'id': 'block-under-over'})
        try:
            table_25 = total_all_tables.find('table', {'id': 'odds_ou_2.5'})
        except AttributeError:
            return odds_data

        table_rows = table_25.find_all('tr')
        header, bookmakers_rows = table_rows[0], table_rows[1:]
        for bookmaker_row in bookmakers_rows:
            bookmaker_string = bookmaker_row.find('a', 'elink').attrs['title']
            if bookmaker_string not in allowed_bookmakers:
                continue
            block_odds = bookmaker_row.find_all('td', 'kx')
            runner_over = self.__get_option_odd(block_odds[0], 'Over 2.5 Goals')
            runner_under = self.__get_option_odd(block_odds[1], 'Under 2.5 Goals')

            current_bookmaker_data = {'bookmaker': bookmaker_string,
                                      'runners': [runner_over, runner_under]
                                      }
            odds_data['odds_info'].append(current_bookmaker_data)
        return odds_data

    def __get_match_odds_ht_total_15(self, match_odd_page_soup, market):
        '''
        Получить коэффициенты матча по категории "1-й тайм: тотал 1.5"
        :param match_odd_page_soup: общая таблица кэфов
        :return:
        '''

        odd_sources = OddSource.objects.filter(title='Flashscore')
        allowed_bookmakers = [odd_source.bookmaker.title for odd_source in odd_sources]
        market_name = market.market_name_en

        odds_data = {'market_type': market_name, 'odds_info': []}

        total_all_tables = match_odd_page_soup.find('div', {'id': 'block-under-over-1hf'})
        try:
            table_15 = total_all_tables.find('table', {'id': 'odds_ou_1.5'})
        except AttributeError:
            return odds_data
        table_rows = table_15.find_all('tr')
        header, bookmakers_rows = table_rows[0], table_rows[1:]
        for bookmaker_row in bookmakers_rows:
            bookmaker_string = bookmaker_row.find('a', 'elink').attrs['title']
            if bookmaker_string not in allowed_bookmakers:
                continue
            block_odds = bookmaker_row.find_all('td', 'kx')
            runner_over = self.__get_option_odd(block_odds[0], 'Over 1.5 Goals')
            runner_under = self.__get_option_odd(block_odds[1], 'Under 1.5 Goals')

            current_bookmaker_data = {'bookmaker': bookmaker_string,
                                      'runners': [runner_over, runner_under]
                                      }

            odds_data['odds_info'].append(current_bookmaker_data)
        return odds_data

    def __get_match_odds_correct_score(self, match_odd_page_soup, market):
        '''
        Получить коэффициенты матча по категории "Точный счет"
        :param match_odd_page_soup: общая таблица кэфов
        :return:
        '''
        odd_sources = OddSource.objects.filter(title='Flashscore')
        allowed_runners_names = [runner.runner_name for runner in market.runners.all()]
        allowed_bookmakers = [odd_source.bookmaker.title for odd_source in odd_sources]
        market_name = market.market_name_en

        odds_data = {'market_type': market_name, 'odds_info': []}
        try:
            total_all_tables = match_odd_page_soup.find('div', {'id': 'block-correct-score-ft'})
            tables_for_correct_score_odds = total_all_tables.find_all('table', 'odds sortable')
        except AttributeError:
            return odds_data

        # Учитывая, что в таблице точного счета несколько таблиц, стандартный формат массива не
        # подходит для использования во внутренних операциях, поэтому выбираем другой,
        # котоырй потом конвертируем в общий
        bookmakers_data = {}
        for odd_table in tables_for_correct_score_odds:
            table_rows = odd_table.find('tbody').find_all('tr')
            for bookmaker_row in table_rows:
                score_position_key = bookmaker_row.find('td', 'correct_score').text
                # Нам нужны не все варианты счета подряд, а только те, которые присутствуют в системе
                if score_position_key not in allowed_runners_names:
                    break
                try:
                    bookmaker_string = bookmaker_row.find('a', 'elink').attrs['title']
                except AttributeError as err:
                    raise err
                if bookmaker_string not in allowed_bookmakers:
                    continue
                if bookmaker_string not in bookmakers_data.keys():
                    bookmakers_data[bookmaker_string] = []
                correct_score_odd_span = bookmaker_row.find('td', 'kx').find('span')
                correct_score_odd = correct_score_odd_span.string
                if self.__is_trend_exist(correct_score_odd_span.attrs['alt']):
                    correct_score_trend = self.__convert_trend_string(correct_score_odd_span)
                    correct_score_trend.reverse()
                else:
                    correct_score_trend = None

                bookmakers_data[bookmaker_string].append(
                    {'runner_name': score_position_key,
                     'value': correct_score_odd,
                     'trend': correct_score_trend})
        formatted_odds_data = [{'bookmaker': book, 'runners': book_odds} for book, book_odds in bookmakers_data.items()]
        odds_data['odds_info'] = formatted_odds_data
        return odds_data

    def __get_match_odds_winner(self, match_odd_page_soup, market):
        '''
        Получить коэффициенты матча по категории "Winner"
        :param match_odd_page_soup: общая таблица кэфов
        :param market: Объект модели маркета "Winner"
        :return: словарь с коэффициентами
        '''

        odd_sources = OddSource.objects.filter(title='Flashscore')
        allowed_bookmakers = [odd_source.bookmaker.title for odd_source in odd_sources]
        bts_market_name = market.market_name_en
        odds_data = {'market_type': bts_market_name, 'odds_info': []}

        odd_score_soup = match_odd_page_soup.find('div', {'id': 'block-1x2-ft'})
        try:
            odd_table = odd_score_soup.find('table', {'id': 'odds_1x2'})
        except AttributeError:
            return odds_data
        table_rows = odd_table.find_all('tr')
        header, bookmakers_rows = table_rows[0], table_rows[1:]
        for bookmaker_row in bookmakers_rows:
            bookmaker_string = bookmaker_row.find('a', 'elink').attrs['title']
            if bookmaker_string not in allowed_bookmakers:
                continue
            block_odds = bookmaker_row.find_all('td', 'kx')
            runner_home = self.__get_option_odd(block_odds[0], 'Home Team')
            runner_draw = self.__get_option_odd(block_odds[1], 'The Draw')
            runner_away = self.__get_option_odd(block_odds[2], 'Away Team')

            current_bookmaker_data = {'bookmaker': bookmaker_string,
                                      'runners': [runner_home, runner_draw, runner_away]
                                      }
            odds_data['odds_info'].append(current_bookmaker_data)
        return odds_data

    def __get_league_name_slug(self, league):
        '''
        Получить названия лиги, используемое для нее на flashscore (к примеру, везде может быть
         Bundesliga 2, а на flashscore - Second Bundesliga)
        :param league: объект League
        :return:
        '''
        alias = LeagueNameAlias.objects.get(league=league, service_name=ServicesTypes.flashscore)
        slug = alias.league_name.lower().replace(' ', '-').replace('.', '')
        return slug

    def __convert_trend_string(self, span_tag):
        '''
        У многих коэффициентов хранится т.н. тренд - то есть рядом есть "стрелочка",
         при наведении на которую высвечивается, какой кэф был у события ранее. Эту своего
         рода историю мы и извлекаем, возвращая в виде списка значений, от старого к новому
        :param span_tag:
        :return:
        '''
        string = span_tag.attrs['alt']
        if '[d]' in string:
            delimeter = '[d]'
        else:
            delimeter = '[u]'
        return string.replace(':', '').split(delimeter)

    def __is_trend_exist(self, alt_string):
        '''
        Проверить, есть ли информацию о тренде в принципе
        :param alt_string: содержимое тэга, котоырй обычно хранит данные о тренде
        :return:
        '''
        return '[u]' in alt_string or '[d]' in alt_string

    def __get_option_odd(self, tag_with_cell, runner_name):
        '''
        Получить значение коэффициента для конкретного типа исхода
        :param tag_with_cell:
        :param runner_name:
        :return:
        '''
        cell_span = tag_with_cell.find('span')
        value = cell_span.string
        if self.__is_trend_exist(cell_span.attrs['alt']):
            value_trend = self.__convert_trend_string(cell_span)
            value_trend.reverse()
        else:
            value_trend = None
        return {'runner_name': runner_name, 'value': value, 'trend': value_trend}

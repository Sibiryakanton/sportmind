from requests import get
from json import dumps, loads
from django.conf import settings

import os


class SportmonksSoccerAPIManager:
    '''
    Класс-менеджер, в котором реализованы методы запросов к SoccerAPI сервиса Sportmonks,
    предоставляющего данные о спортивных матчах, и методы сохранения в базу
    '''
    between_dates_file = 'fixtures.json'
    between_dates_file_name = os.path.join(settings.MEDIA_ROOT, between_dates_file)

    SPORTMONKS_TOKEN = settings.SPORTMONKS_TOKEN
    SPORTMONKS_ROOT_URL = settings.SPORTMONKS_ROOT_URL
    BASE_DATA = {'api_token': SPORTMONKS_TOKEN}
    FIXTURES_INCLUDES_LIST = ['localTeam.latest.cards', 'localTeam.latest.localTeam', 'localTeam.latest.visitorTeam',
                              'localTeam.latest.stats', 'visitorTeam.latest.cards', 'visitorTeam.latest.localTeam',
                              'visitorTeam.latest.visitorTeam', 'visitorTeam.latest.stats', 'season', 'venue',
                              'odds', 'cards', 'league.country', 'stats', 'sidelined']
    FIXTURES_INCLUDES = ','.join(FIXTURES_INCLUDES_LIST)
    TEAM_INCLUDES_LIST = ['latest.localTeam', 'latest.visitorTeam', 'latest.cards']
    TEAM_INCLUDES = ','.join(TEAM_INCLUDES_LIST)
    DEPRECATED_INCLUDES = 'stats,goals,bench,visitorCoach,localCoach,lineup.player.stats'  # Не используемые опции

    def fixtures_specific_date(self, date):
        '''
        Получить фикстуры по конкретной дате
        :param date: Дата формата ГГГГ-ММ-ДД, по которой нужно брать фикстуры
        :return dict response_json: Словарь со списком фикстур
        '''
        additional_url = '/fixtures/date/{}'.format(date)
        data = {'api_token': self.SPORTMONKS_TOKEN, 'include': self.FIXTURES_INCLUDES}
        response = get(self.SPORTMONKS_ROOT_URL + additional_url, params=data)
        if response.status_code != 200:
            self.create_error_obj(response.text, response.status_code)
            print(response.text)
        response_json = loads(response.text)
        self.save_pretty_json(response_json)
        return response_json['data']

    def fixtures_between_dates(self, first_date, second_date):
        '''
        Запросить фикстуры матчей, проходящих между указанными датами (в формате ГГГГ-ММ-ДД)

        :param first_date: Дата формата ГГГГ-ММ-ДД, от которой (включительно) нужно брать фикстуры
        :param second_date: Дата формата ГГГГ-ММ-ДД, до которой (включительно) нужно брать фикстуры
        :return dict response_json: Словарь со списком фикстур
        '''
        additional_url = '/fixtures/between/{}/{}'.format(first_date, second_date)
        data = {'api_token': self.SPORTMONKS_TOKEN, 'include': self.FIXTURES_INCLUDES}
        response = get(self.SPORTMONKS_ROOT_URL + additional_url, params=data)
        if response.status_code != 200:
            self.create_error_obj(response.text, response.status_code)
            print(response.text)
        response_json = loads(response.text)
        self.save_pretty_json(response_json)
        return response_json['data']

    def fixtures_specific_id(self, fixture_id):
        '''
        Запросить фикстуру с конкретным id
        :param integer fixture_id: ID Фикстуры в базе Sportmonks
        '''
        additional_url = '/fixtures/{}'.format(fixture_id)
        data = {'api_token': self.SPORTMONKS_TOKEN, 'include': self.FIXTURES_INCLUDES}
        response = get(self.SPORTMONKS_ROOT_URL + additional_url, params=data)
        response_json = loads(response.text)

        self.save_pretty_json(response_json)
        return response_json['data']

    def fixtures_id_list(self, fixture_ids):
        fixtures_string = ','.join(fixture_ids)
        additional_url = '/fixtures/multi/{}'.format(fixtures_string)
        data = {'api_token': self.SPORTMONKS_TOKEN, 'include': self.FIXTURES_INCLUDES}
        response = get(self.SPORTMONKS_ROOT_URL + additional_url, params=data)
        if response.status_code != 200:
            self.create_error_obj(response.text, response.status_code)
            print(response.text)
        response_json = loads(response.text)
        for i in response_json['data']:
            print(i['id'], i['sidelined'])
        self.save_pretty_json(response_json)
        return response_json['data']

    def team_specific_id(self, team_id):
        '''
        Запросить фикстуры команды с конкретным id
        :param integer team_id: ID команды в базе Sportmonks
        '''
        additional_url = '/teams/{}'.format(team_id)
        data = {'api_token': self.SPORTMONKS_TOKEN, 'include': self.TEAM_INCLUDES}
        response = get(self.SPORTMONKS_ROOT_URL + additional_url, params=data)
        response_json = loads(response.text)
        self.save_pretty_json(response_json)
        return response_json['data']

    def save_pretty_json(self, data):
        '''
        Сохранение json-ответа сервера в удобном для чтения виде. Сохраненные файлы кладутся в папку settings.MEDIA_ROOT
        :param dict data: Данные в формате JSON
        :param string input_file_name: Имя файла, в который будут записаны результаты
        '''
        input_file_name = self.between_dates_file_name
        os.mkdir(settings.MEDIA_ROOT) if not os.path.exists(settings.MEDIA_ROOT) else None
        with open(input_file_name, 'w') as input_file:
            new_json = dumps(data, indent=4)
            input_file.write(new_json)

    def create_error_obj(self, message, status_code, error_type='Соединение со Sportmonks',):
        '''
        Зафиксировать ошибку, связанную с получением данных от Sportmonks, в базе данных
        :param message: Ответ сервера в полном виде
        :param error_type: Краткое описание типа ошибки
        :param status_code: статус ответа сервера (404, 500 и т.п.)
        '''
        Error.objects.create(text=message, error_type=error_type, status_code=status_code)

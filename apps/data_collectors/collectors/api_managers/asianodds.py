import requests
import json
import os
from django.conf import settings
import logging
from .base_api_manager import BaseAPIManager
log = logging.getLogger('django.parsers')


class AsianOddsAPIManager(BaseAPIManager):
    '''
    API-менеджер для взаимодействия с конторой asianodds88.com
        Email: support@asianodds88.com
        Skype ID: asianodds88
        Documentation: https://www.asianodds88.com/Documentation.aspx

    ПРИМЕЧАНИЕ: от интеграции с AsianOdds решено отказаться по бизнес-причинам
     (если компания не принимает банковские платежи или более-менее известные кошельки,
     а расчитывается только очень мутными кошельками, причем после оплаты нужно самому оповестить техподдержку
      об оплате... Учитывая, что речь о букмекерской конторе, это очень несерьезный уровень
    '''

    root = 'https://webapi.asianodds88.com/AsianOddsService/'

    def __init__(self, username, password):
        self.session = requests.Session()
        self.session.headers = {'accept': 'application/json'}
        login_response = self.login(username, password)
        login_json = login_response.json()

        if login_response.status_code == 200:
            token = login_json['Result']['Token']
            key = login_json['Result']['Key']
            self.session.headers.update({'AOToken': token, 'AOKey': key})
            register_response = self.register(username)
            if register_response.status_code != 200:
                raise AttributeError('Error in register method')
            self.session.headers.pop('AOKey')
        else:
            log.error(json.dumps(login_json))
            raise AttributeError('Incorrect credentials')

    def login(self, username, password):
        '''
        Authenticate the user with the system and obtain
        the AOToken and AOKey for authorization. It also provides the URL.
        Once the user has called this method, he/she have 60 seconds
        to be authorized. Authorization is done by calling Register method.
        :param username:
        :param password:
        :return:
        '''
        data = locals()
        relative_url = 'Login'
        response = self._make_request('get', relative_url, data)
        print(response.json())
        return response.json()

    def register(self, username):
        data = {'username': username}
        response = self._make_request('get', 'Relative', data)
        print(response.json())
        return response

    def get_bets(self):
        '''
        This method shows all the bets placed by the user, both currently
        running and currently not on running state such as Pending, Void,
         Sending, etc. The maximum number of bet information returned is 150.
        :return:
        '''
        response = self._make_request('get', 'GetBets')
        return response

    def get_running_bets(self):
        '''
        This method shows all the running bets placed by the user.
        This is similar to clicking the MyBets from the Asianodds
        we platform. The maximum number of bet information returned is 50.
        :return:
        '''
        response = self._make_request('get', 'GetRunningBets')
        return response

    def get_not_running_bets(self):
        '''
        This method shows all of the non-running bets placed by the user. The maximum number of bet information returned is 100.
        :return:
        '''
        response = self._make_request('get', 'GetNonRunningBets')
        return response.json()

    def get_account_summary(self):
        response = self._make_request('get', 'GetAccountSummary')
        return response.json()

    def get_history_statement(self):
        pass

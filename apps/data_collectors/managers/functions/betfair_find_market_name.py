from apps.data_collectors.collectors.api_managers.betfair import BetFairAPIManager
import json
import logging

log = logging.getLogger('django.parsers')


def helper_find_market_name(market_name):
    '''
    Названия маркетов, равно как и раннеров, дожлны в точности соответствовать тем, что есть в BetFair.
     Автоматически искать не получится, придется вбивать разные строки и искать наобум
    :return:
    '''
    manager = BetFairAPIManager(log_mode=False)
    markets = manager.list_market_catalogue(text_query=market_name)
    with open('test-betfair-markets.txt', 'w') as inf:
        inf.write(json.dumps(markets, indent=4))
    for market in markets:
        name = market['marketName']
        if market_name in name:
            print(name)
            print(market['marketId'])
            runners = market['runners']
            for runner in runners:
                print('--', runner['selectionId'], runner['runnerName'])
            return None

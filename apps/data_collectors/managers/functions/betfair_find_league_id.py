from apps.data_collectors.collectors.api_managers.betfair import BetFairAPIManager
import json
import logging

log = logging.getLogger('django.parsers')


def helper_find_league_id(country_code, league_name):
    '''
    Так в на стороне Betfair как-то все не прозрачно с кодами, поэтому лучше делать запрос подходящих элементов,
    чтобы пользователь выбрал вручную
    :return:
    '''
    manager = BetFairAPIManager(log_mode=False)
    event_type_id = 1
    competitions = manager.list_competitions(event_type_ids=[event_type_id], text_query=league_name,
                                             market_countries=[country_code])
    for competition in competitions:
        info = {'id': competition['competition']['id'], 'name': competition['competition']['name']}
        print(info)

from apps.data_collectors.managers.controllers import BetfairController
import logging


log = logging.getLogger('django.parsers')


def upload_odds(debug=False):
    manager = BetfairController()
    manager.upload_odds(debug=debug)

from apps.data_collectors.managers.controllers import XscoreController


def xscore_collect_dataset():
    manager = XscoreController()
    manager.fetch_and_save_matches_data(collect_train_dataset=True)


def xscore_last_matches(debug=False):
    manager = XscoreController()
    manager.fetch_and_save_matches_data(collect_train_dataset=False, debug=debug)

from apps.data_collectors.managers.controllers.flashscore_controller import FlashscoreController


def flashscore_actual_matches(debug=False):
    manager = FlashscoreController()
    manager.fetch_and_save_actual_matches(debug)

from apps.data_collectors.managers.controllers.flashscore_controller import FlashscoreController


def flashscore_last_results(debug=False):
    manager = FlashscoreController()
    manager.fetch_and_save_matches_data_results(debug)

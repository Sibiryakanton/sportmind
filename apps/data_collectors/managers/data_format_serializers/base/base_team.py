from rest_framework import serializers
from apps.data_collectors.models import Team
from django.utils.timezone import timedelta


class BaseTeamInfoSerializer(serializers.Serializer):
    name = serializers.CharField()
    event_date = serializers.DateField()

    def save_team(self, league, season, tour):
        data = self.validated_data
        team_data = {'league': league, 'tour': tour, 'season': season}
        hours_before = data['event_date'] - timedelta(hours=24)
        hours_after = data['event_date'] + timedelta(hours=24)
        team = Team.objects.filter(title=data['name'], event_date__gte=hours_before,
                                   event_date__lte=hours_after).first()
        if team is None:
            team = Team.objects.get_or_create(title=data['name'], event_date=data['event_date'], **team_data)[0]
        return team

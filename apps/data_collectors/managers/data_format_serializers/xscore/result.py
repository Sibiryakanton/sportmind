from rest_framework import serializers
from apps.data_collectors.models import Result
from .team_stat import XscoreTeamStatSerializer


class XscoreResultInfoSerializer(serializers.Serializer):
    first_team = XscoreTeamStatSerializer()
    second_team = XscoreTeamStatSerializer()

    def save_result(self, event_datetime, league, season, tour):
        data = self.validated_data
        result_data = {'league': league, 'tour': tour, 'season': season}

        first_team_stats = self.process_one_team(data['first_team'], event_datetime)
        second_team_stats = self.process_one_team(data['second_team'], event_datetime)
        result = Result.objects.update_or_create(first_team_stats=first_team_stats, second_team_stats=second_team_stats,
                                                 defaults=result_data)[0]
        return result

    def process_one_team(self, team_data, event_datetime):
        team_serializer = XscoreTeamStatSerializer(data=team_data)
        team_serializer.is_valid()
        team_stat = team_serializer.save_team_statistic(event_datetime)
        return team_stat

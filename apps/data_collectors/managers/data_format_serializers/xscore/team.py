from rest_framework import serializers
from .xg_values import XscoreXGDataSerializer
from apps.data_collectors.models import Team, League
from django.utils.timezone import timedelta
from apps.data_collectors.managers.data_format_serializers.base import BaseTeamInfoSerializer


class XscoreTeamInfoSerializer(BaseTeamInfoSerializer):
    xg_data = XscoreXGDataSerializer(many=True, allow_null=True)

    def save_team(self, league, season, tour):
        data = self.validated_data
        team = super().save_team(league, season, tour)
        for xg in data['xg_data']:
            xg_serializer = XscoreXGDataSerializer(data=xg)
            xg_serializer.is_valid()
            xg_serializer.save_xg_group(team)
        return team

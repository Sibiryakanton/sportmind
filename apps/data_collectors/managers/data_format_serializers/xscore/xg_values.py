from rest_framework import serializers
from apps.data_collectors.models import XGParameter, XGMatchValue


class XscoreXGElementSerializer(serializers.Serializer):
    name = serializers.CharField(label='Название xg-значения')
    description = serializers.CharField(label='Описание сути xg-значения')
    value = serializers.DecimalField(max_digits=5, decimal_places=2, label='Значение')

    def save_xg_value(self, team, xg_group_type, subtype_name):
        data = self.validated_data
        xg_parameter = XGParameter.objects.get_or_create(name=data['name'], description=data['description'],
                                                         group_type=xg_group_type, subgroup_type=subtype_name)[0]
        xg_value = XGMatchValue.objects.update_or_create(team=team, xg_parameter=xg_parameter,
                                                         defaults={'value': data['value']})[0]
        return xg_value


class XscoreXGSubgroupSerializer(serializers.Serializer):
    subtype_name = serializers.CharField()
    values = XscoreXGElementSerializer(many=True)

    def save_subgroup(self, team, xg_group_type):
        data = self.validated_data
        subgroup_values_list = []
        for value in data['values']:
            value_serializer = XscoreXGElementSerializer(data=value)
            value_serializer.is_valid()
            xg_value = value_serializer.save_xg_value(team, xg_group_type, data['subtype_name'])
            subgroup_values_list.append(xg_value)
        return subgroup_values_list


class XscoreXGDataSerializer(serializers.Serializer):
    xg_group_type = serializers.CharField()
    data = XscoreXGSubgroupSerializer(many=True)

    def save_xg_group(self, team):
        data = self.validated_data
        group_values_list = []
        for subgroup in data['data']:
            subgroup_serializer = XscoreXGSubgroupSerializer(data=subgroup)
            subgroup_serializer.is_valid()
            subgroup_values = subgroup_serializer.save_subgroup(team, data['xg_group_type'])
            group_values_list.append(subgroup_values)
        return group_values_list


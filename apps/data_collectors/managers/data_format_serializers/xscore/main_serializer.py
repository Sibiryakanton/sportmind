from rest_framework import serializers
from .team import XscoreTeamInfoSerializer
from .result import XscoreResultInfoSerializer
from apps.data_collectors.models import Country, League, Match
from apps.common.enums import EventStatusTypes


class XscoreMainSerializer(serializers.Serializer):
    '''
    Общий сериализатор данных, которые дожлны прийти от сервиса Xscore.
    Сериализатор описывается ожидаемый формат, проводит валидацию, а затем с помощью
     подчиненных сериализаторов поэтапно сохраняет данные о матче
    '''
    xscore_id = serializers.CharField(allow_null=True)

    first_team_info = XscoreTeamInfoSerializer()
    second_team_info = XscoreTeamInfoSerializer()
    result_info = XscoreResultInfoSerializer(allow_null=True)
    event_datetime = serializers.DateTimeField()
    country = serializers.SlugRelatedField(queryset=Country.objects.filter(is_active=True),
                                           slug_field='country_name_en')
    league = serializers.SlugRelatedField(queryset=League.objects.filter(is_active=True),
                                          slug_field='xscore_id')
    season = serializers.CharField()
    tour = serializers.IntegerField(min_value=1)

    def save_team(self, team_info):
        data = self.validated_data
        league = data['league']
        season = data['season']
        tour = data['tour']
        first_team_serializer = XscoreTeamInfoSerializer(data=team_info)
        first_team_serializer.is_valid(raise_exception=True)
        first_team = first_team_serializer.save_team(league=league, season=season, tour=tour)
        return first_team

    def save_match(self):
        '''
        сохраняем объекты матча с следующем порядке:
        команды
         xg-стату на момент матча, если есть
         сам матч
         результат матча, если есть
        :return:
        '''
        data = self.validated_data
        event_datetime = data.pop('event_datetime')
        league = data['league']
        season = data['season']
        tour = data['tour']
        first_team = self.save_team(data.pop('first_team_info'))
        second_team = self.save_team(data.pop('second_team_info'))

        result_info = data.pop('result_info')
        # Так как xscore присылает только саму дату начала события, без времени, то на случай, если дату подгрузил
        # другой парсер, мы проверяем, есть ли связанный с этим матчем экземпляр модели в базе, и есть есть,
        # обновляем данные, но время не трогаем. Если оно уже указано, то значит, нашелся источник с точным
        # временем начала события и именно его надо использовать дальше
        match_info = {'xscore_id': data['xscore_id'], 'league': data['league'], 'season': data['season'],
                      'tour': data['tour']}
        match, is_created, is_correct = Match.objects.update_or_create_with_alias_using(first_team=first_team,
                                                                                        second_team=second_team,
                                                                                        defaults=match_info)
        if not is_correct:
            return None
        if is_created:
            match.event_datetime = event_datetime
            match.save()
        else:
            event_datetime = match.event_datetime
        if result_info is not None:
            result_serializer = XscoreResultInfoSerializer(data=result_info)
            result_serializer.is_valid()
            result = result_serializer.save_result(event_datetime, league=league, season=season, tour=tour,)
            match.result = result
            match.event_status = EventStatusTypes.FINISHED
            match.save()
        return match

from rest_framework import serializers
from apps.data_collectors.models import TeamStatistic


class XscoreTeamStatSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=50)
    score = serializers.IntegerField(min_value=0)
    xg_score = serializers.DecimalField(max_digits=4, decimal_places=2, allow_null=True)
    is_first_goal = serializers.BooleanField(default=False)

    def save_team_statistic(self, event_datetime):
        data = self.validated_data
        defaults = {'score': data['score'], 'is_first_goal': data['is_first_goal']}
        team_stat = TeamStatistic.objects.update_or_create(team_title=data['title'],
                                                           event_datetime=event_datetime,
                                                           defaults=defaults)[0]
        return team_stat

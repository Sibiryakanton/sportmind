from rest_framework import serializers
from apps.data_collectors.managers.data_format_serializers.base import BaseTeamInfoSerializer


class FlashscoreTeamInfoSerializer(BaseTeamInfoSerializer):
    name = serializers.CharField()
    event_date = serializers.DateField()

    def save_team(self, league, season, tour):
        team = super().save_team(league, season, tour)
        return team

from rest_framework import serializers
from apps.data_collectors.models import TeamStatistic


class FlashscoreTeamStatSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=50)
    score = serializers.IntegerField(min_value=0)
    score_1_half = serializers.IntegerField(min_value=0)

    def save_team_statistic(self, event_datetime):
        data = self.validated_data
        title = data.pop('title')
        team_stat = TeamStatistic.objects.update_or_create(team_title=title,
                                                           event_datetime=event_datetime,
                                                           defaults=data)[0]
        return team_stat

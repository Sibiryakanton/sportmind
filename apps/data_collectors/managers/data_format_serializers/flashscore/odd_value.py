from rest_framework import serializers
from apps.data_collectors.models import TeamStatistic, OddModel, Bookmaker, Match, EventRunnerType, OddSource, EventMarketType


class FlashscoreRunnerOddSerializer(serializers.Serializer):
    value = serializers.FloatField()
    runner_name = serializers.CharField()
    trend = serializers.ListField(child=serializers.FloatField(), allow_null=True)

    def custom_validate(self, market_type):
        data = self.validated_data
        errors = {}
        try:
            runner = EventRunnerType.objects.get(runner_name=data['runner_name'], event_type=market_type)
            self.validated_data['runner_name'] = runner
        except EventRunnerType.DoesNotExist:
            errors['runner_name'] = 'The runner with name {} in market {} ' \
                                    'does not exist'.format(data['runner_name'], market_type.market_name_en)
        if errors:
            raise serializers.ValidationError(errors)

    def save_odd(self, match, odd_source, market_type):
        self.custom_validate(market_type)
        data = self.validated_data
        defaults = {'value': data['value'], 'trend_data': data['trend']}
        odd = OddModel.objects.update_or_create(match=match, odd_source=odd_source, event_type=data['runner_name'],
                                                defaults=defaults)[0]
        return odd


class FlashscoreBookmakerOddSerializer(serializers.Serializer):
    runners = FlashscoreRunnerOddSerializer(many=True)
    bookmaker = serializers.SlugRelatedField(slug_field='title', queryset=Bookmaker.objects.all())

    def save_bookmaker_odds(self, match, market_type):
        data = self.validated_data
        odds_list = []
        for i in data['runners']:
            runner_serializer = FlashscoreRunnerOddSerializer(data=i)
            runner_serializer.is_valid(raise_exception=True)
            odd_source = OddSource.objects.get(bookmaker=data['bookmaker'], title='Flashscore')
            odd = runner_serializer.save_odd(match, odd_source, market_type)
            odds_list.append(odd)
        return odds_list


class FlashscoreOddsTableSerializer(serializers.Serializer):
    market_type = serializers.SlugRelatedField(slug_field='market_name_en', queryset=EventMarketType.objects.all())
    odds_info = FlashscoreBookmakerOddSerializer(many=True)

    def save_odd_table(self, match):
        data = self.validated_data
        all_odds = []
        for bookmaker_odds in data['odds_info']:
            odd_serializer = FlashscoreBookmakerOddSerializer(data=bookmaker_odds)
            odd_serializer.is_valid(raise_exception=True)
            bookmaker_odds = odd_serializer.save_bookmaker_odds(match, data['market_type'])
            all_odds.extend(bookmaker_odds)
        return all_odds

from django.db.transaction import atomic
from rest_framework import serializers
from .team import FlashscoreTeamInfoSerializer
from .result import FlashscoreResultInfoSerializer
from .odd_value import FlashscoreOddsTableSerializer


from apps.data_collectors.models import Country, League, Match
from apps.common.enums import EventStatusTypes


class FlashscoreMainSerializer(serializers.Serializer):
    '''
    Общий сериализатор данных, которые дожлны прийти от сервиса Flashscore.
    Сериализатор описывается ожидаемый формат, проводит валидацию, а затем с помощью
     подчиненных сериализаторов поэтапно сохраняет данные о матче
    '''
    flashscore_id = serializers.CharField(allow_null=True)

    first_team_info = FlashscoreTeamInfoSerializer()
    second_team_info = FlashscoreTeamInfoSerializer()
    result_info = FlashscoreResultInfoSerializer(allow_null=True)
    event_datetime = serializers.DateTimeField()
    event_status = serializers.ChoiceField(choices=EventStatusTypes.CHOICES)

    country = serializers.SlugRelatedField(queryset=Country.objects.filter(is_active=True),
                                           slug_field='country_name_en')
    league = serializers.PrimaryKeyRelatedField(queryset=League.objects.filter(is_active=True))
    season = serializers.CharField()
    tour = serializers.CharField()
    odds = FlashscoreOddsTableSerializer(many=True, allow_null=True)

    def save_team(self, team_info):
        data = self.validated_data
        league = data['league']
        season = data['season']
        tour = data['tour']
        team_serializer = FlashscoreTeamInfoSerializer(data=team_info)
        team_serializer.is_valid(raise_exception=True)
        team = team_serializer.save_team(league=league, season=season, tour=tour)
        return team

    @atomic()
    def save_match(self):
        '''
        сохраняем объекты матча с следующем порядке: команды, сам матч и результат матча, если есть
        :return:
        '''
        data = self.validated_data
        event_datetime = data.pop('event_datetime')
        first_team = self.save_team(data.pop('first_team_info'))
        second_team = self.save_team(data.pop('second_team_info'))
        league = data['league']
        season = data['season']
        tour = data['tour']
        result_info = data.pop('result_info')
        event_status = data['event_status']
        match_info = {'flashscore_id': data['flashscore_id'], 'event_status': event_status,
                      'event_datetime': event_datetime, 'league': league, 'season': season,
                      'tour': tour}
        match, is_created, is_correct = Match.objects.update_or_create_with_alias_using(first_team=first_team,
                                                                                        second_team=second_team,
                                                                                        defaults=match_info)
        if not is_correct:
            return None
        if result_info is not None:
            result_serializer = FlashscoreResultInfoSerializer(data=result_info)
            result_serializer.is_valid()
            result = result_serializer.save_result(event_datetime, league=league, season=season, tour=tour)
            match.result = result
            match.save()
        if event_status == EventStatusTypes.NOT_STARTED:
            odds = data['odds']
            for odd in odds:
                odd_serializer = FlashscoreOddsTableSerializer(data=odd)
                odd_serializer.is_valid(raise_exception=True)
                odd_serializer.save_odd_table(match)
        return match

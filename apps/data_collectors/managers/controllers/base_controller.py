from abc import ABC
import threading


class BaseController(ABC):
    '''
    Абстрактный класс для API-пультов - практического применения api-менеджеров. Т.к. api-менеджеры могут содержать
    непредсказуемый функционал, это касается и пультов. поэтому единственное, что можно четко прописать в пульте -
    это, собственно, сам класс коллектора данных. Какие у него будут методы и какой функционал - на усмотрение
    разработчика
    '''

    @property
    def collector_class(self, collector_class):
        '''
        :param collector_class: класс api менеджера или парсера
        :return:
        '''
        if collector_class is None:
            raise AttributeError('Error: the collector class is required')
        return collector_class

    def process_leagues_with_threading(self, leagues, function_name, debug=False, *args, **kwargs):
        '''
        Лиги можно обрабатывать в одном потоке или раскидать их по отдельным тредам
        (вариант содним потоком нужен скорее для отладки)
        :param leagues: Список объектов League
        :param function_name: Функция дл обработки отдельно взятой лиги
        :param debug: True, если нужно выполнять команду в рамках отладки (то есть однопоточно, синхронно)
        '''
        if debug:
            for league in leagues:
                print(league.pk, league.league_name)
                function_name(league=league, *args, **kwargs)
        else:
            threads = []
            for league in leagues:
                kwargs['league'] = league
                t = threading.Thread(target=function_name, kwargs=kwargs)
                t.start()
                threads.append(t)
            for thread in threads:
                thread.join(300)

from .base_controller import BaseController
from apps.data_collectors.collectors.parsers.xscore import XscoreParser
from apps.data_collectors.managers.data_format_serializers.xscore import XscoreMainSerializer
from apps.data_collectors.models import League
from apps.common.utils import catch_manager_error
from apps.data_collectors.enums import ServicesTypes


class XscoreController(BaseController):
    '''
    Общие функции, используемые в работе с сервисом Xscore
    '''
    collector_class = XscoreParser

    def fetch_and_save_matches_data(self, collect_train_dataset=False, debug=False):
        '''
        Функция, связывающая коллектор и сериализатор для сохранения актуальных данных о матчах (если матч окончен,
        добавит результат матча)
        :param collect_train_dataset: если True, идет сбор данных обо ВСЕХ матчах лиги за все сезоны, доступные на сайте
        :return:
        '''
        leagues = League.objects.get_using_leagues()
        self.process_leagues_with_threading(leagues, function_name=self.process_league,
                                            debug=debug, collect_train_dataset=collect_train_dataset)

    @catch_manager_error(service=ServicesTypes.flashscore)
    def process_league(self, league, collect_train_dataset=False):
        collector_class = self.collector_class()
        seasons = collector_class.get_seasons(league.xscore_id)
        if collect_train_dataset:
            for season in seasons:
                tours_list = collector_class.get_tours(season['id'])
                for tour in tours_list:
                    tour_matches_data = collector_class.processing_tour_matches_list(league, season, tour)
                    for match in tour_matches_data:
                        self.check_and_save_one_match(match)
        else:
            last_season = seasons[-1]
            tours_list = collector_class.get_tours(last_season['id'])
            for tour in tours_list[-2:]:
                tour_matches_data = collector_class.processing_tour_matches_list(league, last_season, tour)
                for match in tour_matches_data:
                    self.check_and_save_one_match(match)

    def check_and_save_one_match(self, match_info):
        match_info_serializer = XscoreMainSerializer(data=match_info)
        if match_info_serializer.is_valid():
            match_info_serializer.is_valid(raise_exception=True)
        match_info_serializer.save_match()

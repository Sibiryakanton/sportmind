from django.utils.timezone import now, timedelta, datetime
from django.db import transaction
from django.conf import settings
from apps.betting.models import SportsbookProfile, WalletHistoryElem
from apps.data_collectors.collectors.api_managers.betfair import BetFairAPIManager
from apps.data_collectors.utils import get_or_update_alias
from apps.data_collectors.models import Country, EventMarketType, EventRunnerType, Match, OddModel, Bookmaker, OddSource
from apps.common.utils import catch_manager_error
from apps.data_collectors.enums import ServicesTypes
from apps.analysis.models import Tip
from apps.authentication.models import User


from .base_controller import BaseController
import logging
import os

from pytz import UTC

log = logging.getLogger('django.parsers')


class BetfairController(BaseController):
    '''
    Общие функции, используемые в работе с сервисом BetFair
    '''
    manager_class = BetFairAPIManager

    # Периодически может понадобиться записать ответы betfair по конкретному матчу, сюда указывать локальный id матча
    dev_test_id_match = None
    dev_test_dir = os.path.join(settings.BASE_DIR, 'test-betfair')

    def __init__(self):
        self.bookmaker = self.__get_bookmaker()
        if self.bookmaker is None:
            text = 'The Bookmaker with name BetFair does not exist'
            log.error(text)
            raise AttributeError(text)
        try:
            self.odd_source = OddSource.objects.get(title__isnull=True, bookmaker=self.bookmaker)
        except OddSource.DoesNotExist:
            text = 'The clear odd_source for BetFair does not exist'
            log.error(text)
            raise AttributeError(text)

    @catch_manager_error(service=ServicesTypes.betfair)
    def place_bet(self, profile, tip):
        api_manager = self.manager_class(log_mode=False)
        self.update_wallet_info()
        self.update_wallet_history()
        market_id = tip.tip_type.event_type.betfair_id
        selection_id = tip.tip_type.betfair_id
        last_back_price = tip.preferred_odd
        bet_amount = profile.wallet.calculate_bet_amount()
        print('Ставка', tip)
        raise Exception('Функция отправки ставок временно закрыта по бизнес-причинам')

        place_order_response = api_manager.place_order(market_id, selection_id, last_back_price, bet_amount)
        instruction = place_order_response.json()['instructionReports'][0]
        bet_id = instruction['betId']
        placed_datetime = instruction['placedDate']
        price = instruction['averagePriceMatched']
        size = instruction['sizeMatched']
        defaults = {'relative_profit': price, 'size': size,
                    'tip': tip, 'matched_at': placed_datetime}
        WalletHistoryElem.objects.get_or_create(bet_id=bet_id, wallet=profile.wallet, defaults=defaults)

    @catch_manager_error(service=ServicesTypes.betfair)
    def update_wallet_info(self):
        '''
        Обновить баланс счета и историю
        '''
        manager = self.manager_class(log_mode=False)

        bookmaker = self.__get_bookmaker()
        if bookmaker is None:
            return
        account = SportsbookProfile.objects.filter(bookmaker=bookmaker, is_active=True).first()
        log.info('Start balance uploading')
        funds = manager.get_account_funds().json()
        wallet = account.wallet
        wallet.balance = funds['availableToBetBalance']
        wallet.save()

        self.__update_bet_balance_signal(wallet)

    @catch_manager_error(service=ServicesTypes.betfair)
    def update_wallet_history(self):
        '''
        Получить информацию о всех ставках, сделанных с кошелька, и на их основе создать историю кошелька
        :return:
        '''
        bookmaker = self.__get_bookmaker()
        if bookmaker is None:
            return
        betting_manager = self.manager_class(log_mode=False)
        cleared_orders_response = betting_manager.list_cleared_orders().json()
        for order in cleared_orders_response['clearedOrders']:
            bet_id = order['betId']

            event_id = int(order['eventId'])
            selection = order['selectionId']
            bet_size = order['sizeSettled']
            profit = order['profit']
            odd_price = order['priceMatched']
            matched_date = order['lastMatchedDate']
            settled_date = order['settledDate']

            match = Match.objects.filter(betfair_id=event_id).first()
            if match is not None:
                selector = EventRunnerType.objects.filter(betfair_id=selection)
                tip = Tip.objects.filter(match=match, tip_type=selector)
            else:
                tip = None
            data = {'absolute_profit': profit, 'amount': bet_size, 'tip': tip,
                    'matched_at': matched_date, 'settled_at': settled_date, 'relative_profit': odd_price}
            WalletHistoryElem.objects.update_or_create(wallet=betting_manager.profile.wallet,
                                                       bet_id=bet_id, defaults=data)

    def upload_odds(self, debug=False):
        '''
        Загрузить коэффициенты с Betfair Exchange API по всем доступным лигам
        :param debug:
        :return:
        '''
        manager = self.manager_class(log_mode=False)
        log.info('Start odds uploading')
        countries_list = Country.objects.filter(is_active=True)
        for country in countries_list:
            leagues = country.leagues.get_using_leagues()
            self.process_leagues_with_threading(function_name=self.process_league, leagues=leagues, debug=debug,
                                                manager_token=manager.session_token)

    @catch_manager_error(service=ServicesTypes.betfair)
    def process_league(self, manager_token, league):
        manager = self.manager_class(log_mode=False, session_token=manager_token)
        league_id = league.betfair_id

        events = manager.list_events(competition_ids=[league_id])
        events_info = {}
        for event in events:
            events_info[event['event']['name']] = {'betfair_id': event['event']['id'],
                                                   'open_date': event['event']['openDate']}
        with transaction.atomic():
            matches_in_db = Match.objects.get_not_close_matches(league)
            for match in matches_in_db:
                # Ищем матч  по названию. Если не находим, првоеряем,
                # есть ли совпадение по одной из команд и дате. Если есть,
                # Записываем название другой команды как альтернативное и собираем кэфы, иначе пропуск
                betfair_match_id = self.__get_betfair_match_id(events_info, match)
                if betfair_match_id is None:
                    # log.error('error with processing match {}: cant find the match with same teams '
                    #          'in BetFair'.format(match_title))
                    continue
                # Есть условно два типа интересующих нас исходов: со статичными вариантами
                # ответа (Да/Нет, Конкретный счет и т.п.) и с динамичными
                # вариантами (к примеру, по исхоу победитель
                # есть только один общий исход - Ничья. Остальные варианты обозначены
                # названиями конкретных команда, а не
                # абстрактными "Гостевая команда", "Домашная команда" или вроде того
                # Поэтому мы по разной логике собираем варианты
                static_markets = EventMarketType.objects.filter(is_active=True, is_dynamic_market=False)
                market_catalogue_response = manager.list_market_catalogue(event_ids=[betfair_match_id])
                if match.pk == self.dev_test_id_match:
                    with open(os.path.join(self.dev_test_dir, 'team_check.json'), 'w') as inf:
                        import json
                        inf.write(json.dumps(market_catalogue_response, indent=4))

                odds_objects = []
                for market in static_markets:
                    market_id, market_runners = self.__get_market_info(market_catalogue_response, market)
                    if match.pk == self.dev_test_id_match:
                        filename = 'team_check_{}.json'.format(market.market_name_en.replace('/', '-'))
                        with open(os.path.join(self.dev_test_dir, filename), 'w') as inf:
                            import json
                            inf.write(json.dumps(market_catalogue_response, indent=4))
                    if market_id is None:
                        # log.error('The id for market {} in event {} not '
                        #          'found'.format(market.market_name_en, betfair_match_id))
                        continue
                    active_runners = market.runners.filter(is_active=True, betfair_id__isnull=False)
                    # Перебираем варианты исхода. Находим соответствующие
                    # им варианты на стороне букмекера, собираем их
                    # кэфы по линии BACK
                    for runner in active_runners:
                        odds_objects.append(self.__save_static_odd(manager, match, betfair_match_id,
                                                                   market_id, runner, self.odd_source))

                dynamic_markets = EventMarketType.objects.filter(is_active=True, is_dynamic_market=True)
                for market in dynamic_markets:
                    market_id, market_runners = self.__get_market_info(market_catalogue_response, market)
                    if market_id is None:
                        # log.error('The id for market {} in event {} not '
                        #          'found'.format(market.market_name_en, betfair_match_id))
                        continue
                    runners = market.runners.filter(is_active=True, betfair_id__isnull=False)
                    for runner in runners:
                        odds_objects.append(self.__save_static_odd(manager, match, betfair_match_id,
                                                                   market_id, runner, self.odd_source))
                    selectors_info = {}
                    for runner in market_runners:
                        selectors_info[runner['runnerName']] = runner['selectionId']
                    odds_objects.append(self.__save_dynamic_odd(manager, match, betfair_match_id,
                                                                market_id, selectors_info, market,
                                                                'Home Team', self.odd_source))
                    odds_objects.append(self.__save_dynamic_odd(manager, match, betfair_match_id,
                                                                market_id, selectors_info, market,
                                                                'Away Team', self.odd_source))

                match.betfair_id = betfair_match_id
                match.save()

    def __update_bet_balance_signal(self, wallet):
        '''
        Размер ставки расчитывается автоматически в зависимости от размера т.н.
         ставочного баланса - суммы, которая была на счету на момент
         начала ставок и которая онбовляется каждый месяц. Она нужна,
         чтобы избежать ненужного уменьшения размера ставки после каждой последующей.
         Эта функция проверяет, пришло ли время для обновления этой самой суммы.
        :param wallet:
        :return:
        '''
        current_time = now()
        is_first_time_update = wallet.bet_balance is None or wallet.bet_balance_updated_at is None
        if_month_first_day_update = wallet.bet_balance != wallet.balance and current_time.date().day == 1
        is_month_passed = (current_time - timedelta(days=10)) > (wallet.bet_balance_updated_at or
                                                                 UTC.localize(datetime.utcfromtimestamp(1)))
        if is_first_time_update or if_month_first_day_update or is_month_passed:
            wallet.bet_balance = wallet.balance
            wallet.bet_balance_updated_at = current_time
            wallet.save()

    def __get_bookmaker(self):
        try:
            bookmaker = Bookmaker.objects.get(title='Betfair')
        except Bookmaker.DoesNotExist:
            log.error('The Bookmaker with name BetFair does not exist')
            bookmaker = None
        return bookmaker

    def __get_betfair_match_id(self, events_info, match):
        '''
        Получить id матча в системе betfair с учетом возможных варантов названий команд.
         Если матч не найден, вернет None
        :param events_info: Полный перечень спортивных событий с Betfair
        :param match: объект матча
        :return:
        '''
        match_title = match.title_in_betfair_format()
        try:
            match_id = events_info[match_title]['betfair_id']
        except KeyError:
            all_events_titles = events_info.keys()
            first_team, second_team = match.first_team.title, match.second_team.title
            match_id = None
            for title in all_events_titles:
                is_one_team_matched = first_team in title or second_team in title
                if is_one_team_matched:
                    event_datetime = events_info[title]['open_date'].split('T')
                    event_date = event_datetime[0]
                    event_time = event_datetime[1].split('Z')[0]
                    new_event_datetime_string = '{} {}'.format(event_date, event_time)
                    date_time_obj = datetime.strptime(new_event_datetime_string, '%Y-%m-%d %H:%M:%S.%f')
                    localized_outer_date = UTC.localize(date_time_obj).date()
                    is_same_event_datetime = localized_outer_date == match.event_datetime.date()
                    if is_same_event_datetime:
                        title_as_list = title.split(' v ')
                        if first_team in title_as_list:
                            get_or_update_alias(match.second_team, match, title_as_list[1])
                        else:
                            get_or_update_alias(match.first_team, match, title_as_list[0])
                        for possible_title in match.get_all_possible_match_titles():
                            if possible_title in all_events_titles:
                                match_id = events_info[possible_title]['betfair_id']
                                break
                if match_id:
                    break
        return match_id

    def __get_market_info(self, market_catalogue_response, market_model_obj):
        '''
        Разобрать ответ сервиса с информацией о маркете,
         затем вернуть id  маркета и данные об опциях вариантов исходов
        :param market_catalogue_response:
        :param market_model_obj:
        :return:
        '''
        market_id = None
        market_runners = None
        for catalogue_elem in market_catalogue_response:
            if catalogue_elem['marketName'] == market_model_obj.market_name_en:
                market_id = catalogue_elem['marketId']
                market_runners = catalogue_elem['runners']
                break
        return market_id, market_runners

    def __update_odd_trend_data(self, odd_obj, new_odd):
        '''
        Полученный кэф добавляем в запись тренда движения кэфа на ставку
        :param odd_obj:
        :param new_odd:
        :return:
        '''
        if odd_obj.trend_data is None or odd_obj.trend_data == []:
            odd_obj.trend_data = [new_odd]
        else:
            current_trend = odd_obj.trend_data
            current_trend.reverse()
            if current_trend[-1] != new_odd:
                current_trend.append(new_odd)
            current_trend.reverse()
            if len(current_trend) > 5:
                current_trend = current_trend[:5]
            odd_obj.trend_data = current_trend
        odd_obj.save()

    def __get_odd(self, manager, match, betfair_match_id, market_id, runner_id, runner_name):
        '''
        Получить значение коэффициента для конкретного матча, маркета и варианта исхода
        :param manager: экземпляр класса BetfairAPIManager
        :param match:
        :param betfair_match_id:
        :param market_id:
        :param runner_id:
        :param runner_name:
        :return:
        '''
        match_selection_list = manager.list_runners(market_id=market_id,
                                                    selection_id=runner_id)
        if match.pk == self.dev_test_id_match:
            with open(os.path.join(self.dev_test_dir, 'runners_check_{}.json'.format(runner_name)), 'w') as inf:
                import json
                inf.write(json.dumps(match_selection_list, indent=4))

        last_runner = match_selection_list[-1]['runners'][0]
        available_to_back = last_runner['ex']['availableToBack']

        if len(available_to_back):
            odd = last_runner['ex']['availableToBack'][0]['price']
            # print('{} | {} | {} | {}| {}'.format(match.event_datetime, match_title, betfair_match_id,
            #                                     runner_name, odd))
        else:
            # log.info('Runners for match {}, market {} selection {} '
            #         'not found'.format(betfair_match_id, market_id, runner_id))
            odd = None
        return odd

    def __save_dynamic_odd(self, manager, match, betfair_match_id,
                           market_id, selectors_info, market,
                           runner_name, odd_source):
        '''
        Часть матчей имеют уникальное название и, соответственно, id. К примеру, в ставках на победителя
        нет общих ответов вроде Home Team / Draw / Away Team, вместо этого для каждого матча идут уникальные опции вроде
        Spartak / Draw / CSKA MOSCOW
        :param manager: класс менеджера api
        :param match: класс матча
        :param betfair_match_id: id матча в betfair
        :param market_id: id маркета в betfair
        :param selectors_info: список словарей с селектораи маркета
        :param market: объект маркета
        :param runner_name: Тип команды, для которой собирается коэффициент (разрешенные варианты: Home Team, Away Team)
        :return:
        '''
        home_team, away_team = 'Home Team', 'Away Team'
        if runner_name not in [home_team, away_team]:
            raise AttributeError('runner_name attribute must have value only in 2 options: Home Team, Away Team')
        if runner_name == home_team:
            team_possible_names = match.first_team.get_all_possible_titles()
        else:
            team_possible_names = match.second_team.get_all_possible_titles()
        for team_name in team_possible_names:
            if team_name in selectors_info.keys():
                runner_id = selectors_info[team_name]
                odd = self.__get_odd(manager, match, betfair_match_id,
                                     market_id, selectors_info[team_name],
                                     team_name)
                defaults = {'value': odd}
                runner_obj = market.runners.get(runner_name=runner_name)
                odd_obj = OddModel.objects.update_or_create(match=match, event_type=runner_obj,
                                                            betfair_runner_id=runner_id,
                                                            odd_source=odd_source,
                                                            market_id=market_id,
                                                            defaults=defaults)[0]
                self.__update_odd_trend_data(odd_obj, odd)
                return odd_obj

    def __save_static_odd(self, manager, match, betfair_match_id,
                          market_id, runner, odd_source):
        '''
        В отличие от __save_dynamic_odd, этот метод ориентирован на статичные маркеты,
        то есть такие, в которых варианты выбора у маркета не меняются от матча
        к матчу. К примеру, Both Teams to Score: там просто два варианта Yes/No.
        :param manager: класс менеджера api
        :param match: класс матча
        :param betfair_match_id: id матча в betfair
        :param market_id: id маркета в betfair
        :param runner: объект раннера (селектора, опции исхода матча)
        :return:
        '''
        odd = self.__get_odd(manager, match, betfair_match_id,
                             market_id, runner.betfair_id, runner.runner_name)
        if odd is not None:
            defaults = {'value': odd}
            odd_obj = OddModel.objects.update_or_create(match=match, event_type=runner,
                                                        odd_source=odd_source, market_id=market_id,
                                                        defaults=defaults)[0]
            self.__update_odd_trend_data(odd_obj, odd)
            return odd_obj

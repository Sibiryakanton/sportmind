from .base_controller import BaseController
from apps.data_collectors.collectors.parsers.flashscore import FlashScoreParser
from apps.data_collectors.managers.data_format_serializers.flashscore import FlashscoreMainSerializer
from apps.data_collectors.models import League, Match
from apps.common.enums import EventStatusTypes
from apps.common.utils import catch_manager_error
from apps.data_collectors.enums import ServicesTypes


class FlashscoreController(BaseController):
    '''
    Общие функции, используемые в работе с сервисом Flashscore
    '''
    collector_class = FlashScoreParser

    def fetch_and_save_matches_data_results(self, debug=False):
        '''
        Функция, связывающая коллектор и сериализатор для сохранения актуальных данных о матчах (если матч окончен,
        добавит результат матча)
        :return:
        '''
        leagues = League.objects.get_using_leagues()
        self.process_leagues_with_threading(leagues, self.process_league_results, debug=debug)

    def fetch_and_save_actual_matches(self, debug=False):
        '''
        Извлечь и сохранить информацию о будущих матчах
        :return:
        '''
        leagues = League.objects.get_using_leagues()
        self.process_leagues_with_threading(debug=debug, function_name=self.process_league_fixtures, leagues=leagues)

    def check_and_save_one_match(self, match_info):
        match_info_serializer = FlashscoreMainSerializer(data=match_info)
        if not match_info_serializer.is_valid():
            print(match_info)
            print(match_info_serializer.errors)
            match_info_serializer.is_valid(raise_exception=True)
        match = match_info_serializer.save_match()
        return match

    @catch_manager_error(service=ServicesTypes.flashscore)
    def process_league_results(self, league):
        collector_class = self.collector_class()
        formatted_data = collector_class.get_match_results(league)
        updated_matches = []
        for i in formatted_data:
            match = self.check_and_save_one_match(i)
            if match is not None:
                updated_matches.append(match.pk)
        # Если результатов матча нет даже спустя 10 часов после ожидаемого начала, значит, матч
        # в принципе не начался/ не закончился и его следует отметить как отложенный (PostPoned)
        postponed_matches = Match.objects.get_postponed_matches(league=league)
        postponed_matches.update(event_status=EventStatusTypes.POSTPONED)

    @catch_manager_error(service=ServicesTypes.flashscore)
    def process_league_fixtures(self, league):
        collector_class = self.collector_class()
        formatted_data = collector_class.get_match_fixtures(league)
        updated_matches = []
        for i in formatted_data:
            match = self.check_and_save_one_match(i)
            if match is not None:
                updated_matches.append(match.pk)
        # Если матч был в будущих, но пропал, то вероятно, что перешел в лайв
        print(updated_matches)
        Match.objects.get_live_matches(
            league=league, updated_matches=updated_matches).update(event_status=EventStatusTypes.LIVE)
        print('Check fixtures is done')

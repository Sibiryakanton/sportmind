from django.apps import AppConfig


class DataCollectorsConfig(AppConfig):
    name = 'data_collectors'

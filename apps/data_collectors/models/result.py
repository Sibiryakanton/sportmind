# coding=utf-8
from django.db import models
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from apps.data_collectors.models.team_statistic import TeamStatistic


class Result(models.Model):
    '''
    Объект для хранения реального результата. По полям отдельно разобран каждый параметр исхода: конкретный победитель,
    голы и карточки
    Применяется в двух конструкция: как дополнительная часть к объекту Match для хранения результата,
    либо как часть объекта Team для описания результатов последних игр команды
    '''
    league = models.ForeignKey('League', on_delete=models.CASCADE, verbose_name=_('Лига'), null=True)
    season = models.CharField(_('Сезон'), max_length=30, null=True)
    tour = models.CharField('Тур', max_length=15, null=True)

    first_team_stats = models.ForeignKey('TeamStatistic', verbose_name='Статистика первой команды',
                                         on_delete=models.SET_NULL, null=True)
    second_team_stats = models.ForeignKey('TeamStatistic', verbose_name='Статистика второй команды',
                                          on_delete=models.SET_NULL, related_name='+', null=True)
    event_datetime = models.DateTimeField('Дата и время начала события', default=timezone.now)
    date_published = models.DateTimeField('Дата и время публикации записи', default=timezone.now)

    class Meta:
        verbose_name = 'результат'
        verbose_name_plural = 'результаты'

    def winner(self):
        if self.first_team_stats.score > self.second_team_stats.score:
            return 1
        elif self.first_team_stats.score < self.second_team_stats.score:
            return 2
        return 0

    def total(self):
        return self.first_team_stats.score + self.second_team_stats.score

    def ht_total(self):
        if None not in [self.first_team_stats.score_1_half, self.second_team_stats.score_1_half]:
            return self.first_team_stats.score_1_half + self.second_team_stats.score_1_half

    def is_total_over25(self):
        return self.total() > 2.5

    def is_ht_total_over15(self):
        if self.ht_total() is not None:
            return self.ht_total() > 1.5

    def is_win_to_nil(self):
        first_team_win_to_nil = self.first_team_stats.score != 0 and self.second_team_stats.score == 0
        second_team_win_to_nil = self.second_team_stats.score != 0 and self.first_team_stats.score == 0

        if first_team_win_to_nil:
            return '1'
        elif second_team_win_to_nil:
            return '2'
        return False

    def correct_score(self):
        return '{}:{}'.format(self.first_team_stats.score, self.second_team_stats.score)

    def is_both_scored(self):
        if None not in [self.first_team_stats.score, self.second_team_stats.score]:
            return bool(self.first_team_stats.score and self.second_team_stats.score)

    def __str__(self):
        if self.first_team_stats and self.second_team_stats:
            return '{}:{}'.format(self.first_team_stats.score, self.second_team_stats.score)
        return ''


# Сигнал при удалении команд: удалить тренеров и игроков
@receiver(pre_delete, sender=Result)
def remove_result(sender, instance, **kwargs):
    try:
        if instance.first_team_stats:
            instance.first_team_stats.delete()
    except TeamStatistic.DoesNotExist:
        pass
    try:
        if instance.second_team_stats:
            instance.second_team_stats.delete()
    except TeamStatistic.DoesNotExist:
        pass

# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class OddSource(models.Model):
    '''
        Модель источника коэффициента для конкретной букмекерской конторы (кэф не всегда берется непосредственно
        у букмекера - могут использоваться агрегаторы)
    '''
    title = models.CharField(_('Наименование'), max_length=50, null=True, blank=True,
                             help_text=_('Если кэф берется непосредственно у букмекера, оставить пустым'))
    bookmaker = models.ForeignKey('Bookmaker', verbose_name=_('Название класса-менеджера'), on_delete=models.PROTECT)

    def __str__(self):
        return '{} {}'.format(self.title, self.bookmaker)

    class Meta:
        verbose_name = 'источник коэффициента'
        verbose_name_plural = 'источники коэффициентов'


# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Bookmaker(models.Model):
    '''
    Модель букмекерской конторы
    '''
    title = models.CharField(_('Наименование'), max_length=50)
    component = models.ForeignKey('common.ComponentInfo', null=True, blank=True,
                                  verbose_name=_('Компонент'), on_delete=models.SET_NULL)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'букмекерская контора'
        verbose_name_plural = 'букмекерские конторы'


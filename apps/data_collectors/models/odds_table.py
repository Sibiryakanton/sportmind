# coding=utf-8
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _


class OddModel(models.Model):
    '''
    Модель для хранения кэфа на конкретное событие матча
    '''
    match = models.ForeignKey('Match', on_delete=models.CASCADE, verbose_name='Матч', related_name='odds')
    betfair_runner_id = models.CharField(max_length=30, verbose_name=_('ID селектора в BetFair'),
                                         help_text=_('Применительно только к динамическим маркетам'),
                                         null=True, blank=True)
    odd_source = models.ForeignKey('OddSource', on_delete=models.CASCADE, verbose_name='источник кэфа', null=True)
    trend_data = ArrayField(verbose_name=_('Движение кэфа'), null=True, base_field=models.FloatField(),
                            blank=True)
    event_type = models.ForeignKey('EventRunnerType', on_delete=models.CASCADE, verbose_name='Тип события')
    market_id = models.CharField('ID Маркета', null=True, max_length=100)
    value = models.DecimalField('Значение', max_digits=8, decimal_places=4)
    updated_at = models.DateTimeField('Время обновления', auto_now=True)

    def __str__(self):
        return '{} {} {} ({})'.format(self.match, self.event_type, self.value, self.odd_source)

    class Meta:
        verbose_name = 'Коэффициент ставки'
        verbose_name_plural = 'Коэффициены ставок'
        ordering = ['-match__event_datetime']

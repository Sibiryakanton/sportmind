# coding=utf-8
from django.db import models
from apps.data_collectors.models import TeamAliasArray
from django.utils.translation import ugettext_lazy as _


class Team(models.Model):
    '''
    Описание команды на момент конкретного матча, то есть одна и та же команда может иметь несколько
    объектов Team, привязанных к разным матчам.
    '''
    title = models.CharField(_('Наименование'), max_length=50)
    league = models.ForeignKey('League', null=True, verbose_name=_('Лига'), on_delete=models.SET_NULL)
    season = models.CharField(_('Сезон'), max_length=30, null=True)
    tour = models.CharField(_('Тур'), null=True, max_length=15)
    event_date = models.DateField(_('Дата события'), null=True, blank=True)
    is_favourite = models.BooleanField(_('Фаворит'), default=False)

    def __str__(self):
        return '{} ({})'.format(self.title, self.pk)

    def get_all_possible_titles(self):
        '''
        :return:
        '''
        alias_obj = TeamAliasArray.objects.filter(titles_array__contains=[self.title]).first()
        if alias_obj is not None:
            return alias_obj.titles_array
        return [self.title]

    class Meta:
        verbose_name = 'команду'
        verbose_name_plural = 'команды'


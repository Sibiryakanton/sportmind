# coding=utf-8
from django.db import models
from django.utils import timezone


class BaseHuman(models.Model):
    name = models.CharField('Имя', max_length=50)
    lastname = models.CharField('Фамилия', max_length=50)
    common_name = models.CharField('Обычная запись имени и фамилии', max_length=100)
    birth_date = models.DateField('Дата рождения', default=timezone.now, null=True)
    country = models.CharField('Страна', max_length=50)
    date_published = models.DateTimeField('Дата и время публикации записи', default=timezone.now)

    def __str__(self):
        return self.common_name

    class Meta:
        abstract = True

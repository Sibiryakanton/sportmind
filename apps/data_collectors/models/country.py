# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    '''
    Модель для ифнормации о стране
    '''
    country_name_ru = models.CharField(_('Название страны по-русски'), max_length=300)
    country_name_en = models.CharField(_('Название страны по-английски'), max_length=300)

    xscore_id = models.PositiveIntegerField(_('ID страны в xscore.win'), null=True)
    code = models.CharField(_('Код страны'), max_length=10)
    is_active = models.BooleanField(_('Используется в работе'), default=True)

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'
        ordering = ["country_name_en"]

    def __str__(self):
        return '({}) {}'.format(self.code, self.country_name_en)

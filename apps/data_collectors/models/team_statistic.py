# coding=utf-8
from django.db import models


class TeamStatistic(models.Model):
    '''
    Объект для хранения статистики команды в конкретном матче
    Кол-во пасов, обычных атак и опасных, кол-во ударов по воротам и мимо, кол-во голов, процент времени владения мячом
    - эти и другие подобные данные будут храниться в данном экземпляре
    '''

    team_title = models.CharField('Первая команда', max_length=100, null=True)
    score = models.PositiveIntegerField('Кол-во голов', null=True)
    score_1_half = models.PositiveIntegerField('Кол-во голов в первом тайме', null=True)
    shots_total = models.PositiveIntegerField('Кол-во ударов в сторону сетку', null=True)
    shots_ongoal = models.PositiveIntegerField('Кол-во ударов по воротам', null=True)
    shots_offgoal = models.PositiveIntegerField('Кол-во ударов мимо ворот', null=True)
    possession_time = models.PositiveIntegerField('Владение мячом, %', null=True)
    is_first_goal = models.BooleanField(verbose_name='Первый гол', default=False)

    event_datetime = models.DateTimeField('Дата события', null=True)

    def __str__(self):
        return '{} {}'.format(self.team_title, self.id)

    class Meta:
        verbose_name = 'объект статистики команды'
        verbose_name_plural = 'данные статистики команд'

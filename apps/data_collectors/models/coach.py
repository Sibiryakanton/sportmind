# coding=utf-8
from django.db import models
from .base_human import BaseHuman


class Coach(BaseHuman):
    '''
    Описание тренера на момент конкретного матча
    '''

    class Meta:
        verbose_name = 'тренера'
        verbose_name_plural = 'тренеры'

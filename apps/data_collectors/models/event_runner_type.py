# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class EventRunnerType(models.Model):
    '''
    Модель для хранения раннера (варианта ставки). То есть если "Обе забьют" - это маркет, то "Да" - это раннер, вариант
    '''
    event_type = models.ForeignKey('EventMarketType', on_delete=models.CASCADE, related_name='runners')
    betfair_id = models.PositiveIntegerField(verbose_name=_('ID раннера'), null=True, blank=True)
    runner_name = models.CharField(max_length=30, verbose_name=_('Название раннера'))
    frontend_name = models.CharField(max_length=20, verbose_name=_('Сокращенное название для фронтенда'),
                                     null=True, blank=True)
    xg_clf_alias = models.CharField(max_length=200, verbose_name=_('Условное обозначение раннера при работе с '
                                                                   'xg-классификаторами'), null=True,
                                    help_text='Обозначение определяется в соответствии с выдачей классификаторов: '
                                              'какое значене по логике соответствует данному раннера, то и вписать)')
    is_active = models.BooleanField(default=True, verbose_name=_('Активно'))

    def __str__(self):
        if self.frontend_name is None:
            return '{} {}'.format(self.event_type.market_name_en, self.runner_name)
        else:
            return self.frontend_name

    class Meta:
        verbose_name = 'Тип раннера'
        verbose_name_plural = 'Типы раннеров'

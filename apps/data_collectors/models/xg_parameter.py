# coding=utf-8
from django.db import models


class XGParameter(models.Model):
    group_type = models.CharField('Тип группы xg-значений', max_length=30)
    subgroup_type = models.CharField('Тип ПОДГРУППЫ xg-значений', max_length=15, null=True)
    name = models.CharField('Условное обозначение xg-значения', max_length=15)
    description = models.TextField(' описание сути xg-значения')

    def __str__(self):
        return self.full_name()

    def full_name(self):
        return '{}_{}_{}_{}'.format(self.group_type, self.subgroup_type, self.name.replace('/', ''), self.pk)

    class Meta:
        verbose_name = 'Шаблон xg-параметра'
        verbose_name_plural = 'Шаблоны xg-параметров'

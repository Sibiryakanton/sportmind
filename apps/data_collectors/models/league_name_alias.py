# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.data_collectors.enums import ServicesTypes


class LeagueNameAlias(models.Model):
    '''
    Одна и та же лига в разных сервисах имеет разные названия - к примеру, в xscore японская лига называется
    J. League, а в flashscore - J1 League. Чтобы с раcширением списка используемых сервисов не плодить кол-во полей и
     при этом избежать использования JSON, создана отдельная моделька для асссоциации конкретного названия у сервиса
      с лигой в нашей базе. Для удобства заполнения в админке эта модель будет использоваться как inline
    '''
    league = models.ForeignKey('League', on_delete=models.CASCADE, verbose_name=_('Ассоциируемая лига'))
    league_name = models.CharField(_('Название лиги по-английски'), max_length=300)
    service_name = models.CharField(max_length=100, choices=ServicesTypes.choices(), verbose_name=_('Названия сервиса'))

    class Meta:
        verbose_name = 'Алиас лиги'
        verbose_name_plural = 'Алиасы лиги'

    def __str__(self):
        return '{} {}'.format(self.league_name, self.service_name)

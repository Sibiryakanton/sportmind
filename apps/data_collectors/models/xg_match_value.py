# coding=utf-8
from django.db import models


class XGMatchValue(models.Model):
    '''
    Модель для хранения показателя команды по конкретному xg-параметру
    '''
    team = models.ForeignKey('Team', on_delete=models.CASCADE, verbose_name='Матч', related_name='xg_data')
    xg_parameter = models.ForeignKey('XGParameter', on_delete=models.CASCADE, verbose_name='Тип xg-параметра')
    value = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return '{} {}'.format(self.team, self.xg_parameter, self.value)

    class Meta:
        verbose_name = 'XG-значение'
        verbose_name_plural = 'XG-значение'

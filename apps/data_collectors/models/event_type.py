# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class EventMarketType(models.Model):
    '''
    Модель для хранения общего типа маркета (например, "Тотал больше/меньше 2.5").
    '''
    betfair_id = models.CharField(max_length=50, verbose_name=_('ID в системе Betfair'), null=True)
    market_name_en = models.CharField(max_length=100, verbose_name=_('Название по-английски'))
    market_name_ru = models.CharField(max_length=100, verbose_name=_('Название по-русски'))
    xg_clf_alias = models.CharField(max_length=200, verbose_name=_('Условное обозначение маркета при работе с '
                                                                   'xg-классификаторами'), null=True,
                                    help_text='Обозначение дожлно быть идентично названию мтеода Result, '
                                              'который испольузется для проверки данного исхода')
    flashscore_method = models.CharField(max_length=200, verbose_name=_('Название метода для сбора кэфов в flashscore'),
                                         null=True, blank=True)
    is_active = models.BooleanField(default=True, verbose_name=_('Активно'))
    is_dynamic_market = models.BooleanField(default=False, verbose_name=_('Динамичный маркет'),
                                            help_text=_('''У этого маркета, помимо указанных напрямую, 
                                            есть раннеры в виде названий команд (к примеру, 
                                            ставки на победителя - это ставка на конкретную команду либо ничью'''))

    def __str__(self):
        return self.market_name_en

    class Meta:
        verbose_name = 'Маркет'
        verbose_name_plural = 'Маркеты'

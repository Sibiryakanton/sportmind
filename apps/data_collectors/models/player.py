# coding=utf-8
from django.db import models
from apps.common.enums import POSITION_TYPES
from .base_human import BaseHuman


class Player(BaseHuman):
    '''
    Описание игрока на момент конкретного матча
    '''
    position = models.CharField('Позиция', max_length=20, choices=POSITION_TYPES)

    class Meta:
        verbose_name = 'игрока'
        verbose_name_plural = 'игроки'

# coding=utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class LeagueManager(models.Manager):
    def get_using_leagues(self):
        return self.get_queryset().filter(betfair_id__isnull=False, is_active=True,
                                          country__is_active=True).order_by('-id')


class League(models.Model):
    '''
    Объект для хранения данных о лиге
    '''
    country = models.ForeignKey('Country', verbose_name=_('Страна лиги'), max_length=300, on_delete=models.CASCADE,
                                related_name='leagues')
    league_name = models.CharField(_('Название лиги по-английски'), max_length=300)
    xscore_id = models.PositiveIntegerField(_('ID лиги в xscore.win'), null=True)
    betfair_id = models.CharField(max_length=20, verbose_name=_('ID лиги в BetFair'), null=True, blank=True)
    is_active = models.BooleanField(_('Используется в работе'), default=True)

    objects = LeagueManager()

    class Meta:
        verbose_name = 'Лига'
        verbose_name_plural = 'Лиги'
        ordering = ["country", 'league_name']

    def __str__(self):
        return self.league_name

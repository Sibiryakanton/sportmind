# coding=utf-8
from django.db import models
from django.contrib.postgres.fields import ArrayField
from apps.common.enums import FORMATION_TYPES
from django.utils.translation import ugettext_lazy as _


class TeamAliasArray(models.Model):
    '''
    В рамках разных источников данных одна и та же команда может иметь разные названия (к примеру, в xscore
    ЦСКА может называться ЦСКА Ростов, а во flashscore - просто ЦСКА). Можно было бы предположить, что это разные
    команды, если бы они не значились в ондом чемпионате в один и тот же тур на одну и ту же дату. И чтобы парсер \
    тоже умел это распознать, используется т.к. список псевдонимов команды - возможных названий,
    которые ей могут дать источники данных
    '''
    titles_array = ArrayField(base_field=models.CharField(max_length=50), verbose_name=_('Список алиасов'))

    def __str__(self):
        return str(self.titles_array)

    class Meta:
        verbose_name = 'Псевдоним команды'
        verbose_name_plural = 'Псевдонимы команды'
        ordering = ['titles_array']

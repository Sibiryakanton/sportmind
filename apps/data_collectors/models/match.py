# coding=utf-8
from django.db import models
from django.db.utils import IntegrityError
from django.db.models.signals import post_delete, post_save

from django.utils.timezone import now, timedelta
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from apps.common.enums import EventStatusTypes
from apps.data_collectors.utils import get_or_update_alias
from .xg_parameter import XGParameter


class MatchManager(models.Manager):
    def get_postponed_matches(self, league):
        '''
        Получить отмененные матчи
        '''
        return super().get_queryset().filter(
            league=league, result__isnull=True, event_datetime__lt=now()-timedelta(hours=10),
            event_status__in=[EventStatusTypes.LIVE, EventStatusTypes.NOT_STARTED, None])

    def get_live_matches(self, league, updated_matches):
        '''
        Получить матчи, которые только что вышли в лайв, но в базе пока значатся как Not started
        :param league: объект League
        :param updated_matches: список id обновленных матчей
        '''
        return super().get_queryset().filter(
            league=league, result__isnull=True,
            event_status=EventStatusTypes.NOT_STARTED).exclude(
            pk__in=updated_matches)

    def get_not_close_matches(self, league):
        '''
        Получить матчи, до которых больше получаса
        '''
        return super().get_queryset().filter(league=league, result__isnull=True,
                                             event_datetime__gte=now() + timedelta(minutes=30))

    def update_or_create_with_alias_using(self, first_team, second_team, defaults):
        '''
        Сохранение с учетом того, что название одной из команд может отличаться от существующего в базе.
        Если проблема в неправильном названии команды, нужно найти матч по правильной команде, а неожиданное
        альтернативное название другой - зафиксировать. Исключением ниже мы пытаемся определить, с какой из
        команд проблема
        :param first_team: Team объект
        :param second_team: Team объект
        :param defaults: остальные данные матча
        :return:
        '''
        try:
            match, is_created = Match.objects.update_or_create(first_team=first_team, second_team=second_team,
                                                               defaults=defaults)
            return match, is_created, True
        except IntegrityError:
            try:
                match = Match.objects.get(first_team=first_team)
                is_correct = get_or_update_alias(match.second_team, match, second_team.title)[-1]
            except Match.DoesNotExist:
                match = Match.objects.get(second_team=second_team)
                is_correct = get_or_update_alias(match.first_team, match, first_team.title)[-1]
            if is_correct:
                for key, value in defaults.items():
                    setattr(match, key, value)
                match.save()
            return match, 0, is_correct


class Match(models.Model):
    '''
    Основной объект статистики, к которому остальные модели (судьи, игроки, коучи, команды, результат и т.п.)
    идут в качестве дополнения.
    Значения некоторых полей отмечу отдельно:
    '''
    xscore_id = models.CharField(_("ID матча с системе xscore"), null=True, max_length=50)
    betfair_id = models.CharField(_("ID матча с системе BetFair"), null=True, max_length=50)
    flashscore_id = models.CharField(_("ID матча с системе Flashscore"), null=True, max_length=50)
    first_team = models.OneToOneField('Team', verbose_name=_('Первая команда'), on_delete=models.PROTECT,
                                      related_name='+')
    second_team = models.OneToOneField('Team', verbose_name=_('Вторая команда'), on_delete=models.PROTECT,
                                       related_name='+')
    result = models.OneToOneField('Result', verbose_name=_('Результат'), on_delete=models.PROTECT, related_name='match',
                                  null=True, blank=True)

    league = models.ForeignKey('League', on_delete=models.CASCADE, verbose_name=_('Лига'))
    season = models.CharField(_('Сезон'), max_length=30)
    tour = models.CharField('Тур', max_length=15)
    event_datetime = models.DateTimeField(_('Дата и время начала события'), null=True)
    event_status = models.CharField(_('Статус события'), choices=EventStatusTypes.CHOICES, null=True, max_length=30)

    objects = MatchManager()

    def __str__(self):
        return '{} vs. {} ({})'.format(self.first_team, self.second_team, self.pk)

    def title_in_betfair_format(self):
        '''
        Отдельно для работы с Betfair название матча нужно выдавать в специальном формате
        '''
        return '{} v {}'.format(self.first_team.title, self.second_team.title)

    def is_passed(self):
        '''
        Прошел ли матч
        '''
        return bool(self.result)

    def xgdata_status(self):
        '''
        Собраны ли все xg-данные о матче
        '''
        if self.first_team and self.first_team.xg_data.all().exists():
            match_values = len(self.first_team.xg_data.all())
            all_xgdata_variables = len(XGParameter.objects.all())
            return match_values == all_xgdata_variables or '{}/{}'.format(match_values, all_xgdata_variables)

    def get_xscore_classifier_data(self):
        '''
        Получить xg-данные матча и результат
        :return: fteam_values - xg-значения первой команды, steam_values - второй команды, result - объект результата
        '''
        firstteam_xg_values_objs = self.first_team.xg_data.all()
        secondteam_xg_values_objs = self.second_team.xg_data.all()
        fteam_values = dict((xg_elem.xg_parameter.full_name(), xg_elem.value) for xg_elem in firstteam_xg_values_objs)
        steam_values = dict((xg_elem.xg_parameter.full_name(), xg_elem.value) for xg_elem in secondteam_xg_values_objs)
        return fteam_values, steam_values, self.result

    def get_all_possible_match_titles(self):
        '''
        Получить все возможные варианты названия матча с учетом возможных названий команд
        '''
        first_team_alias = get_or_update_alias(self.first_team)[0]
        second_team_alias = get_or_update_alias(self.second_team)[0]
        titles_list = []
        for first_alias in first_team_alias.titles_array:
            for second_alias in second_team_alias.titles_array:
                titles_list.append(self.title_in_betfair_format(first_alias, second_alias))
        return titles_list

    class Meta:
        verbose_name = _('матч')
        verbose_name_plural = _('матчи')


@receiver(post_delete, sender=Match)
def remove_results_and_teams(sender, instance, **kwargs):
    if instance.first_team:
        instance.first_team.delete()
    if instance.second_team:
        instance.second_team.delete()
    if instance.result:
        instance.result.delete()


@receiver(post_save, sender=Match)
def check_tips(sender, instance, **kwargs):
    '''
    Проверить связанные с матчем прогнозы и отметить их как выигрышные/проигрышные
    '''
    if instance.result is not None:
        odds = instance.tips.filter(tip_status__isnull=True)
        for odd in odds:
            odd.update_tip_status()


from django.core.management.base import BaseCommand
from apps.data_collectors.managers import flashscore_last_results


class Command(BaseCommand):
    help = 'launch the XscoreParser'

    def handle(self, *args, **options):
        flashscore_last_results()

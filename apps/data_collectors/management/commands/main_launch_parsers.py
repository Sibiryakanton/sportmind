from django.core.management.base import BaseCommand
from django.utils.timezone import now

from apps.analysis.management.commands.xscore_create_tips import xscore_create_tips
from apps.betting.management.commands.activate_tips import activate_tips

from apps.data_collectors.managers import (xscore_last_matches, flashscore_last_results,
                                           flashscore_actual_matches, upload_odds)


class Command(BaseCommand):
    help = 'scrap the data with parsers and create tips'

    def handle(self, *args, **options):
        debug = False
        xscore_last_matches(debug=debug)
        flashscore_actual_matches(debug=debug)
        flashscore_last_results(debug=debug)
        upload_odds(debug=debug)
        xscore_create_tips('production')
        activate_tips()

    def write_time(self):
        '''
        Функция просто записывает текущее время в дополние к остальному тексту файла.
        Применяется в этой же команде в редких случаях, когда надо посмотреть
        производительность парсеров
        '''
        with open('test-time.txt', 'a') as inf:
            string = str(now()) + '\n'
            inf.write(string)

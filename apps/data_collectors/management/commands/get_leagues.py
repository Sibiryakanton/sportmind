from django.core.management.base import BaseCommand
from apps.data_collectors.managers import helper_find_league_id


class Command(BaseCommand):
    help = 'Upload odds from BetFair'

    def handle(self, *args, **options):
        '''
        Просто меняйте вручную country_code перед применением команды - она вернет список лиг на Betfair,
        подходящих под условия
        :param args:
        :param options:
        :return:
        '''
        country_code = 'KOR'
        league_name = ''
        helper_find_league_id(country_code, league_name)

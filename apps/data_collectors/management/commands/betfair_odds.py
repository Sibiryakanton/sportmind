from django.core.management.base import BaseCommand
from apps.data_collectors.managers.controllers import BetfairController


class Command(BaseCommand):
    help = 'Upload odds from Betfair'

    def add_arguments(self, parser):
        parser.add_argument('debug', type=bool, nargs='?', default=False)

    def handle(self, *args, **options):
        controller = BetfairController()
        controller.upload_odds(debug=options['debug'])

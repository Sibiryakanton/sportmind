from django.core.management.base import BaseCommand
from apps.data_collectors.managers.controllers import BetfairController


class Command(BaseCommand):
    help = 'update balance from Betfair'

    def handle(self, *args, **options):
        controller = BetfairController()
        controller.update_wallet_info()

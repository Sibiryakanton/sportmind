from django.core.management.base import BaseCommand
from apps.data_collectors.managers import xscore_collect_dataset


class Command(BaseCommand):
    help = 'launch the XscoreParser'

    def handle(self, *args, **options):
        xscore_collect_dataset()

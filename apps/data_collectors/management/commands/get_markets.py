from django.core.management.base import BaseCommand
from apps.data_collectors.managers import helper_find_market_name


class Command(BaseCommand):
    help = 'Upload odds from BetFair'

    def handle(self, *args, **options):
        market_name = 'Both teams to Score?'
        helper_find_market_name(market_name)

from django.core.management.base import BaseCommand
from apps.data_collectors.managers import xscore_last_matches


class Command(BaseCommand):
    help = 'launch the XscoreParser'

    def handle(self, *args, **options):
        xscore_last_matches()

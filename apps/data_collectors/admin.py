from django.contrib import admin
from .models import *


class AdminDeletePermissionDenied(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return True


class OddModelInline(admin.TabularInline):
    model = OddModel


class LeagueNameAliasInline(admin.TabularInline):
    model = LeagueNameAlias


@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
    list_display = ('pk', 'first_team', 'second_team', 'league', 'event_datetime', 'event_status',
                    'result', 'xgdata_status')
    search_fields = ('pk', 'first_team__title', 'second_team__title', 'first_team__pk', 'second_team__pk',
                     'league__league_name', 'league__country__country_name_en', 'flashscore_id', 'result__pk')
    readonly_fields = ('first_team', 'second_team', 'result')
    inlines = [OddModelInline]
    list_per_page = 20


@admin.register(Team)
class TeamAdmin(AdminDeletePermissionDenied):
    list_display = ('pk', 'title')
    search_fields = ('title', 'pk')

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(Result)
class ResultAdmin(AdminDeletePermissionDenied):
    list_display = ('event_datetime', 'first_team_stats', 'second_team_stats')
    search_fields = ('pk', 'match__xscore_id', 'match__pk')
    list_display_links = ('event_datetime', 'first_team_stats', 'second_team_stats')

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(TeamStatistic)
class TeamStatisticAdmin(AdminDeletePermissionDenied):
    list_display = ('pk', 'team_title', 'score_1_half', 'score', 'is_first_goal')
    search_fields = ('pk', 'team_title', )

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(Country)
class CountryAdmin(AdminDeletePermissionDenied):
    list_display = ('pk', 'code', 'country_name_ru', 'country_name_en', 'xscore_id', 'is_active')
    search_fields = ('pk', 'code', 'country_name_ru', 'country_name_en', 'xscore_id', 'is_active')
    list_editable = ('is_active', 'code')


@admin.register(League)
class LeagueAdmin(AdminDeletePermissionDenied):
    list_display = ('pk', 'league_name', 'country', 'xscore_id', 'betfair_id', 'is_active')
    search_fields = ('pk', 'league_name', 'country', 'xscore_id', 'betfair_id', 'is_active')
    list_display_links = ('league_name', )
    list_editable = ('is_active', 'betfair_id',)
    inlines = [LeagueNameAliasInline]


class EventRunnerTypeInline(admin.TabularInline):
    model = EventRunnerType


@admin.register(EventMarketType)
class EventMarketTypeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'market_name_en', 'is_active')
    search_fields = ('market_name_en',)
    list_display_links = ('market_name_en', )
    list_editable = ('is_active',)
    inlines = [EventRunnerTypeInline]


@admin.register(XGParameter)
class XGParameterAdmin(AdminDeletePermissionDenied):
    list_display = ('pk', 'group_type', 'subgroup_type', 'name')
    search_fields = ('pk', 'group_type', 'subgroup_type', 'name')

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

'''
@admin.register(XGMatchValue)
class XGMatchValueAdmin(AdminDeletePermissionDenied):
    list_display = ('pk', 'team', 'xg_parameter', 'value')
    search_fields = ('pk', 'team__pk')

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]
'''

admin.site.register(Bookmaker)
# admin.site.register(OddSource)
admin.site.register(TeamAliasArray)

from apps.data_collectors.managers.controllers import BetfairController
from .services import ServicesTypes


class BookmakerControllerTypes:
    '''
    Этот класс используется в автоматических ставках, чтобы в зависимости от того, у какого
    букмекера нужно сделать ставку, был подобран соответствующий контроллер
    '''
    CLASS_CHOICES = {ServicesTypes.betfair.value: BetfairController}

from apps.common.enums import BaseEnum


class ServicesTypes(BaseEnum):
    xscore = 'XScore'
    flashscore = 'FlashScore'
    betfair = 'BetFair'
